import { View, Text,StyleSheet,Image,TextInput } from 'react-native'
import React from 'react'
import { Width } from '../../dimensions/dimension'
import { imageConstant } from '../../utils/constant'

const MyCommentNew = () => {
  return (
    <View style={styles.main}>
     {/* <View style={{marginTop:'5%',borderBottomWidth:0.5,paddingVertical:10,borderBottomColor:'#929292'}}>
            <View style={styles.view1}>
            <Image
            resizeMode='contain'
            source={imageConstant.profile2}
            style={styles.img1}
            />
            <View>
                <Text style={styles.text1}>Kelvin</Text>
                <Text style={styles.text2}>We had a great time. 😃</Text>
            </View>
            <Text style={styles.text3}>22/10/2022, 6:32 PM</Text>
            </View>
            <TextInput
            placeholder='Add a comment...'
            placeholderTextColor='#929292'
            style={styles.input}
            />
        </View> */}
        <View style={{marginTop:'5%',borderBottomWidth:0.5,paddingVertical:15,borderBottomColor:'#929292',justifyContent:'center'}}>
            <View style={styles.view1}>
            <Image
            resizeMode='contain'
            source={imageConstant.profile2}
            style={styles.img1}
            />
            <View>
                <Text style={styles.text1}>We had a great time. 😃</Text>
                <Text style={[styles.text2,{color:'#929292'}]}>22 minutes ago</Text>
            </View>
            {/* <Text style={styles.text3}>22/10/2022, 6:32 PM</Text> */}
            </View>
            <View style={{flexDirection:'row',width:'35%',justifyContent:'space-between',marginTop:'3%',left:'90%',alignItems:'center'}}>
            <Image
            resizeMode='contain'
            source={imageConstant.profile2}
            style={styles.img2}
            />
            <View >
                {/* <Text style={styles.text1}>Kelvin</Text> */}
                <Text style={styles.text2}>Thanks...!! 😃</Text>
                <Text style={styles.text3}>22/10/2022, 6:32 PM</Text>
            </View>
            
            </View>
        </View>
        <View style={{borderBottomWidth:0.5,paddingVertical:20,borderBottomColor:'#929292',justifyContent:'center'}}>
            <View style={styles.view1}>
            <Image
            resizeMode='contain'
            source={imageConstant.profile2}
            style={styles.img1}
            />
            <View>
                <Text style={styles.text1}>We had a great time. 😃</Text>
                <Text style={[styles.text2,{color:'#929292'}]}>22 minutes ago</Text>
            </View>
            {/* <Text style={styles.text3}>22/10/2022, 6:32 PM</Text> */}
            </View>
           
        </View>
        {/* <View style={{marginTop:'5%'}}>
            <View style={styles.view1}>
            <Image
            resizeMode='contain'
            source={imageConstant.profile2}
            style={styles.img1}
            />
            <View>
                <Text style={styles.text1}>Kelvin</Text>
                <Text style={styles.text2}>We had a great time. 😃</Text>
                <Text style={styles.text4}>Reply</Text>
            </View>
            <Text style={styles.text3}>22/10/2022, 6:32 PM</Text>
            </View>
        
        </View> */}
    </View>
  )
}

export default MyCommentNew
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF'
    },
    view1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-evenly',
        width:Width*0.70,
        // alignSelf:'flex-start'
    },
    input:{
        width:299,
        borderBottomWidth:1,
        borderBottomColor:'#929292',
        color:'grey',
        left:'10%'
    },
img1:{
    height:40,
    width:40
},
text1:{
    color:'#000000',
    fontSize:17,
    fontWeight:'700'
},
text2:{
    color:'#000000',
    fontSize:12
},
text3:{
fontSize:10,
color:'#929292'
},
img2:{
    height:30,
    width:30
},
text4:{
    fontSize:14,
    color:'#6C00C0',
    marginTop:'5%'
}
})