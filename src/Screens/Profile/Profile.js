import { Image, ImageBackground, Platform, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View,FlatList, Pressable } from 'react-native'
import React from 'react'
// import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { imageConstant,colorConstant,fontConstant } from '../../utils/constant'
import { Height,Width } from '../../dimensions/dimension'
import { baseUrl } from '../../utils/Urls'
import { useSelector } from 'react-redux'
import { useEffect } from 'react'
import { useState } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useIsFocused } from '@react-navigation/native'
import CustomHeader from '../../custom/CustomHeader'
// import { FlatList } from 'react-native-gesture-handler'

const Profile = (props) => {
    const [profileData,setProfileData]=useState("")
let {jwtToken}=useSelector((state)=>state.reducers)
const isFocused=useIsFocused()
useEffect(()=>{
    getProfileData()
},[isFocused])
const getProfileData=async()=>{
let response=await fetch(baseUrl+"user/passenger-view-profile",{
    method:"GET",
    headers:{
        'Authorization':jwtToken
    }
})
.then(async(res)=>{
    let jsonData=await res.json()
    return jsonData
})
.then(async(res)=>{
    setProfileData(res)
    console.log("propfile ka data",profileData)
    await AsyncStorage.setItem("Profile",JSON.stringify(res))
})
}

    let data2=[
        {
            id:1,
            img:imageConstant.taxi,
        },
        {
            id:2,
            img:imageConstant.taxi3,
        },
        {
            id:3,
            img:imageConstant.taxi,
        },
        {
            id:4,
            img:imageConstant.taxi2,
        },
        {
            id:5,
            img:imageConstant.taxi2,
        },
        {
            id:6,
            img:imageConstant.taxi,
        },
        {
            id:7,
            img:imageConstant.taxi3,
        },
        {
            id:8,
            img:imageConstant.taxi,
        },
        {
            id:9,
            img:imageConstant.taxi2,
        },
        {
            id:10,
            img:imageConstant.taxi,
        },
        {
            id:11,
            img:imageConstant.taxi3,
        },
        {
            id:12,
            img:imageConstant.taxi,
        }
    ]
    const renderDetails=({item,index})=>{
        return(
            
            <View
            key={index}>
                <Image
                resizeMode='contain'
                style={styles.img5}
                source={item?.img}
                />
            </View>
        )
    }
    const _renderItem = () => {
        return (
            <View style={{
                alignSelf:"center",
                margin:6
            }}>
                 <Image
                resizeMode='contain'
                source={imageConstant.jpeg}
                style={{
                    width: 110,
                    height:110,
                }}
            />
            </View>
           
        )
    }
  return (
    <View style={styles.main}>
        <CustomHeader
        title={"My Profile"}
        onPressBack={()=>props.navigation.goBack()}
        />

        
          

          <ScrollView
          bounces={false}
          contentContainerStyle={{paddingBottom:30}}
              showsVerticalScrollIndicator={false}
          >

<View style={styles.view3}>
            <Image
            resizeMode='contain'
            style={styles.img3}
            source={profileData?.userData?.profilePic ? {uri:profileData?.userData?.profilePic} : imageConstant.profileDemo}
            />
            <View style={{right:0}}>
                <Text style={styles.text4}>{profileData?.userData?.name} {profileData?.userData?.lastName}</Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                     <Image style={{height:18,width:18}} resizeMode='contain' source={imageConstant.star}/>
                <Text style={styles.text5}> 4</Text>
                </View>
            </View>
            <View style={styles.view4}>
                <Image
                resizeMode='contain'
                style={{height:33,width:33}}
                source={imageConstant.calen}/>
                <Pressable
                onPress={()=>props.navigation.navigate("Comments")}
                >
                <Image
                resizeMode='contain'
                style={{height:30,width:30,right:15}}
                source={imageConstant.Speech}/>
                </Pressable>
            </View>
        </View>
              
              <Text style={styles.text1}>{profileData?.userData?.about_you}</Text>
              <View style={styles.row1}>
            {profileData?.userData?.is_smoking   ?   <Image
                      resizeMode='contain'
                      source={imageConstant.NoSmoking}
                      style={{
                        width: 30,
                        height: 30,
                        marginLeft: 23

                    }}
                  />:null}
              {profileData?.userData?.is_music ?  <Image
                      source={imageConstant.Musical}
                      style={{
                          width: 30,
                          height: 30,
                          marginLeft: 23

                      }}
                  />
                  :
                  null}
                  {profileData?.userData?.is_chat   ? 
                  <Image
                      source={imageConstant.Speech}
                      style={{
                          width: 30,
                          height: 30,
                          marginLeft: 23

                      }}
                  />
                  :null}
                 {profileData?.userData?.is_pet   ?  <Image
                      source={imageConstant.NoAnimals}
                      style={{
                          width: 30,
                          height: 30,
                          marginLeft: 23

                      }}
                  />
                  :null}
              </View>
              
              <Pressable style={styles.pres1}
              onPress={()=>props.navigation.navigate("WriteAbout",{txt:"WriteAbout"})}
              >
              <Image style={styles.img4} resizeMode='contain' source={imageConstant.EDIT}/> 
                <Text style={styles.text2}>   Edit</Text>
              </Pressable>
              
              <Pressable style={styles.pres1}>
              <Image style={styles.img4} resizeMode='contain' source={imageConstant.upload}/> 
                <Text style={styles.text2}>   Upload</Text>
              </Pressable>
              <FlatList
                renderItem={renderDetails}
                data={data2}
                // horizontal
                numColumns={4}
                contentContainerStyle={styles.flatview}
                />
          </ScrollView>

          

    </View>
  )
}

export default Profile

const styles = StyleSheet.create({
    main:{ 
        flex:1,
        backgroundColor:colorConstant.white
    },
    row:{
        marginTop:20,
        flexDirection:"row",
        alignItems:"center",
        width:"90%",
        alignSelf:"center",
        justifyContent:"space-between"
    },
    img:{
        width:20,
        height:20
    },
    img1:{
        width:20,
        height:20,
    },
    searchView:{
        flexDirection:"row",
        width:"85%",
        alignItems:"center",
        backgroundColor:'#EDF1F7',
        paddingVertical:3,
        paddingHorizontal:10,
        borderRadius:10,
        height:45
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 3,
        // },
        // shadowOpacity: 0.27,
        // shadowRadius: 4.65,

        // elevation: 6,
    },
    input:{
        width:"90%",
        paddingLeft:5,
        ...Platform.select({
            android:{
                paddingVertical:0
            }
        })
    },
    profile:{
        width:100,
        height:100,
        alignSelf:"center",
        marginTop:40
    },
    text:{
        fontSize:24,
        fontFamily:fontConstant.semi,
        color:colorConstant.black,
        textAlign:"center",
        marginTop:15
    },
    img2:{
        width:"60%",
        height:30,
        alignSelf:"center",
        marginTop:20
    },
    text1:{
        fontSize:14,
        fontFamily:fontConstant.regular,
        color:colorConstant.black,
        width:"90%",
        alignSelf:"center",
        textAlign:"left",
        marginTop:20,
        lineHeight:17
    },
    row1:{
        flexDirection:"row",
        alignSelf:"flex-start",
        // width:"90%",
        alignItems:"center",
        justifyContent:"center",
        marginTop:20
    },
    view3:{
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
        marginTop:'8%'
    },
    img3:{
        height:60,
        width:60,
        borderRadius:30,
        borderWidth:2,
        borderColor:colorConstant.theme
    },
    text4:{
        fontSize:15,
        color:'#000000',
        fontWeight:'600'
    },
    text5:{
        fontSize:14,
        color:'#000000',
        textAlign:'center'
    },
    text6:{
        fontSize:12,
        color:'#000000'
    },
    text7:{
        fontSize:14,
        color:'#6C00BF',
        fontWeight:'600'
    },
    view4:{
        width:'25%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    pres1:{
        height:38,
        width:Width*0.75,
        borderRadius:9,
        borderWidth:1,
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row',
        alignSelf:'center',
        marginTop:'5%'
    },
    img4:{
        height:24,
        width:24
    },
    text2:{
        color:'#000000',
        fontSize:15,
        fontWeight:'700'
    },
    img5:{
        margin:3,
        height:80,
        width:80
    },
    flatview:{
        width:Width *0.90,
        alignSelf:'center',
        alignItems:'center',
        justifyContent:'center',
        marginTop:'12%'
    },
})