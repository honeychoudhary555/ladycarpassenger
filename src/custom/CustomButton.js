import { View, Text, TouchableOpacity ,StyleSheet,Platform,Image} from 'react-native';
import React, { useEffect, useState } from 'react';
import {Width,Height} from '../dimensions/dimension';
import { colorConstant, fontConstant, imageConstant } from '../utils/constant';
import { heightPercentageToDP, widthPercentageToDP } from '../utils/responsiveFIle';



const CustomButton = (props) => {
  const [imgFlag,setImgFlag]=useState(false)
  const [imageRightFlag,setImageRightFlag]=useState(false)
  useEffect(()=>{
    setImgFlag(props.imageFlag)
    setImageRightFlag(props.imageRightFlag)
  },[])
  return (
      <TouchableOpacity
      disabled={props.disabled ? props.disabled :false}
        onPress={props.OnButtonPress}
        activeOpacity={0.8}
        style={[
          {...styles.ButtonTouch},
          {
              marginTop:props.marginTop,
              marginBottom:props.marginBottom,
              width: props.width ? props.width : "85%",
              position:props.position,
              bottom:props.bottom,
              borderRadius:props.borderRadius,
              borderColor:props.borderColor,
              top:props.top,
              borderWidth:props.borderWidth,
              shadowColor:props.shadowColor,
              shadowRadius:props.shadowRadius,
              shadowOpacity:props.shadowOpacity,
              shadowOffset:props.shadowOffset,
              elevation:props.elevation,
              alignSelf:props.alignSelf,
              backgroundColor:props.disabled ? "rgba(0,0,0,0.2)" : props.backgroundColor? props.backgroundColor: colorConstant.theme
          }]}
        >
      <View style={{flexDirection:'row'}}> 
     {imgFlag ? <Image 
        source={props.imageName}
        style={{
          width:25,
          height:25
        }}
        resizeMode="contain"
        />:null}
       <Text style={[styles.text,{color:props.color ? props.color : colorConstant.white,marginTop:props.txtmarginTop}]}> {props.buttonName}</Text>
        {
          props.imageNames &&
          <Image source={props.imageNames}
            resizeMode='contain'
            style={{
              width: 26,
              height: 30,
              left: 50,

            }}
          />}
        </View>
      </TouchableOpacity>
  );
};

export default CustomButton;

const styles = StyleSheet.create({
  ButtonTouch:{
    height :  Height/18,
    // alignSelf:"center",
    justifyContent:"center",
    alignItems:"center",
    borderRadius:Width/38,
    
  },
  text:{
    fontSize:  17,
    fontFamily:fontConstant.semibold,
    color:colorConstant.white
    
  }    
})
