import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity, Pressable, ScrollView } from 'react-native'
import React from 'react'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Height, Width } from '../../dimensions/dimension';
import { useState } from 'react';
import CustomButton from '../../custom/CustomButton';
import { useFocusEffect } from '@react-navigation/native';


const IdVerification = (props) => {
const [check, setCheck]=useState(true)
const [SelectedId, setSelectedId]=useState(null)
const [status,setStatus]=useState(false)
const [value,setValue]=useState(true)
useFocusEffect(()=>{
    setTimeout(()=>{
        props.navigation.navigate('OnTheWay')
    },4000)
})
    const mapStyle = [
        { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
        {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{ color: '#263c3f' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#6b9a76' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{ color: '#38414e' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#212a37' }],
        },
        {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#9ca5b3' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{ color: '#746855' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#1f2835' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#f3d19c' }],
        },
        {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{ color: '#2f3948' }],
        },
        {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{ color: '#17263c' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#515c6d' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{ color: '#17263c' }],
        },
    ];




    return (
        <View style={styles.main}>
            <ScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{backgroundColor:'#FFFFFF'}}
            >
            <View style={{height:Height * 0.50}}>
            <MapView
                zoomEnabled={true}
                zoomTapEnabled={true}
                style={styles.mapStyle}
                initialRegion={{
                    latitude: 28.5355,
                    longitude: 77.3910,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
                customMapStyle={mapStyle}>
                <Marker
                    icon={imageConstant.location}
                    draggable
                    coordinate={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                    }}
                    onDragEnd={
                        (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                    }
                    title={'Test Marker'}
                    description={'This is a description of the marker'}
                />
            </MapView>
         
            <View style={styles.view}>
                <Pressable onPress={()=>{props.navigation.toggleDrawer()}}>

                
                <Image
                resizeMode='contain'
                style={{height:42,width:42}}
                    source={imageConstant.menuIcon}
                />
            </Pressable>
            <Pressable onPress={()=>props.navigation.navigate("Profile")}>
                    <Image
                    resizeMode='contain'
                        style={styles.img1}
                        source={imageConstant.profile}
                    />
                </Pressable>
            </View>

            <Image
                style={styles.img2}
                source={imageConstant.Zooms}
            />
             </View>
            <Pressable  style={styles.view1}>
            <Image
                    resizeMode='contain'
                    style={styles.img3}
                    source={imageConstant.Rect}
                />
                <Text style={{ fontSize: 9,marginTop:'1%' }}>Travel safe</Text>
                <TouchableOpacity 
            
            style={styles.touch}>
               <Image
                resizeMode='contain'
                style={styles.img4}
                source={imageConstant.profile2}/>
                
                <View style={{right:'80%'}}>
                <Text style={styles.text4}>Sonia</Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                <Image
                    source={imageConstant.star}/>
                <Text style={{fontSize:15,color:'#000000',left:5}}>4.3</Text>
                </View>
                </View>

                <View style={{flexDirection:'row',alignItems:'center',width:'20%',justifyContent:'space-between'}}>
                    <Image
                      resizeMode='contain'
                      style={{height:28,width:28,top:2,right:10}}
                    source={imageConstant.Phone}
                    />
                    <Image
                    resizeMode='contain'
                    style={{height:30,width:30}}
                    source={imageConstant.Speech}
                    />
                     
                </View>
            </TouchableOpacity>
                    <Text style={styles.text3}>Wait for your driver to verify her/his identity before getting in the car  </Text>
                  <View style={styles.view4}>
                    <Text style={styles.text1}>Driver’s face validation</Text>
                    <View style={styles.view5}>
                        <Text style={styles.text5}>Pending</Text>
                    <Image
                    source={imageConstant.Sand}
                    />
                    
                    </View>
                  </View>
                  <View style={[styles.view4,{marginTop:'5%'}]}>
                    <Text style={styles.text1}>Passenger’s face validation</Text>
                    <View style={styles.view5}>
                        <Text style={styles.text5}>Done</Text>
                    <Image
                    source={imageConstant.Ok}
                    />
                   
                    </View>
                  </View>
                  <Pressable
                  style={{flexDirection:'row',alignItems:'center',marginTop:'7%',height:20}}
                  onPress={()=>props.navigation.navigate("CancelReason")}>
            <Text style={styles.cancelText}>Cancel?  </Text>
            <Image resizeMode='contain' style={{height:24,width:24}} source={imageConstant.info}/>
            </Pressable>
            </Pressable>
                    
            </ScrollView>
        </View>
    )
}

export default IdVerification
const styles = StyleSheet.create({
    main: {
        flex: 1,
        // alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    mapStyle: {
        flex: 2,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom:0,
    },
    text:{
        fontSize:26,
        color:colorConstant.theme,
        width:150,
        top:30,
        fontFamily:fontConstant.semibold,
        height:90,
        textAlign:'center'
    },
    text2:{
        fontSize:20,
        color:'#000000',
        right:25
    },
    view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Width,
        marginTop:'2%'
    },
    view1: {
        backgroundColor: '#FFFFFF',
        height: '45%',
        // position: 'absolute',
        // top: '55%',
        width: Width,
        alignItems: 'center',
       

    },
  
    view2:{
        width:'95%',
        backgroundColor:'#FFFFFF',
        height:'20%',
        // borderTopWidth:1,
        marginTop:'3%',
        flexDirection:'row',
        justifyContent:'space-between',
        // marginTop:'5%',
        alignItems:'center'
    },
 
    view3:{
        flexDirection:'row',
        justifyContent:'space-between',
        // alignItems:'center',
        marginTop:'8%'
    },
    cancelText:{
        alignSelf:'center',
        color:colorConstant.theme,
        marginTop:'10%',
        paddingBottom:50
    },
    img1:{
        borderWidth: 2, 
        borderColor: '#FFFFFF', 
        borderRadius: 100, 
        right: 5,
        height:33,
        width:33
    },
    img2:{
        alignSelf: 'flex-end', 
        right: 5,
         top: '3%'
    },
    img3:{
        marginTop: '3%',
         width: 77,
          height: 6
    },
    touch:{
        flexDirection:'row',
        height:60,
        backgroundColor:'#FFFFFF',
        width:Width ,
        borderRadius:14,
        justifyContent:'space-around',
        alignItems:'center',
        // top:'65%'
    },
    text1:{
        color:colorConstant.black,
        fontFamily:fontConstant.semibold
    },
    text3:{
        color:colorConstant.theme,
        fontSize:19,
        top:'10%',
        width:'90%',
        alignSelf:'center',
        fontFamily:fontConstant.bold,
        textAlign:'center'
    },
    view4:{
        flexDirection:'row'
        ,marginTop:'15%',
        // backgroundColor:'red',
        width:Width *0.8,
        justifyContent:'space-between',
        alignItems:'center'
    },
    view5:{
        flexDirection:'row',
        width:'25%',
        justifyContent:'space-around',
        alignItems:'center'
    },
    driverText:{
        color:'#000000',
        fontSize:18,
        fontFamily:fontConstant.bold
    },
    imgProfile:{
        borderWidth:2,
        height:39,
        width:39,
        borderColor:'#FFFFFF',
        borderRadius:50
    },
    ratingText:{
        right:1,
        color:'#000000',
        fontSize:20
    },
    text4:{
        fontSize:20,
        color:'#000000',
        // right:40
    },
    view6:{
        flexDirection:'row',
        alignItems:'center'
    },
    img4:{
        right:'10%'
    },
    text5:{
        fontSize:12,
        color:'#000000'
    }
})
