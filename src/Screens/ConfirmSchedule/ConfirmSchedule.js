import { View, Text,StyleSheet, FlatList, Image, Pressable } from 'react-native'
import React from 'react'
import CustomMap from '../../custom/CustomMap'
import MapView, { PROVIDER_GOOGLE,Marker } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Height, Width } from '../../dimensions/dimension';
import CustomButton from '../../custom/CustomButton';
import { ScrollView } from 'react-native-gesture-handler';
import { useState } from 'react';
import { panHandlerName } from 'react-native-gesture-handler/lib/typescript/handlers/PanGestureHandler';
import CheckBox from '@react-native-community/checkbox';


const ConfirmSchedule = (props) => {
  const [Selected,setSelected]=useState(false)
  const [check,setCheck]=useState(false)
let val=props?.route?.params?.val
    const Data=[
        {
            id:1,
            name:'Standard'
        },
        {
            id:2,
            name:'XL'
        },
        {
            id:3,
            name:'Auto'
        },
    ]

    const mapStyle = [
        {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
        {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
        {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
        {
          featureType: 'administrative.locality',
          elementType: 'labels.text.fill',
          stylers: [{color: '#d59563'}],
        },
        {
          featureType: 'poi',
          elementType: 'labels.text.fill',
          stylers: [{color: '#d59563'}],
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [{color: '#263c3f'}],
        },
        {
          featureType: 'poi.park',
          elementType: 'labels.text.fill',
          stylers: [{color: '#6b9a76'}],
        },
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [{color: '#38414e'}],
        },
        {
          featureType: 'road',
          elementType: 'geometry.stroke',
          stylers: [{color: '#212a37'}],
        },
        {
          featureType: 'road',
          elementType: 'labels.text.fill',
          stylers: [{color: '#9ca5b3'}],
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry',
          stylers: [{color: '#746855'}],
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry.stroke',
          stylers: [{color: '#1f2835'}],
        },
        {
          featureType: 'road.highway',
          elementType: 'labels.text.fill',
          stylers: [{color: '#f3d19c'}],
        },
        {
          featureType: 'transit',
          elementType: 'geometry',
          stylers: [{color: '#2f3948'}],
        },
        {
          featureType: 'transit.station',
          elementType: 'labels.text.fill',
          stylers: [{color: '#d59563'}],
        },
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [{color: '#17263c'}],
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [{color: '#515c6d'}],
        },
        {
          featureType: 'water',
          elementType: 'labels.text.stroke',
          stylers: [{color: '#17263c'}],
        },
      ];

     
  return (
    <View style={styles.main}>
      <View style={{height:Height * 0.70}}>
      <MapView
          zoomEnabled={true}
          zoomTapEnabled={true}
              style={styles.mapStyle}
              initialRegion={{
                  latitude: 28.5355,
                  longitude: 77.3910,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
              }}
              customMapStyle={mapStyle}>
              <Marker
              icon={imageConstant.location}
                  draggable
                  coordinate={{
                      latitude: 37.78825,
                      longitude: -122.4324,
                  }}
                  onDragEnd={
                      (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                  }
                  title={'Test Marker'}
                  description={'This is a description of the marker'}
              />
              
          </MapView>
          <View style={styles.view4}>
            <Image
            resizeMode='contain'
            style={styles.img1}
            source={imageConstant.groupLine}
            />
            <View style={{width:'85%'}}>
          <View style={styles.veiw1}>
                  <Text style={styles.text1}>Washington Street</Text>
                  
              </View>
              <View style={styles.veiw1}>
                
                  <Text style={styles.text1}>Museum of Art</Text>
              </View>
              </View>
              </View>
              </View>
              <View style={styles.view2}>
                <View style={styles.view5}>
              {/* <Pressable
              onPress={()=>setSelected(!Selected)}
               style={[styles.pres1,{backgroundColor:Selected ? colorConstant.theme :'#FFFFFF'}]}>
               </Pressable> */}
               <View style={{flexDirection:'row',alignItems:'center',width:Width * 0.69,justifyContent:'space-between'}}>
               <Text style={styles.text2}>Do you have someone with you?</Text>
               <CheckBox
                    disabled={false}
                    value={check}
                    onValueChange={()=>setCheck(!check)}
               />
               </View>
               </View>
               <CustomButton
               buttonName={"Continue"}
               borderRadius={9}
               width={'75%'}
               height={38}
               marginTop={'10%'}
               OnButtonPress={()=>props.navigation.navigate("CabBooking",{val:val})}
               marginBottom={20}
               />
                </View>
                
    </View>
  )
}

export default ConfirmSchedule
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF'
        // alignItems:'center'
    },
    mapStyle: {
        flex:2,
        // position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom:0,
      },
      veiw1:{
        width:'85%',
        borderRadius:10,
        marginTop:'5%',
        backgroundColor:'#EBE8FC',
        height:35,
        alignItems:'center',
        // justifyContent:'space-around',
        flexDirection:'row'
      },
      flatView:{
        height:83,
        width:Width,
        backgroundColor:'#FFFFFF',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
      },
      text1:{
        color:'#800080',
        fontSize:13,
        left:20,
        
      },
      textName:{
        color:'#000000',
        fontSize:17,
        fontWeight:'700',
        lineHeight:22
      },
      timeText:{
        fontSize:16,
        color:'#555555'
      },
      view2:{
        backgroundColor:'#FFFFFF',
     
        height:'25%',
      
        width:Width,
        alignItems:'center'
      
      },
      view3:{
        height:50,
        alignItems:'center',
        width:Width * 0.95,
        flexDirection:'row',
        justifyContent:'space-between',
        borderTopWidth:2,
        borderTopColor:'#0000001A'
      },
      view4:{
        width:Width,
        flexDirection:'row',
        alignSelf:'center',
        justifyContent:'space-between',
        alignItems:'center',
        marginTop:10,
        position:'absolute'
      },
      img1:{
        height:70,
        width:15,
        left:'22%',
        top:'3%'
      },
      pres1:{
        height:20,
        width:20,
        borderRadius:10,
        borderWidth:1.5,
        borderColor:colorConstant.theme
      },
      text2:{
        color:'#555555',
        fontSize:16,
        // left:7,
        right:'60%'
      },
      view5:{
        flexDirection:'row',
        width:Width * 0.60,
        justifyContent:'space-between',
        alignItems:'center',
        alignSelf:'center',
        marginTop:'5%'
      }
})

