// import regular from '../asset/fonts/regular.ttf'
import bgImage from '../asset/images/bgImage.png'
import location from '../asset/images/location.png'
import Carrental from '../asset/images/Carrental.png'
import Line from '../asset/images/Line.png'
import Google from '../asset/images/Google.png';
import Facebook from '../asset/images/Facebook.png'
import Apple from '../asset/images/Apple.png'
import Arrow from '../asset/images/Arrow.png'
import polygon from '../asset/images/Polygon.png'
import camera from '../asset/images/camera.png'
import info from '../asset/images/info.png'
import Illustration from '../asset/images/Illustration.png'
import check from '../asset/images/check.png'
import FaceRec from '../asset/images/FaceRec.png'
import Musical from '../asset/images/Musical.png'
import NoSmoking from '../asset/images/NoSmoking.png'
import Speech from '../asset/images/Speech.png'
import NoAnimals from '../asset/images/NoAnimals.png'
import cal from '../asset/images/cal.png'
import Car from '../asset/images/Car.png'
import downArrow from '../asset/images/downArrow.png'
import leftArrow from '../asset/images/leftArrow.png'
import Fog from '../asset/images/Fog.png'
import i from '../asset/images/i.png'
import Eye from '../asset/images/Eye.png'
import Vector from '../asset/images/Vector.png'
import backArrow from '../asset/images/backArrow.png'
import PayPal from '../asset/images/PayPal.png'
import nextArrow from '../asset/images/nextArrow.png'
import blackimg from '../asset/images/blackimg.png'
import flag from '../asset/images/flag.png'
import menuIcon from '../asset/images/menuIcon.png'
import profile from '../asset/images/profile.png'
import Zooms from '../asset/images/Zooms.png'
import Rect from '../asset/images/Rect.png'
import Tesla from '../asset/images/Tesla.png'
import Timer from '../asset/images/Timer.png'
import star from '../asset/images/star.png'
import profile2 from '../asset/images/profile2.png'
import ComingSoon from '../asset/images/ComingSoon.png'
import Phone from '../asset/images/Phone.png'
import schema from '../asset/images/schema.png'
import droite from '../asset/images/droite.png'
import Sand from '../asset/images/Sand.png'
import woman from '../asset/images/woman.png'
import Ok from '../asset/images/Ok.png'
import emergency from '../asset/images/emergency.png'
import schema2 from '../asset/images/schema2.png'
import Arrow290 from '../asset/images/Arrow290.png'
import ExpandArrow from '../asset/images/ExpandArrow.png'
import Profilewomen from '../asset/images/Profilewomen.png'
import cancel from '../asset/images/cancel.png'
import finger from '../asset/images/finger.png'
import FaceID from '../asset/images/FaceID.png'
import userIcon from '../asset/images/userIcon.png'
import calen from '../asset/images/calen.png'
import CreditCard from '../asset/images/CreditCard.png'
import arrowDown from '../asset/images/arrowDown.png'
import eyeslash from '../asset/images/eyeslash.png'
import profiledp from '../asset/images/profiledp.png'
import rightclick from '../asset/images/rightclick.png'
import passengercar from '../asset/images/passengercar.png'
import Arrow5 from '../asset/images/Arrow5.png'
import gpspoint from '../asset/images/gpspoint.png'
import redRight from '../asset/images/redRight.png'
import drawerback from '../asset/images/drawerback.png'
import logout from '../asset/images/logout.png'
import whiteArrow from '../asset/images/whiteArrow.png'
import walletback from '../asset/images/walletback.png'
import profileAccount from '../asset/images/profileAccount.png'
import Pencil from '../asset/images/Pencil.png'
import modalImage from '../asset/images/modalImage.png'
import taxi from '../asset/images/taxi.png'
import taxi2 from '../asset/images/taxi2.png'
import taxi3 from '../asset/images/taxi3.png'
import cross from '../asset/images/cross.png'
import forgetPassword from '../asset/images/forgetPassword.png'
import resetPassword from '../asset/images/resetPassword.png'
import Line2 from '../asset/images/Line2.png'
import chrono from '../asset/images/chrono.png'
import camera1 from '../asset/images/camera1.png'
import voice from '../asset/images/voice.png'
import groupLine from '../asset/images/groupLine.png'
import jpeg from '../asset/images/jpeg.png'
import story from '../asset/images/story.png'
import search from '../asset/images/search.png'
import pp from '../asset/images/pp.png'
import Ellipse from '../asset/images/Ellipse.png'
import tick from '../asset/images/tick.png'
import carRoof from '../asset/images/carRoof.png'
import loc from '../asset/images/loc.png'
import verticalLine from '../asset/images/verticalLine.png'
import cancelSmall from '../asset/images/cancelSmall.png'
import whiteEllipse from '../asset/images/whiteEllipse.png'
import Loading from '../asset/images/Loading.png'
import success from '../asset/images/success.png'
import map from '../asset/images/map.png'
import EDIT from '../asset/images/EDIT.png'
import upload from '../asset/images/upload.png'
import newLoc from '../asset/images/newLoc.png'
import profileDemo from '../asset/images/profileDemo.png'
import startPoint from '../asset/images/startPoint.png'
import endPoint from '../asset/images/endPoint.png'
import boarding2 from '../asset/images/boarding2.png'
const imageConstant={
    bgImage,
    location,
    Carrental,
    Line,
    Google,
    Facebook,
    Apple,
    Arrow,
    polygon,
    camera,
    info,
    Illustration,
    check,
    FaceRec,
    Musical,
    Speech,
    NoSmoking,
    NoAnimals,
    cal,
    Car,
    downArrow,
    leftArrow,
    i,
    Eye,
    Vector,
    backArrow,
    PayPal,
    nextArrow,
    blackimg,
    flag,
    menuIcon,
    profile,
    Zooms,
    Rect,
    Tesla,
    Timer,
    star,
    profile2,
    ComingSoon,
    Phone,
    schema,
    droite,
    Sand,
    woman,
    Ok,
    emergency,
    schema2,
    Arrow290,
    ExpandArrow,
    Profilewomen,
    cancel,
    finger,
    FaceID,
    userIcon,
    calen,
    CreditCard,
    arrowDown,
    eyeslash,
    profiledp,
    rightclick,
    passengercar,
    Arrow5,
    gpspoint,
    redRight,
    drawerback,
    logout,
    whiteArrow,
    walletback,
    profileAccount,
    Pencil,
    modalImage,
    taxi,
    taxi2,
    taxi3,
    cross,
    forgetPassword,
    resetPassword,
    Line2,
    chrono,
    camera1,
    voice,
    groupLine,
    jpeg,
    search,
    pp,
    story,
    Ellipse,
    tick,
    carRoof,
    loc,
    verticalLine,
    cancelSmall,
    whiteEllipse,
    Loading,
    success,
    map,
    EDIT,
    upload,
    newLoc,
    profileDemo,
    startPoint,
    endPoint,
    boarding2

}
const colorConstant={
    white:'#FFFFFF',
    blue:'#6C00C0',
    black:'#000000',
    theme:'#6C00C0'
}
const fontConstant = {
        thin:"SfProText-Thin",
        regular:"SfProText-Regular",
        semibold:"SfProText-Semibold",
        ultralight:"SfProText-Ultralight",
        bold:"SfProText-Bold"
}
export {colorConstant,fontConstant,imageConstant}