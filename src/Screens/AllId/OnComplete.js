import { View, Text, StyleSheet, Image, ScrollView } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import CustomButton from '../../custom/CustomButton'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useEffect,useState } from 'react'
import { S3Client, AbortMultipartUploadCommand, PutObjectCommand } from "@aws-sdk/client-s3";
import { baseUrl } from '../../utils/Urls'
const OnComplete = (props) => {
    const [userData,setUserData]=useState()
    const [idFrontDetails,setIdFrontDetails]=useState()
    const [idBackDetails,setIBackDetails]=useState()
    const client=new S3Client({region:"us-east-1"})
   
    useEffect(()=>{
     setTimeout(async()=>{
        let asyncBack=await AsyncStorage.getItem("idBack")
       let asyncFront=await AsyncStorage.getItem("idFront")
       let asynchUser=await AsyncStorage.getItem("user")
       setUserData(JSON.parse(asynchUser))
       setIdFrontDetails(JSON.parse(asyncFront))
       setIBackDetails(JSON.parse(asyncBack))

       
     },0)
        
    },[])
  
    
      const params = {
        keyPrefix: "passengers",
        bucket: "ladycar",
        region: "us-east-1",
        accessKey: "AKIAU4GHOSL2MMRJXJ5J",
        secretKey: "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",
        successActionStatus: 201,
      };
      const putCommand = new PutObjectCommand({
        Bucket:"ladycar",
        Key: "AKIAU4GHOSL2MMRJXJ5J",
        Body: 'Hello World!'
      })

    const awsData=async()=>{
        // const createCommand = new CreateBucketCommand({
        //     Bucket: bucketName
        //   })
        
         
        try{
            const data=await client.send(putCommand)
            .then((data)=>{
                console.log(data)
            })
        }
        catch(error){
            console.log("s3 Error",error)
           
                
        }
  
     
    }
const getsignUpData=async()=>{
    let data={
        address: userData?.Address,
        phoneNumber: userData?.Phone,
        name: userData.Name,
        dob: userData.DoB,
        email: userData.Email,
        idFront: idFrontDetails?.assets[0]?.uri,
        idBack: idBackDetails?.assets[0]?.uri,
        password: userData?.Password
    }
    try{
        let response=await fetch(baseUrl+"auth/passenger-signUp",{
            method:"POST",
            body:JSON.stringify(data),
            headers:{
                'content-type': 'application/json'
            }
        })
        .then(async(res)=>{
            let jsonData=await res.json()
            return jsonData
            
        })
        .then((res)=>{
            console.log("response--->",res)
           
            props.navigation.navigate("Login",{text:"signup"})
        })
        console.log(data)
    }
    catch(error){
        console.log(error)
    }
}
    const handleData=()=>{
        // awsData()
        // let asyncData=await AsyncStorage.getItem("idBack")
        console.log("Async data--->",idFrontDetails?.assets[0]?.fileName)
        console.log("Async Back id data--->",idBackDetails?.assets[0]?.fileName)
        console.log("Async User--->",userData.Name)
        getsignUpData()
    }
    // console.log("outside -->",userData.Name)
  return (
    <ScrollView
    bounces={false}
    showsVerticalScrollIndicator={false}
    style={{backgroundColor:'#FFFFFF'}}>
    <View style={styles.main}>
      <Text style={styles.welcomText}>LadyCar wishes 
you a great trip!</Text>
<Text style={styles.text}>
The time to confirm your profile and background check will take:
</Text>
<Image
source={imageConstant.Fog}
style={{marginTop:'3%'}}
/>
<Text style={styles.text1}>24 hours</Text>
<Image
resizeMode='contain'
style={{width:280,height:194,marginTop:'5%'}}

source={imageConstant.Illustration}/>
<View style={{height:24,marginTop:15,alignItems:'center',justifyContent:'center',width:24,backgroundColor:'#D9D9D9',borderRadius:15}}>
<Image
source={imageConstant.i}
resizeMode='contain'
style={{width:22,height:17}}
/>
</View>
<CustomButton
buttonName={'START'}
marginTop={'10%'}
borderRadius={10}
backgroundColor={'#7C7C7C'}
// OnButtonPress={()=>handleData()}
OnButtonPress={()=>props.navigation.navigate("Welcome")}
marginBottom={20}
/>
    </View>
    </ScrollView>
  )
}

export default OnComplete
const styles=StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
    },
    welcomText:{
        color:'#6C00C0',
        fontFamily:fontConstant.semibold,
        fontSize:27,
        width:'70%',
        lineHeight:37,
        textAlign:'center',
        marginTop:'17%'
    },
    text:{
        width:'75%',
        marginTop:'10%',
        fontSize:21,
        color:'#000000',
        textAlign:'center',
        fontFamily:fontConstant.semibold
    },
    text1:{
        color:colorConstant.theme,
        fontSize:40,
        fontFamily:fontConstant.semibold
    }
})