import { View, Text, StyleSheet, ScrollView, Image, ImageBackground, Pressable, Touchable } from 'react-native'
import React from 'react'
import { Width } from '../../dimensions/dimension'
import { fontConstant, imageConstant, colorConstant } from '../../utils/constant'
import CustomButton from '../../custom/CustomButton'
import { useState } from 'react'
import { useIsFocused } from '@react-navigation/native'
import { useEffect } from 'react'
import { baseUrl } from '../../utils/Urls'
import { useSelector } from 'react-redux'

const MyWallet = (props) => {
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [profilePic, setProfilePic] = useState("")
    const isFocus = useIsFocused()
    const { jwtToken } = useSelector((state) => state.reducers)
    useEffect(() => {
        if (isFocus) {
            getProfileData()
        }
        return () => {
            console.log("finish")
        }
    }, [isFocus])
    const getProfileData = async () => {
        let response = await fetch(baseUrl + "user/passenger-view-profile",
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application.json',
                    'Authorization': jwtToken
                }
            }
        )
            .then(async (res) => {
                let jsonData = await res.json()
                return jsonData
            })
            .then((res) => {
                console.log("my Account Get", res)
                // setProfile(res)

                setFirstName(res?.userData?.name)
                setLastName(res?.userData.lastName)
                setProfilePic(res?.userData?.profilePic)
            })
    }
    return (
        <View style={styles.main}>
            <ScrollView
                showsVerticalScrollIndicator={false}
                bounces={false}>
                <ImageBackground
                    resizeMode='contain'
                    style={styles.img1}
                    source={imageConstant.walletback}
                >

                    <Pressable
                        onPress={() => props.navigation.goBack()}
                        style={styles.img2}>
                        <Image
                            resizeMode='contain'
                            source={imageConstant.whiteArrow}

                        />

                    </Pressable>
                    <Text style={styles.text2}>{firstName} {lastName}</Text>
                    <Text style={[styles.text1, { marginTop: -20 }]}>Payments</Text>

                    <Image
                        style={styles.img3}
                        source={profilePic ? { uri: profilePic } : imageConstant.profileDemo}
                    />


                </ImageBackground>
                <Image
                    style={styles.img5}
                    source={imageConstant.CreditCard} />
                <View style={styles.view3}>
                    <Pressable
                        onPress={() => props.navigation.navigate('AddCard')}
                        style={styles.view4}>
                        <Text style={styles.text3}>+</Text>
                    </Pressable>
                    <Text style={styles.text4}>Add New Card</Text>
                </View>
                <CustomButton
                    buttonName={"SET PAYMENT METHOD"}
                    alignSelf={'center'}
                    marginTop={'40%'}
                    borderRadius={9}
                    width={'85%'}
                    marginBottom={20}
                    OnButtonPress={() => props.navigation.goBack()}
                />
            </ScrollView>
        </View>
    )
}

export default MyWallet
const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        // alignItems:'center'
    },
    img1: {
        // height:221,
        width: Width,
        aspectRatio: 1 / 0.562
    },
    img2: {
        width: 29,
        left: '5%',
        top: '9%'

    },
    text1: {
        color: '#FFFFFF',
        fontSize: 22,
        // fontFamily:fontConstant.semibold,
        fontWeight: '500',
        alignSelf: 'center',
        top: '15%'
    },

    img3: {
        marginTop: '32%',
        borderWidth: 5,
        borderColor: '#FFFFFF',
        borderRadius: 50,
        height: 103,
        width: 103,
        alignSelf: 'center'
    },
    img4: {
        alignSelf: 'center',
        left: '6%',
        width: 28,
        height: 28,
        backgroundColor: '#D9D9D9',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 14,
        marginTop: -20

    },
    view1: {
        alignSelf: 'flex-start',
        left: '5%',
        marginTop: '20%'
    },
    img5: {
        marginTop: '20%',
        alignSelf: 'center',

    },
    text2: {
        color: '#FFFFFF',
        alignSelf: 'center',
        fontSize: 17,
        marginTop: 3
    },
    view3: {
        width: Width * 0.4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: '8%',
        alignSelf: 'center'
    },
    view4: {
        height: 30,
        width: 30,
        borderRadius: 7,
        backgroundColor: '#F7EDFF',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text3: {
        fontSize: 22,
        color: '#6C00C0'
    },
    text4: {
        fontSize: 17,
        color: '#606060'
    },
    pres1: {
        height: 30,
        width: 110,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: '10%',
        borderRadius: 10
    },
    presText: {
        color: colorConstant.theme,
        fontSize: 12
    }

})