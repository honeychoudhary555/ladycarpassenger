import { View, Text ,StyleSheet, Image, TouchableOpacity, TextInput, Modal, ScrollView, Keyboard} from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import CustomHeader from '../../custom/CustomHeader'
import CustomInput from '../../custom/CustomInput'
import { useState } from 'react'
import CustomButton from '../../custom/CustomButton'
import { ReactNativeBiometricsLegacy } from 'react-native-biometrics'
import { Height, Width } from '../../dimensions/dimension'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import CheckBox from '@react-native-community/checkbox'
import { useDispatch, useSelector } from 'react-redux'
import { action } from '../../redux/reducers'
import { useEffect } from 'react'
import { baseUrl } from '../../utils/Urls'
import LottieView from 'lottie-react-native';
import toastShow from '../../utils/Toast'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useFocusEffect, useIsFocused } from '@react-navigation/native'
import { set } from 'react-native-reanimated'
import { ImageUploadInBucket } from '../../custom/ImageUploadInBucket'
// import LottieView from 'lottie-react-native';

const WriteAbout = (props) => {
  let id
    if(props.route.params)
    {
      id=props.route.params.txt
    }
    const [isSelected, setSelection] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [modalVisible2, setModalVisible2] = useState(false);
    const [firstName,setFirstName]=useState("")
    const [lastName,setLastName]=useState("")
    const [check,setCheck]=useState(false)
    const [check1,setCheck1]=useState(false)
    const [check2,setCheck2]=useState(false)
    const [check3,setCheck3]=useState(false)
    const [visible,setVisible]=useState(false)
    const [padding,setPadding]=useState(10)
    const [visible1,setVisible1]=useState(false)
    const [userComment,setUserComment]=useState("")
    const [demo,setDemo]=useState(false)
    let [profileData,setProfileData]=useState(null)
    const dispatch=useDispatch()
    const isFocused=useIsFocused()
    let {jwtToken}=useSelector((state)=>state.reducers)
    useEffect(()=>{
      let sub=Keyboard.addListener('keyboardWillShow',(e)=>{
        setPadding(e.endCoordinates.height+10)
      })
      let subs=Keyboard.addListener('keyboardWillHide',()=>{
        setPadding(10)
      })
    },[])
    useEffect(() => {
      if(isFocused){
      getProfileData()
      }
      return()=>{
          console.log("finis effect")
      }
      // getProfile()
  }, [isFocused])
    useFocusEffect(
      React.useCallback(() => {
        getProfile()

      }, [demo])
    );
    const getProfile=async()=>{
      setVisible1(true)
      setProfileData(JSON.parse(await AsyncStorage.getItem("Profile")))
      if(id=="WriteAbout")
      {
      setUserComment(profileData?.userData?.about_you)
      setCheck(profileData?.userData?.is_smoking)
      setCheck1(profileData?.userData?.is_music)
      setCheck2(profileData?.userData?.is_chat)
      setCheck3(profileData?.userData?.is_pet)
      setDemo(profileData?.userData?.is_pet)
     
      }
      setVisible1(false)
    }


    const onProfilePhoto = ()=>{
      setModalVisible(false);
      setTimeout(()=>{
        openCamera();
      },100)
    }
    const openCamera=async()=>{
      let options={
          mediaType:'photo',
          cameraType:'back',
          saveToPhotos:true	,
          quality:0.9,
          maxWidth:500,
          maxHeight:500
      }
      // launchImageLibrary(options,(response)=>{
      //     console.log('response', response)
      // })
      launchCamera(options,async(response)=>{
        if (response.didCancel) {
          // toast('User cancelled ', colorConstant.darkRed)
          console.log('User cancelled image picker');
      } else if (response.error) {
          // toast('Something went wrong', colorConstant.darkRed)
          console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);

      } else {
        console.log("response",response);
        
        
          console.log('response from gallery------------------->', JSON.stringify(response));
          let formDataReq = new FormData();
          let data = {
              uri:response?.assets?.[0]?.uri,
              type:response?.assets?.[0]?.type,
              name:response?.assets?.[0]?.fileName
          }
          let imageData = await ImageUploadInBucket(data)
          console.log("image front wala", imageData)
          // formDataReq.append("file",data);
          // console.log("FormWala",formDataReq._parts[0][1])
          // handleData(response?.assets?.[0]?.uri)
          handleData(imageData?.postResponse?.location)
          // updateProfileImage(formDataReq);
      }

      })
      
     
  }

  
  const handleNavData=()=>{
    setModalVisible2(false)
    
    setTimeout(()=>{
      dispatch(action.setIntroStatus("main"))
    },200)
    
  }

  const handleData=async(imageItem)=>{
    setVisible(true)
    try{
      console.log("image",imageItem)
    let finalFormData=new FormData()
    data={
      "about_you": userComment,
    "profilePic": imageItem,
    "is_smoking": check,
    "is_music": check1,
    "is_chat": check2,
    "is_pet": check3
    }
    finalFormData.append("about_you",userComment)
    finalFormData.append("profilePic",imageItem)
    finalFormData.append("is_smoking",check,)
    finalFormData.append( "is_music",check1,)
    finalFormData.append("is_chat",check2,)
    finalFormData.append("is_pet",check3)
    console.log("final wala--->",finalFormData)
    await fetch(baseUrl+"user/passenger-about-update",
    {
      method:"POST",
      headers:{
        'Content-Type': 'application/json',
        "Authorization":jwtToken,
      },
      body:JSON.stringify(data)
    }
)
.then(async(res)=>{
  let jsonData= await res.json()
  return jsonData
})
.then(async(res)=>{
  console.log("WriteAbout Data",res)
  await AsyncStorage.setItem("Profile",JSON.stringify(res))
  setVisible(false)
  setModalVisible2(!modalVisible2)


})
    }
    catch(err)
    {
      setVisible(false)
      toastShow("please upload again")
    }
    
  }

  console.log("JWT",jwtToken)
  const handleProfile=()=>{
if(id!="WriteAbout")
    {
      if(userComment=="")
      {
        toastShow("please provide information about you")
      }
      else{
        setModalVisible(true)
      }
     
    }
    else{
      profileUpdate()
    }
   
   
  }

  const profileUpdate = async () => {
    let data = {
      about_you:userComment,
      is_smoking:check,
      is_music:check1,
      is_chat:check2,
      is_pet:check3
    }
    console.log(data)
    await fetch(baseUrl+"user/passenger-edit-profile",
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': jwtToken
            },
            body:JSON.stringify(data)
        })
        .then(async(res)=>{
            let jsonData=await res.json()
            return jsonData
        })
        .then(async(res)=>{
            console.log("setProfile wala",res)
            props.navigation.navigate("Profile")
            await AsyncStorage.setItem("Profile",JSON.stringify(profileData))
        })
}




const getProfileData = async () => {
  let response = await fetch(baseUrl + "user/passenger-view-profile",
      {
          method: "GET",
          headers: {
              'Content-Type': 'application.json',
              'Authorization': jwtToken
          }
      }
  )
      .then(async (res) => {
          let jsonData = await res.json()
          return jsonData
      })
      .then((res) => {
          console.log("my Account Get", res)
          // setProfile(res)
      
          setFirstName(res.userData.name)
          setLastName(res.userData.lastName)
      })
}

  return (
    <View style={styles.main}>
        {/* <CustomHeader
         onPressBack={()=>props.navigation.goBack()} 
        /> */}
        <ScrollView
        contentContainerStyle={{backgroundColor:'#FFFFFF',alignItems:'center',marginTop:'8%',paddingBottom:padding}}
        >

      
      <Text style={styles.welcomText}>Write about you</Text>
      <TextInput style={styles.textinput}
      multiline={true}
      // caretHidden={true}
      placeholder='What do you like to do? Hobbies? Where are you from? etc'
     placeholderTextColor='#5C5C5C'
     onChangeText={(e)=>setUserComment(e)}
     value={userComment}
     >
      </TextInput>
      <View style={{flexDirection:'row',width:229,justifyContent:'space-around',marginTop:'3%'}}>
        <View>
            <Image 
            style={{height:25,width:25}}
            source={imageConstant.NoSmoking}/>
            <CheckBox
              value={check}
              style={{right:4,top:5}}
              onValueChange={()=>setCheck(!check)}
            />
            {/* <TouchableOpacity activeOpacity={0.9} onPress={()=>setSelection(1)} style={{width:20,marginTop:5,height:20,borderWidth:1.5,borderRadius:10,borderColor:colorConstant.theme,
                    backgroundColor:isSelected==1 ? colorConstant.theme : null}}></TouchableOpacity> */}
        </View>
        <View>
            <Image 
            style={{height:25,width:25}}
            source={imageConstant.Musical}/>
            <CheckBox
            style={{right:4,top:5}}
              value={check1}
              onValueChange={()=>setCheck1(!check1)}
            />
             {/* <TouchableOpacity activeOpacity={0.9} onPress={()=>setSelection(2)} style={[styles.touch,{backgroundColor:isSelected==2 ? colorConstant.theme : null}]}></TouchableOpacity> */}
        </View>
        <View>
            <Image 
            style={{height:25,width:25}}
            source={imageConstant.Speech}/>
            <CheckBox
            value={check2}
            style={{right:4,top:5}}
            onValueChange={()=>setCheck2(!check2)}
            />
             {/* <TouchableOpacity activeOpacity={0.9} onPress={()=>setSelection(3)} style={[styles.touch,{backgroundColor:isSelected==3 ? colorConstant.theme : null}]}></TouchableOpacity> */}
        </View>
        <View>
            <Image 
            style={{height:25,width:25}}
            source={imageConstant.NoAnimals}/>
             {/* <TouchableOpacity activeOpacity={0.9} onPress={()=>setSelection(4)} style={[styles.touch,{backgroundColor:isSelected==4 ? colorConstant.theme : null}]}></TouchableOpacity> */}
             <CheckBox
               value={check3}
               style={{right:4,top:5}}
               onValueChange={()=>setCheck3(!check3)}
             />
        </View>
      </View>
      <CustomButton
      buttonName={'CONTINUE'}
      width={'80%'}
      marginTop={'50%'}
      borderRadius={10}
      // borderColor={'white'}
      // borderWidth={1}
      shadowOffset={{width:2,height:2}}
      OnButtonPress={()=>handleProfile()}
      // OnButtonPress={handleData}
      marginBottom={20}
      />
      <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            // alert("Modal has been closed.");
            setModalVisible(!modalVisible);}}
      >
        <View style={styles.modalview1}>
            <View style={styles.modalview2}>
              <Image
              source={imageConstant.profiledp}
              style={{marginTop:'10%'}}
              />
              <Text style={styles.modalText1}>Hi "{firstName} {lastName}"</Text>
              <Text style={styles.modalText2}>It is required to upload a profile picture on your LadyCar's account</Text>
              <Text style={[styles.modalText2,{fontFamily:fontConstant.regular}]}>LadyCar’s goal is safety for passengers and drivers. To ensure safe trips, please upload or take a profile picture to your account.</Text>
              <CustomButton
              buttonName={"Profile photo"}
              marginTop={'10%'}
              borderRadius={10}
              OnButtonPress={onProfilePhoto}
              />
            </View>
        </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible2}
          onRequestClose={() => {
            // alert("Modal has been closed.");
            setModalVisible2(false);}}
      >
        <View style={styles.modalview1}>
            <View style={styles.modalview2}>
              <Image
              source={imageConstant.rightclick}
              style={{marginTop:'10%'}}
              />
              <Text style={styles.modalText1}>Congratulations!</Text>
              <Text style={[styles.modalText2,{fontSize:17,width:Width *0.80}]}>Your Profile is all set</Text>
              <Text style={[styles.modalText2,{fontFamily:fontConstant.regular}]}>Safety is LadyCar's priority for
passengers and drivers. You can now drive with LadyCar and enjoy your trips!</Text>
              <CustomButton
              buttonName={"Continue"}
              marginTop={'10%'}
              borderRadius={10}
              OnButtonPress={()=>handleNavData()}

              />
            </View>
        </View>
        </Modal>
        </ScrollView>
        <Modal
        visible={visible}
        transparent={true}
        onRequestClose={()=>setVisible(!visible)}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            backgroundColor:'rgba(0,0,0,0.6)'
          }}
        >
          <View>
            <LottieView source={require('../../asset/images/loader.json')} style={{ height:100 }} autoPlay />
          </View>
        </View>
      </Modal>
      <Modal
        visible={visible1}
        transparent={true}
        onRequestClose={()=>setVisible(!visible1)}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            backgroundColor:'rgba(0,0,0,0.6)'
          }}
        >
          <View>
            <LottieView source={require('../../asset/images/loader.json')} style={{ height:100 }} autoPlay />
          </View>
        </View>
      </Modal>
    </View>
  )
}

export default WriteAbout
const styles=StyleSheet.create({
    main:{
        flex:1,
        // alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
    welcomText:{
        color:'#6C00C0',
        fontFamily:fontConstant.semibold,
        fontSize:29,
        width:'80%',
        lineHeight:37,
        textAlign:'center',
        marginTop:'9%'
    },
    touch:{
      width:20,
      marginTop:5,
      height:20,
      borderWidth:1.5,
      borderRadius:10,
      borderColor:colorConstant.theme,
      
    },
    textinput:{
      height:221,
      width:259,
      alignItems:'center',
      borderWidth:1,
      borderColor:colorConstant.theme,
      marginTop:'10%',
      borderRadius:15,
      // textAlign:'center',
      textAlignVertical:'top',
      paddingHorizontal:15,
      paddingVertical:15,
    },
    modalview1:{
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22,
      backgroundColor:"rgba(0,0,0,0.26)"
    },
    modalview2:{
      height:Height * 0.7,
      width:Width * 0.88,
      alignItems:'center',
      backgroundColor:'#FFFFFF'
    },
    modalText1:{
      color:colorConstant.theme,
      alignSelf:'flex-start',
      left:'10%',
      fontSize:18,
      fontFamily:fontConstant.semibold,
      marginTop:'10%'
    },
    modalText2:{
      color:colorConstant.black,
      fontSize:16,
      width:Width * 0.70,
      alignSelf:'flex-start',
      left:'10%',
      marginTop:'5%',
      fontFamily:fontConstant.semibold,
      textAlign:'left'
    }
})