import { View, Text, StyleSheet, Image, ScrollView, Pressable, Platform, Keyboard } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import CustomInput from '../../custom/CustomInput'
import CustomButton from '../../custom/CustomButton'
import { Width } from '../../dimensions/dimension'
import { useDispatch } from 'react-redux'
import { action } from '../../redux/reducers'
import { useEffect } from 'react'
import { useState } from 'react'
import auth from '@react-native-firebase/auth';
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin'
import { asin, Value } from 'react-native-reanimated'
import { baseUrl, webClientId } from '../../utils/Urls'
import toastShow from '../../utils/Toast'
import validation from '../../utils/validation'
import { appleAuth } from '@invertase/react-native-apple-authentication';
import PhoneInput from 'react-native-phone-number-input'
const Login = (props) => {
  const [padding, setPadding] = useState(10)
  const [loggedIn, setloggedIn] = useState(false);
  const [userInfo, setuserInfo] = useState(null);
  const [phoneValue, setPhoneValue] = useState("")
  const [callingCode,setCallingCode]=useState(1)
  const [passwordValue, setPasswordValue] = useState("")
  const dispatch = useDispatch()
  useEffect(() => {
    let sub = Keyboard.addListener('keyboardWillShow', (e) => {
      setPadding(e.endCoordinates.height + 10)
    })
    let subs = Keyboard.addListener('keyboardWillHide', () => {
      setPadding(10)
    })
  }, [])
  useEffect(() => {
    GoogleSignin.configure({
      scopes: [],
      webClientId: webClientId,
      offlineAccess: true,
    })
  }, [])
  let navtext = props?.route?.params.text
  const handleNav = () => {
    if (navtext == "signup") {
      props.navigation.navigate('WriteAbout')
    }
    else {
      // props.navigation.navigate('ScheduleTrip')
      // alert("ok")
      dispatch(action.setIntroStatus("main"))
    }
  }

  //**** Apple Login *** */
  const LoginWithAppleId = async () => {
    // performs login request
    try {

      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        // Note: it appears putting FULL_NAME first is important, see issue #293
        requestedScopes: [appleAuth.Scope.FULL_NAME, appleAuth.Scope.EMAIL],
      });
      console.log(appleAuthRequestResponse.user, 'datatajdhjd');



      // get current authentication state for user
      // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
      const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);



      // use credentialState response to ensure the user is authenticated
      if (credentialState === appleAuth.State.AUTHORIZED) {
        // user is authenticated
        loginWithApple(appleAuthRequestResponse.user)
      }


    } catch (error) {
      toastShow("Something went wrong please try again")
    }


  }



  const loginData = async () => {
    if (validation.phoneValid(phoneValue)

    ) {
      let passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
      if (passwordValue == "") {
        toastShow("Please Enter Password")
        // return false
      }
      // else if(!passwordRegex.test(data)){
      //     toastShow("please ente")
      //     return false
      // }
      else {
        console.log(phoneValue, passwordValue)
        data = {
          phoneNumber:callingCode+phoneValue,
          password: passwordValue
        }
        try {
          let resposne = await fetch(baseUrl + "auth/passenger-login", {
            method: "POST",
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
          })
            .then(async (res) => {
              let jsonData = res.json()
              return jsonData;
            })
            .then((res) => {
              if (res.code == "200") {
                if (res?.activeUser.is_aboutInfo == false && res.code == 200) {
                  props.navigation.navigate('WriteAbout')
                  dispatch(action.setJwtToken(res.token))
                }
                else {
                  dispatch(action.setIntroStatus("main"))
                  dispatch(action.setJwtToken(res.token))
                }
              }
              else {
                toastShow(res.message, colorConstant.theme)
              }
              console.log(res)
            })
        }
        catch (err) {
          console.log(err)
        }
      }
    }

  }


  const loginWithApple = async (userInfo) => {
    // alert(JSON.stringify(userInfo))
    const objToSend = {
      signupType: "APPLE",
      socialId: userInfo,
    };
    try {
      const response = await LoginWithGoogleServices(objToSend);
      console.log(response.data, 'jhdkjsfhjksdfhsdjkf');
      await AsyncStorage.setItem("token", response.data.activeUser.jwtToken)
      if (response.data.activeUser.is_aboutInfo) {
        dispatch(action.setIntroStatus("main"))
      }
      else {
        props.navigation.navigate('WriteAbout')
      }



    } catch (error) {
      toastShow("User are not exits please sign up", colorConstant.errorRed);
      console.log(error);
      console.log(objToSend, "hdskjdshfk");

    }
  };

  // ********* Google SignIn **************

  const signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      console.log("reached google sign in");

      // await GoogleSignin.signOut()
      const userInf = await GoogleSignin.signIn();
      // const newData=await GoogleSignin.getCurrentUser()
      // console.log("wwwwww",newData)
      // setuserInfo(userInf)
      // setloggedIn(true)
      // alert("ok")
      console.log("userInf", userInf)
      googleLogin(userInf?.user.id)
    }
    catch (err) {
      if (err.code === statusCodes.IN_PROGRESS) {
        console.log('Signin in progress');

      }
      else if (err.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('PLAY_SERVICES_NOT_AVAILABLE')
      }
      else {
        console.log("err", err)
      }
    }
  }
  const googleLogin = async (userid) => {
    let data = {
      signupType: "GOOGLE",
      socialId: userid
    }
    console.log(data)
    await fetch(baseUrl + "auth/passenger-social-login", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(async (res) => {
        let jsonData = await res.json()
        return jsonData
      })
      .then((res) => {

        console.log("google wala", res)
        if (res.code == "200") {
          if (res?.activeUser.is_aboutInfo == false && res.code == 200) {
            props.navigation.navigate('WriteAbout')
            dispatch(action.setJwtToken(res.token))
            signOut()
          }
          else {
            dispatch(action.setIntroStatus("main"))
            dispatch(action.setJwtToken(res.token))
          }
        }
        else {
          toastShow(res.message)
          signOut()
          // 

        }
        if (res.code == "404") {
          toastShow(res.message, colorConstant.theme)
        }
      })
  }

  signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      //   setloggedIn(false);
      //   setuserInfo([]);
    } catch (error) {
      console.error(error);
    }
  };



  // ***** faceboook login

  const loginWithFaceBook = async (userInfo) => {
    alert(userInfo, 'sjsjsjsjs')
    const objToSend = {
      signupType: "FACEBOOK",
      socialId: userInfo,
    };
    try {
      const response = await LoginWithGoogleServices(objToSend);
      console.log(response.data, 'jhdkjsfhjksdfhsdjkf');
      await AsyncStorage.setItem("token", response.data.activeUser.jwtToken)
      if (response.data.activeUser.is_aboutInfo) {
        disptach(actions.setIntroStatus("main"));
      }
      else {
        props.navigation.navigate("AboutDriver", { userDatils: response.data });
      }

    } catch (error) {
      toastShow("User are not exits please sign up", colorConstant.errorRed);
      console.log(error);
      console.log(objToSend, "hdskjdshfk");

    }
  }


  const fbLogin = (response) => {
    LoginManager.logOut()
    return LoginManager.logInWithPermissions(["email", "public_profile"]).then(
      result => {
        console.log(result, 'jhdjkdfhkjdshfkjdshfkjsdhfkjdsfhkjdsfh')
        if (result.declinedPermissions && result.declinedPermissions.includes("email")) {
          response("Message Email is require")
        }
        if (result.isCancelled) {
          console.log("error")
        }
        else {
          const res = new GraphRequest(
            '/me?fields=email,name,picture',
            null,
            response
          );
          new GraphRequestManager().addRequest(res).start()
        }
      },
      function (error) {
        console.log(error);
      }
    )
  }

  const onFbLogin = async () => {
    try {
      await fbLogin(_responceCallBack)
    } catch (error) {
      console.log(err
      );
    }
  }

  const _responceCallBack = async (error, result) => {
    if (error) {
      toastShow("Facebook Login error")
    }
    else {
      const userData = await result

      loginWithFaceBook(userData.id)
      console.log(userData, 'this is user data')
    }
  }

  console.log("user info ", userInfo, loggedIn)
  return (
    <ScrollView
      contentContainerStyle={{ paddingBottom: padding, backgroundColor: '#FFFFFF' }}
      style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
      <View style={styles.main}>
        <Text style={styles.welcomText}>Your Account Is Ready!</Text>
        <Image
          source={imageConstant.Illustration}
          style={styles.img}
        />
        <Text style={styles.text}>Log In To Start The Adventure</Text>
        {/* <CustomInput
          height={40}
          marginTop={'5%'}
          width={'85%'}
          borderColor={colorConstant.theme}
          onChangeText={(e)=>setPhoneValue(e)}
          keyboardType={"numeric"}
          // keyboardType={}
          placeholder="Phone Number" /> */}
        <PhoneInput
          // defaultValue={phoneNumber}
          defaultCode="US"
          onChangeCountry={(c) => {
            setCallingCode(c.callingCode[0])
            // setCountry(c.name)
          }}
          layout="first"

          onChangeText={(text) => {
            setPhoneValue(text);
            // setUserData({ ...userData, Phone: text })
          }}
          placeholder='Phone Number'

          //  onChangeFormattedText={(text) => {
          //    // setFormattedValue(text);
          //  }}
          withDarkTheme
          //   withShadow
          //   autoFocus
          textInputStyle={{
            paddingVertical: 0,
            height: 20

          }}
          containerStyle={styles.phoneContainer}
          flagButtonStyle={styles.flag}
          codeTextStyle={{ height: 0, width: 0 }}
          textContainerStyle={styles.textContainer}
        />
        <CustomInput
          height={40}
          imageName={imageConstant.Eye}
          marginTop={'5%'}
          width={'85%'}
          borderColor={colorConstant.theme}
          imgFlag={true}
          imgmarginTop={2}
          onChangeText={(e) => setPasswordValue(e)}
          placeholder="Password" />
        <Pressable style={{paddingVertical:5}} onPress={() => props.navigation.navigate("ForgetPassword", { text: navtext })}>
          <Text style={styles.forgetText}>I forgot my password</Text>
        </Pressable>
        <CustomButton
          buttonName={"CONFIRM"}
          // width={'85%'}
          marginTop={'5%'}
          borderRadius={10}
          // OnButtonPress={() => handleNav()}
          OnButtonPress={() => loginData()}
        />
        {/* <View style={styles.imageView}>
      <Image
        source={imageConstant.FaceID}
        />
        <Image
        source={imageConstant.finger}/>
       
      </View> */}
        <CustomButton
          imageFlag={true}
          buttonName={'Continue with Face ID'}
          backgroundColor={'#FFFFFF'}
          marginTop={20}
          // width={'85%'}
          color={'#000000'}
          borderColor={colorConstant.theme}
          borderWidth={1}
          borderRadius={10}
          imageName={imageConstant.FaceID}
        />
        <CustomButton
          imageFlag={true}
          buttonName={'Continue with Google'}
          backgroundColor={colorConstant.black}
          marginTop={20}
          // width={'85%'}
          borderRadius={10}
          imageName={imageConstant.Google}
          OnButtonPress={() => signIn()}
        />
        <CustomButton
          imageFlag={true}
          buttonName={'Continue with Facebook'}
          backgroundColor={colorConstant.black}
          marginTop={20}
          // width={'85%'}
          borderRadius={10}
          imageName={imageConstant.Facebook}
        // marginBottom={20}
        />
        {Platform.OS === 'ios' ? <CustomButton
          imageFlag={true}
          buttonName={'Continue with Apple'}
          backgroundColor={colorConstant.black}
          marginTop={20}
          // width={294}
          borderRadius={10}
          imageName={imageConstant.Apple}
          marginBottom={20}
          OnButtonPress={LoginWithAppleId}
        />
          : null}
      </View>
    </ScrollView>

  )
}

export default Login
const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  },
  welcomText: {
    color: '#6C00C0',
    fontFamily: fontConstant.semibold,
    fontSize: 29,
    width: '85%',
    lineHeight: 37,
    textAlign: 'center',
    marginTop: '10%'
  },

  img: {
    width: 280,
    marginTop: '5%'
  },
  text: {
    color: '#6C00C0',
    fontFamily: fontConstant.semibold,
    fontSize: 23,
    width: '85%',
    lineHeight: 37,
    textAlign: 'center',
    marginTop: '5%'
  },
  forgetText: {
    color: colorConstant.theme,
    fontSize: 12,
    right: '26%',
    top: '45%',
    textDecorationLine: 'underline',
    fontWeight: '400'
  },
  imageView: {
    flexDirection: 'row',
    marginTop: '7%',
    width: Width * 0.55,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  phoneContainer: {
    marginTop: 20,
    borderWidth: 1.5,
    borderColor: colorConstant.theme,
    borderRadius: 10,
    width: "86%",
    height: 40

  },
  flag: {
    paddingVertical: 0,
    borderRadius: 10,
    // height:50,
    height: 35,
  },
  textContainer: {
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10
  },
})