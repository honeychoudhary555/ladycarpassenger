import { View, Text, ImageBackground, StyleSheet, Image, TextInput, Pressable, Modal, } from 'react-native'
import React from 'react'
import { fontConstant, imageConstant } from '../../utils/constant'
import { Width } from '../../dimensions/dimension'
import { ScrollView } from 'react-native-gesture-handler'
import { useState } from 'react'
import CustomButton from '../../custom/CustomButton'
import { SafeAreaView } from 'react-native-safe-area-context'
import { baseUrl } from '../../utils/Urls'
import { useSelector } from 'react-redux'
import { useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import toastShow from '../../utils/Toast'
import LottieView from 'lottie-react-native';
import validation from '../../utils/validation'
import { ImageUploadInBucket } from '../../custom/ImageUploadInBucket'
import { useIsFocused } from '@react-navigation/native'
const MyAccount = (props) => {
    const [status, setStatus] = useState(false)
    const [Profile, setProfile] = useState(null)
    const [profileData, setProfileDatas] = useState(null)
    const { jwtToken } = useSelector((state) => state.reducers)
    const [imageData, setImageData] = useState("")
    const [modalVisible, setModalVisible] = useState(false)
    const isFocus = useIsFocused()
    const [user, setUser] = useState({
        name: "",
        phone: "",
        email: "",
        password: "",
        profilePic: "",
        lastName: ""
    })
    const [value, setValue] = useState(false)
    useEffect(() => {
        if (isFocus) {
            getProfileData()
        }
        return () => {
            console.log("finis effect")
        }
        // getProfile()
    }, [isFocus])

    const openCamera = async () => {

        // alert('hello')
        let options = {
            mediaType: 'photo',
            cameraType: 'back',
            saveToPhotos: true,
            quality: 0.9,
            maxHeight: 500,
            maxWidth: 500,

        }
        // launchImageLibrary(options,(response)=>{
        //     console.log('response', response)
        // })
        // launchCamera(options,(res)=>{
        //     console.log(res)
        // })
        launchCamera(options, async (response) => {
            if (response.didCancel) {
                // toast('User cancelled ', colorConstant.darkRed)
                console.log('User cancelled image picker');
            } else if (response.error) {
                // toast('Something went wrong', colorConstant.darkRed)
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);

            } else {
                //   setModalVisible(true)
                console.log("response", response);

                console.log('response from gallery------------------->', JSON.stringify(response));
                let formDataReq = new FormData();
                let data = {
                    uri: response?.assets?.[0]?.uri,
                    type: response?.assets?.[0]?.type,
                    name: response?.assets?.[0]?.fileName
                }
                let imageData = await ImageUploadInBucket(data)

                setImageData(imageData?.postResponse?.location)
                setUser({
                    ...user,
                    profilePic: imageData?.postResponse?.location
                })

            }

        })


    }

    const getImageData2 = async (imageName) => {
        // let data={
        //   fileName:imageName
        setModalVisible(true)
        // }
        try {
            let finalFormData = new FormData()
            finalFormData.append("fileName", imageName)
            console.log("image wala", finalFormData)
            await fetch(baseUrl + "auth/image-upload",
                {
                    method: "POST",
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Accept': 'application/json'
                    },
                    body: finalFormData
                }
            )
                .then(async (res) => {
                    let jsonData = await res.json()
                    return jsonData;
                })
                .then(async (res) => {
                    console.log("resssss", res)

                    setImageData(res?.data)
                    setUser({
                        ...user,
                        profilePic: res?.data
                    })
                    setModalVisible(false)
                    console.log("my account mai", imageData)
                    //   await AsyncStorage.setItem("idBack",JSON.stringify(res.data))
                    //   setModalVisible(false)
                    //   setStatus(true)
                    //   props.navigation.navigate("FaceId",{id:"Passport"})
                })
        }
        catch (err) {
            setModalVisible(false)
            toastShow("please upload image again")
        }


    }
    // const getProfile=async()=>{
    //     let response=await fetch(baseUrl+"user/passenger-view-profile",{
    //         method:"GET",
    //         headers:{
    //             'Authorization':jwtToken
    //         }
    //     })
    //     .then(async(res)=>{
    //         let jsonData=await res.json()
    //         return jsonData
    //     })
    //     .then(async(res)=>{
    //         setProfileDatas(res)
    //         console.log("propfile ka data",profileData)
    //         await AsyncStorage.setItem("Profile",JSON.stringify(res))
    //     })
    //     // setProfileDatas(JSON.parse(await AsyncStorage.getItem("Profile")))
    //   }
    console.log("profile", Profile?.userData.name)
    const getProfileData = async () => {
        let response = await fetch(baseUrl + "user/passenger-view-profile",
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application.json',
                    'Authorization': jwtToken
                }
            }
        )
            .then(async (res) => {
                let jsonData = await res.json()
                return jsonData
            })
            .then((res) => {
                console.log("my Account Get", res)
                // setProfile(res)
                setUser({
                    ...user,
                    name: res.userData.name,
                    lastName: res.userData.lastName,
                    phone: res?.userData?.phoneNumber,
                    email: res?.userData?.email,
                    password: res?.userData?.planePassword,
                    profilePic: res?.userData?.profilePic
                })
            })
    }
    const setProfileData = async () => {
        if (validation.nameValid(user.name)

        ) {
            let data = {
                name: user?.name,
                lastName: user?.lastName,
                phoneNumber: user?.phone,
                email: user.email,
                profilePic: user.profilePic
            }

            await fetch(baseUrl + "user/passenger-edit-profile",
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': jwtToken
                    },
                    body: JSON.stringify(data)
                })
                .then(async (res) => {
                    let jsonData = await res.json()
                    return jsonData
                })
                .then(async (res) => {
                    console.log("setProfile wala", res)
                    setStatus(false)
                    // getProfile()
                    await AsyncStorage.setItem("Profile", JSON.stringify(res))
                    getProfileData()
                })
        }
    }
    return (
        <SafeAreaView style={styles.main} forceInset={{ top: 'always' }}>
            <ScrollView
                bounces={false}
            >
                <ImageBackground
                    resizeMode='contain'
                    style={styles.img1}
                    source={imageConstant.walletback}
                >
                    <Pressable
                        onPress={() => props.navigation.goBack()}
                        style={styles.img2}>
                        <Image
                            resizeMode='contain'
                            source={imageConstant.whiteArrow}

                        />

                    </Pressable>
                    {status ? null : <Pressable
                        onPress={() => setStatus(!status)}
                        style={{ position: 'absolute', right: 15, top: '9%' }}>
                        <Image
                            style={{ tintColor: '#FFFFFF' }}
                            source={imageConstant.EDIT}
                        />
                    </Pressable>}
                    <Text style={styles.text1}>My Account</Text>

                    <Image
                        style={styles.img3}
                        source={user.profilePic ? { uri: user.profilePic } : imageConstant.profileDemo}
                    />
                    {status ? <Pressable onPress={openCamera} style={styles.img4}>
                        <Image
                            resizeMode='contain'
                            style={{ height: 17, width: 17 }}
                            source={imageConstant.Pencil}
                        />
                    </Pressable>
                        : null}
                </ImageBackground>
                <View style={styles.view1}>
                    <Text style={styles.labelText}>First Name</Text>
                    <View pointerEvents={!status ? 'none' : 'auto'} style={styles.view2}>
                        <TextInput
                            placeholderTextColor='gray'
                            placeholder='Enter your  name'
                            style={styles.textinput}
                            value={user?.name}
                            onChangeText={(e) => setUser({ ...user, name: e })}
                        />
                        {/* <Image
                            resizeMode='contain'
                            style={styles.img5}
                            source={imageConstant.nextArrow}
                        /> */}
                    </View>
                </View>
                <View style={[styles.view1, { marginTop: '5%' }]}>
                    <Text style={styles.labelText}> Last Name</Text>
                    <View pointerEvents={!status ? 'none' : 'auto'} style={styles.view2}>
                        <TextInput
                            placeholderTextColor='gray'
                            placeholder='Enter your last  name'
                            style={styles.textinput}
                            value={user?.lastName}
                            onChangeText={(e) => setUser({ ...user, lastName: e })}
                        />

                    </View>
                </View>
                <Pressable
                    pointerEvents={!status ? 'none' : 'auto'}
                    onPress={() => props.navigation.navigate("ChangeMobileNumbar", { phoneNumber: user?.phone })}
                    style={[styles.view1, { marginTop: '5%' }]}>
                    <Text style={styles.labelText}>Phone Number</Text>
                    <View pointerEvents='none'>
                        <View style={styles.view2}>

                            <TextInput
                                placeholderTextColor='gray'
                                placeholder='Enter your phone number'
                                style={[styles.textinput, { width: '76%' }]}
                                value={"+" + user?.phone}
                                onChangeText={(e) => setUser({ ...user, phone: e })}
                            />
                            <Text style={styles.verifyText}>Verified</Text>
                            <Image
                                resizeMode='contain'
                                style={styles.img5}
                                source={imageConstant.nextArrow}
                            />
                        </View>
                    </View>
                </Pressable>
                <View pointerEvents='none' style={[styles.view1, { marginTop: '5%' }]}>
                    <Text style={styles.labelText}>Email</Text>
                    <View pointerEvents={!status ? 'none' : 'auto'} style={styles.view2}>
                        <TextInput
                            placeholder='Enter your Email'
                            placeholderTextColor='gray'
                            style={[styles.textinput, { width: '76%' }]}
                            value={user?.email}
                            onChangeText={(e) => setUser({ ...user, email: e })}
                        />

                    </View>
                </View>
                <View style={[styles.view1, { marginTop: '5%' }]}>
                    <Text style={styles.labelText}>Password</Text>
                    <Pressable pointerEvents={!status ? 'none' : 'auto'} onPress={() => props.navigation.navigate("Changepassword", { oldPassword: user?.password })}>
                        <View pointerEvents='none' style={styles.view2}>
                            <TextInput
                                placeholder='Enter password'
                                placeholderTextColor='gray'
                                style={styles.textinput}
                                secureTextEntry={true}
                                value={"********"}
                                onChangeText={(e) => setUser({ ...user, password: e })}
                            />
                            <Pressable style={{ padding: 10 }} onPress={() => setValue(!value)}>
                                <Image
                                    resizeMode='contain'
                                    style={styles.img5}
                                    source={imageConstant.nextArrow}

                                />
                            </Pressable>
                        </View>
                    </Pressable>

                </View>
                {status ?
                    <CustomButton
                        buttonName={"UPDATE"}
                        alignSelf={'center'}
                        marginTop={'5%'}
                        borderRadius={9}
                        marginBottom={10}
                        OnButtonPress={() => setProfileData()}
                    />
                    : null}
            </ScrollView>
            <Modal
                visible={modalVisible}
                transparent={true}
                onRequestClose={() => setModalVisible(!modalVisible)}>
                <View
                    style={{
                        justifyContent: "center",
                        alignItems: "center",
                        flex: 1,
                        backgroundColor: 'rgba(0,0,0,0.6)'
                    }}
                >
                    <View>
                        <LottieView source={require('../../asset/images/loader.json')} style={{ height: 100 }} autoPlay />
                    </View>
                </View>
            </Modal>
        </SafeAreaView>
    )
}

export default MyAccount
const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems: 'center'
    },
    img1: {

        width: Width,
        aspectRatio: 1 / 0.562
    },
    img2: {
        width: 29,
        left: '5%',
        top: '10%',
    },
    text1: {
        color: '#FFFFFF',
        fontSize: 29,
        fontFamily: fontConstant.semibold,
        alignSelf: 'center',
        top: '25%'
    },

    img3: {
        marginTop: '30%',
        borderWidth: 5,
        borderColor: '#FFFFFF',
        borderRadius: 50,
        height: 103,
        width: 103,
        alignSelf: 'center'
    },
    img4: {
        alignSelf: 'center',
        left: '6%',
        width: 28,
        height: 28,
        backgroundColor: '#D9D9D9',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 14,
        marginTop: -20

    },
    view1: {
        alignSelf: 'flex-start',
        left: '5%',
        marginTop: '20%'
    },
    labelText: {
        fontSize: 16,
        color: '#666666'
    },
    img5: {
        height: 16,
        width: 16,
        left: '50%'
    },
    view2: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        width: Width * 0.9,
        borderBottomColor: '#E1E1E1',
        height: 55
    },
    textinput: {
        width: '90%',
        fontSize: 16,
        color: '#000000'
    },
    verifyText: {
        color: '#0EAC00',
        left: '18%'
    }
})