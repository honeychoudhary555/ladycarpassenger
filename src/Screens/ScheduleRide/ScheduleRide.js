import { View, Text, StyleSheet, Pressable,KeyboardAvoidingView } from 'react-native'
import React from 'react'
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Height, Width } from '../../dimensions/dimension';
import { useState } from 'react';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import CustomButton from '../../custom/CustomButton';
import moment from 'moment';

const mapStyle = [
  { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
  { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
  { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
  {
    featureType: 'administrative.locality',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#d59563' }],
  },
  {
    featureType: 'poi',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#d59563' }],
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [{ color: '#263c3f' }],
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#6b9a76' }],
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [{ color: '#38414e' }],
  },
  {
    featureType: 'road',
    elementType: 'geometry.stroke',
    stylers: [{ color: '#212a37' }],
  },
  {
    featureType: 'road',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#9ca5b3' }],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry',
    stylers: [{ color: '#746855' }],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.stroke',
    stylers: [{ color: '#1f2835' }],
  },
  {
    featureType: 'road.highway',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#f3d19c' }],
  },
  {
    featureType: 'transit',
    elementType: 'geometry',
    stylers: [{ color: '#2f3948' }],
  },
  {
    featureType: 'transit.station',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#d59563' }],
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [{ color: '#17263c' }],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#515c6d' }],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.stroke',
    stylers: [{ color: '#17263c' }],
  },
];
const ScheduleRide = (props) => {

  const [datePicker, setDatePicker] = useState(false);
  const [timePicker, setTimePicker] = useState(false);
  const [date, setDate] = useState(new Date());
  const [time, setTime] = useState(new Date(Date.now()));

const maxDate=new Date()
maxDate.setDate(maxDate.getDate() + 7)
const minDate=new Date()
  function showDatePicker() {
    setDatePicker(true);
  };

  function showTimePicker() {
    setTimePicker(true);
  };
  function onDateSelected(event, value) {
    setDate(value);
    setDatePicker(false);
  };

  function onTimeSelected(event, value) {
    setTime(value);
    setTimePicker(false);
  };
  // Geolocation.getCurrentPosition(
  //   position => {
  //     // setLocation(position)
  //     console.log('position',position)
  //     Geocoder.init('AIzaSyAU6KwRMvEmhQbruXnj60cwiORuKQ5BohQ');
  //     Geocoder.from({
  //       latitude:position?.coords.latitude,
  //       longitude:position?.coords.longitude
  //     })
  //       .then(json => {
  //         // var location = json.results[0].geometry.location;
  //     setAddress(json.results[0].formatted_address);
  //     console.log("final address stringify",JSON.stringify(json.results[0]));

  //       })
  //       .catch(error =>{
  //         console.log("error www",error)
  //       });
  //   },
  //   error => {
  //     console.log("error error",error);
  //   },
  //   {enableHighAccuracy:false, timeout: 3000, maximumAge: 3000},
  // )



  return (
    <View style={styles.main}>
      <MapView
        zoomEnabled={true}
        zoomTapEnabled={true}
        style={styles.mapStyle}
        initialRegion={{
          latitude: 28.535517,
          longitude: 77.391029,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
        customMapStyle={mapStyle}>
        <Marker
          icon={imageConstant.location}
          draggable
          coordinate={{
            latitude: 37.78825,
            longitude: -122.4324,
          }}
          onDragEnd={
            (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
          }
          title={'Test Marker'}
          description={'This is a description of the marker'}
        />

      </MapView>
      <View style={styles.view2}>
        <Text style={styles.text1}>Pick-up Date & Time</Text>
        {datePicker ?
          <RNDateTimePicker
            mode="date"
            display="spinner"
            value={date}
            onChange={onDateSelected}
            minimumDate={minDate}
            maximumDate={maxDate}
          /> : null}
        {timePicker ?
          <RNDateTimePicker
            mode="time"
            display="spinner"
            is24Hour={false}
            value={time}
            onChange={onTimeSelected}
          /> : null}
        <View style={styles.view3}>
          <Pressable
            onPress={() => showDatePicker()}
            style={styles.pres1}>
            <Text>{moment(date).format("MMM Do YY")}</Text>
          </Pressable>
          <Pressable
             onPress={()=>showTimePicker()}
            style={styles.pres2}>
            <Text>{moment(time).format("HH")}</Text>
          </Pressable>
          <Pressable
            onPress={()=>showTimePicker()}
            style={styles.pres2}>
            <Text>{moment(time).format("mm")}</Text>
          </Pressable>
          <Pressable
            onPress={() => showTimePicker()}
            style={styles.pres2}>
            <Text>{moment(time).format("A")}</Text>
          </Pressable>
        </View>
        <Text style={styles.text2}>Driver may arrive between {moment(time).format("HH:mm")} - {moment(time).add(30, 'm').format("HH:mm A")}</Text>
        <CustomButton
          buttonName={"Schedule Trip"}
          marginTop={20}
          width={'80%'}
          borderRadius={9}
          height={38}
          OnButtonPress={() => props.navigation.navigate("ConfirmSchedule", { val: "advance" })}
        />


      </View>

    </View>
  )
}

export default ScheduleRide

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  },
  mapStyle: {
    flex: 2,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: '35%',
  },
  view2: {
    position: 'relative',
    backgroundColor: '#FFFFFF',
    top: '65%',
    height: Height * 0.5,
    width: Width,
    alignItems: 'center'
  },
  text1: {
    fontSize: 19,
    color: colorConstant.theme,
    fontWeight: '700',
    alignSelf: 'flex-start',
    marginTop: '5%',
    left: '3%'
    // fontFamily:fontConstant.thin
  },
  pres1: {
    borderWidth: 1.5,
    height: 44,
    width: 106,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: colorConstant.theme,
    borderRadius: 9
  },
  pres2: {
    borderWidth: 1.5,
    height: 44,
    width: 51,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: colorConstant.theme,
    borderRadius: 9
  },
  view3: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: Width * 0.8,
    marginTop: '8%'
  },
  text2: {
    color: '#222B45',
    fontSize: 13,
    fontWeight: '600',
    marginTop: '7%'
  }
})