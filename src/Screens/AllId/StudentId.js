import { View, Text,StyleSheet, Image, Modal } from 'react-native'
import React from 'react'
import { fontConstant, imageConstant } from '../../utils/constant'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import CustomHeader from '../../custom/CustomHeader'
import CustomButton from '../../custom/CustomButton'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useState } from 'react';
import LottieView from 'lottie-react-native';
import toastShow from '../../utils/Toast';
import { baseUrl } from '../../utils/Urls';
import { ImageUploadInBucket } from '../../custom/ImageUploadInBucket';
const StudentId = (props) => {
  const [modalVisible,setModalVisible]=useState(false)
  const openCamera=async()=>{
    // alert('hello')
    let options={
        mediaType:'photo',
        cameraType:'back',
        saveToPhotos:true,
        quality:0.9,
        maxHeight:500,
        maxWidth:500,	
    }
    // launchImageLibrary(options,(response)=>{
    //     console.log('response', response)
    // })
    // launchCamera(options,(res)=>{
    //     console.log(res)
    // })
    launchCamera(options,async(response)=>{
      if (response.didCancel) {
        // toast('User cancelled ', colorConstant.darkRed)
        console.log('User cancelled image picker');
    } else if (response.error) {
        // toast('Something went wrong', colorConstant.darkRed)
        console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);

    } else {
      // console.log("response",response);
    
        console.log('response from gallery------------------->', JSON.stringify(response));
        let data = {
            uri:response?.assets?.[0]?.uri,
            type:response?.assets?.[0]?.type,
            name:response?.assets?.[0]?.fileName
        }
        setModalVisible(true)
try{
        let imageData=await ImageUploadInBucket(data)
        console.log("image front wala",imageData)
        await AsyncStorage.setItem("idFront",JSON.stringify(imageData?.postResponse?.location))
        await AsyncStorage.setItem("idBack",JSON.stringify(imageData?.postResponse?.location))
        // setStatus(true)
        setTimeout(() => {
          setModalVisible(false)
          props.navigation.navigate("FaceId",{id:"Student"})
        }, 3000);
      
}
catch(err)
{
  setModalVisible(false)
  console.log("err",err)
      toastShow("please upload image again")
}
        // 
        // getImageData(formDataReq?._parts[0][1])
        // setModalVisible(false)
        
        // updateProfileImage(formDataReq);
        // await AsyncStorage.setItem("idBack",JSON.stringify(response))
        // await AsyncStorage.setItem("idFront",JSON.stringify(response))
    }

    })
    
   
}

const getImageData=async(imageName)=>{
  setModalVisible(true)
  
try{
 
  let finalFormData=new FormData()
  finalFormData.append("fileName",imageName)
  console.log("image wala",finalFormData)
 await fetch(baseUrl+"auth/image-upload",
 {
  method:"POST",
  headers:{
    'Content-Type': 'multipart/form-data',
    'Accept':'application/json'
  },
  body:finalFormData
 }
 )
 .then(async(res)=>{
  // alert("ok")
  let jsonData=await res.json()
  return jsonData;
 })
 .then(async(res)=>{
  console.log("resssss",res)
  await AsyncStorage.setItem("idFront",JSON.stringify(res.data))
  await AsyncStorage.setItem("idBack",JSON.stringify(res.data))
  setModalVisible(false)
  props.navigation.navigate("FaceId",{id:"Student"})
 })
}
catch(err){
setModalVisible(false)
toastShow("please upload image again")
}

  // let data={
  //   fileName:imageName
  // }
  
}
  return (
    <View style={styles.main}>
        <CustomHeader
         onPressBack={()=>props.navigation.goBack()} 
        />
       <Text style={styles.welcomText}>Take a picture of
your ID <Image source={imageConstant.info} style={{height:16,width:16}}/></Text>
        <Image
        source={imageConstant.camera}
        style={styles.img}
        resizeMode='contain'
        
        />
        <CustomButton
  OnButtonPress={()=>openCamera()}
  buttonName={'Student ID'}
        marginTop={'30%'}
        imageRightFlag={true}
        // imageName={imageConstant.check}
        borderRadius={10}
        txtmarginTop={'1%'}
        />
        <Modal
        visible={modalVisible}
        transparent={true}
        onRequestClose={()=>setModalVisible(!modalVisible)}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            backgroundColor:'rgba(0,0,0,0.6)'
          }}
        >
          <View>
            <LottieView source={require('../../asset/images/loader.json')} style={{ height:100 }} autoPlay />
          </View>
        </View>
      </Modal>
    </View>
  )
}

export default StudentId
const styles=StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
  img:{
width:240,
height:287,
marginTop:'10%'
  },
  welcomText:{
    color:'#6C00C0',
    fontFamily:fontConstant.semibold,
    fontSize:29,
    width:'75%',
    lineHeight:37,
    textAlign:'center',
    marginTop:'10%',
   
},
})