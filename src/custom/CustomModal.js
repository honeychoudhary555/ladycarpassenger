import { View, Text,Modal,StyleSheet, Image, TouchableOpacity, Pressable } from 'react-native'
import React from 'react'
import { heightPercentageToDP } from '../utils/responsiveFIle'
import { Height, Width } from '../dimensions/dimension'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { FlatList } from 'react-native-gesture-handler'
import { useState } from 'react'
import { useEffect } from 'react'

const CustomModal = (props) => {

    const renderDetails=({item,index})=>{
        return(
            
            <View
            key={index}>
                <Image
                resizeMode='contain'
                style={style.img3}
                source={item?.img}
                />
            </View>
        )
    }
  return (
      <View style={style.centeredView}>
          <Modal
              animationType={props.animationType}
              transparent={props.transparent}
              visible={props.visible}
              onRequestClose={props.onRequestClose}
          >
            <Pressable
            onPress={()=>{props.onRequestClose}}
            style={{
              flex:1,
              width:Width,
              justifyContent:"center",
              alignItems:"center",
              backgroundColor:"rgba(1,1,1,0.5)"
            }}>
              <View style={style.main}>
                <Pressable style={style.pres} onPress={props.onRequestClose}>
                <Image
                resizeMode='contain'
                style={style.crossText}
                source={imageConstant.cross}
                />
                </Pressable>
                <View style={style.view}>
                    <Image
                    style={style.crossImg}
                    source={props.modalImage}
                    />
                    <View><Text style={style.nameText}>{props.driverName}</Text>
                    <View style={style.view1}>
                        <Image
                        source={imageConstant.star}
                        />
                        <Text style={style.ratingText}>{props.rating}</Text>
                        
                    </View>
                    </View>
                    <TouchableOpacity style={style.touch}>
                            <Text style={style.text1}>Follow</Text>
                        </TouchableOpacity>
                </View>
                <Text style={style.introText}>{props.intro}</Text>
                <View style={style.imageView}>
                    <Image
                    resizeMode='contain'
                    style={style.img1}
                    
                    source={imageConstant.NoSmoking}
                    />
                      <Image
                    resizeMode='contain'
                    style={style.img1}
                    
                    source={imageConstant.Musical}
                    />
                      <Image
                    resizeMode='contain'
                    style={style.img1}
                    
                    source={imageConstant.Speech}
                    />
                      <Image
                    resizeMode='contain'
                    style={style.img1}
                    
                    source={imageConstant.NoAnimals}
                    />
                </View>
                <Text style={style.commentText}>Comments</Text>
                {
                    props?.data?.map((item,index)=>{
                        return(

                 
                <View style={style.commentview}>
                    <Image
                    style={style.img2}
                    source={item?.commentImage}
                    />
                    <View>
                    <Text style={style.reviewText}>{item?.review}</Text>
                    <Text style={{fontSize:9}}>{item?.date}</Text>
                    </View>
                    <View style={style.view2}>
                        <Image
                        source={imageConstant.star}/>
                        <Text style={[style.ratingText,{fontFamily:fontConstant.bold}]}>{item?.commentRating}</Text>
                    </View>
                </View>
                       )
                    })
                }
                <FlatList
                renderItem={renderDetails}
                data={props.data2}
                // horizontal
                numColumns={4}
                contentContainerStyle={style.flatview}
                />
              </View>
              </Pressable>
          </Modal>
      </View>

      
  )
}

export default CustomModal
const style=StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor:"red",
        
    },
    main:{
        marginVertical:heightPercentageToDP('40%'),
        // justifyContent:"center",
        alignItems:"center",
        backgroundColor:'#FFFFFF',
        height:112,
        borderRadius:15,
        width:'90%',
        alignSelf:'center',
        textAlign:'center',
        height:Height * 0.9
    },
    text:{
        color:'#5C5C5C',
        fontSize:17,
        width:'60%'
    },
    view1:{
        flexDirection:'row',
        alignItems:'center'
    },
    nameText:{
        fontSize:18,
        color:'#000000',
        fontWeight:'600'
    },
    touch:{
        backgroundColor:colorConstant.theme,
        height:24,
        width:76,
        borderRadius:9,
        justifyContent:'center',
        alignItems:'center'
    },
    text1:{
        color:'#FFFFFF',
        fontSize:14,
        fontWeight:'400'
    },
    view:{
        width:Width * 0.9,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-around',
        marginTop:'7%'
    },
    ratingText:{
        color:'#000000',
        fontSize:14,
        left:5
    },
    introText:{
        color:'#393939',
        lineHeight:17,
        width:'83%',
        textAlign:'justify',
        marginTop:'7%'
    },
    img1:{
        height:30,
        width:30
    },
    imageView:{
        width:Width * 0.6,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        alignSelf:'flex-start',
        left:'4.5%',
        marginTop:'7%'
    },
    commentText:{
        color:'#000000',
        fontSize:20,
        fontFamily:fontConstant.bold,
        alignSelf:'flex-start',
        left:'8%',
        marginTop:'7%'
    },
    img2:{
        height:32,
        width:32
    },
    reviewText:{
        color:'#000000',
        fontSize:11,
        width:190,
        textAlign:'justify'
    },
    view2:{
        flexDirection:'row'
    },
    commentview:{
        width:Width * 0.75,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginTop:'8%'
    },
    img3:{
        margin:3,
        height:70,
        width:70
    },
    view3:{
        // marginTop:'5%'
    },
    flatview:{
        width:Width *0.85,
        alignSelf:'center',
        alignItems:'center',
        justifyContent:'center',
        marginTop:'12%'
    },
    crossText:{
     
        height:11,
        width:11
    },
    pres:{
        position:'absolute',
        top:'2%',
        right:'5%',
    }
})