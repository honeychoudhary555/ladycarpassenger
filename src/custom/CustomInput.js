import { View, Text, TextInput, StyleSheet, Image, Pressable } from 'react-native'
import React, { useEffect, useState } from 'react'
import { colorConstant, imageConstant } from '../utils/constant'

const CustomInput = (props) => {
    const [imgFlag,setImgFlag]=useState(false)
    useEffect(()=>{
        setImgFlag(props.imgFlag)
    },[])
  return (
    <View style={{flexDirection:'row',alignSelf:'center'}} >
      <TextInput
      placeholder={props.placeholder}
     
      style={[{...styles.textinput},{
        width:props.width,
        height:props.height
      }]}
      value={props.value}
      editable={props.editable}
      marginTop={props.marginTop}
      secureTextEntry={props.secureTextEntry}
      onChangeText={props.onChangeText}
      borderColor={props.borderColor}
      backgroundColor={props.backgroundColor}
      keyboardType={props.keyboardType}
      placeholderTextColor='gray'
      />
     {imgFlag ? <Pressable onPress={props.onPress}><Image source={props.imageName}
     height={props.imageHeight}
     widht={props.imageWidht}
     marginTop={props.imgmarginTop}
     resizeMode='contain' style={{position:'absolute',widht:20,height:20,right:15,top:28}}/>
     </Pressable>
     : null}
    </View>
  )
}

export default CustomInput
const styles=StyleSheet.create({
    textinput:{
        borderColor:colorConstant.theme,
        borderWidth:1.5,
        width:284,
        marginTop:20,
        borderRadius:9,
        // justifyContent:'space-around',
        paddingLeft:18,
        paddingVertical:0
    }
})