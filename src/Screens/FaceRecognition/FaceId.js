import { View, Text, StyleSheet,Image, ScrollView, BackHandler} from 'react-native'
import React from 'react'
import CustomHeader from '../../custom/CustomHeader'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import CustomButton from '../../custom/CustomButton'
import ReactNativeBiometrics, { BiometryTypes } from 'react-native-biometrics'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import TouchID from 'react-native-touch-id'
import { useState ,useEffect} from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { baseUrl } from '../../utils/Urls'
import toastShow from '../../utils/Toast'
import moment from 'moment'
// import { color } from 'react-native-reanimated'
const FaceIds = (props) => {
  const [userData,setUserData]=useState()
  const [idFrontDetails,setIdFrontDetails]=useState()
  const [idBackDetails,setIBackDetails]=useState()
  const [callingCode,setCallingCode]=useState()
  const idType=props.route.params.id
  useEffect(()=>{
    setTimeout(async()=>{
       let asyncBack=await AsyncStorage.getItem("idBack")
      let asyncFront=await AsyncStorage.getItem("idFront")
      let asynchUser=await AsyncStorage.getItem("user")
      let callingCode= await AsyncStorage.getItem("callingCode")
      setUserData(JSON.parse(asynchUser))
      setIdFrontDetails(JSON.parse(asyncFront))
      setIBackDetails(JSON.parse(asyncBack))
      setCallingCode(JSON.parse(callingCode))
    },500)
       
   },[])
   useEffect(()=>{
   
    BackHandler.addEventListener('hardwareBackPress',handleBackClick);
    return ()=>{
        BackHandler.removeEventListener('hardwareBackPress',handleBackClick)
    }
},[])
const handleBackClick=()=>{
  props.navigation.navigate("IdPicture")
  return true
}
  const getsignUpData=async()=>{
     data={
        address:userData?.Address,
        phoneNumber:callingCode+userData?.Phone,
        name:userData?.Name,
        lastName:userData?.LastName,
        dob:moment(userData.DoB).format("YYYY-MM-DD"),
        email:userData?.Email,
        idFront:idFrontDetails,
        idBack:idBackDetails,
        password:userData?.Password,
        signupType:userData?.signupType,
        socialId:userData?.socialId,
        idType:idType,
        stateId:userData?.State,
        cityId:userData?.City,
        countryCode:callingCode
    }
    let newFormData= new FormData()
    newFormData.append("address",userData?.Address)
    newFormData.append("phoneNumber",userData?.Phone)
    newFormData.append("name",userData?.Name)
    newFormData.append("dob",userData?.DoB)
    newFormData.append("email",userData?.Email)
    newFormData.append("idFront",idFrontDetails)
    newFormData.append("idBack",idBackDetails)
    newFormData.append("password",userData.password)
    newFormData.append("signupType",userData.signupType)

    console.log("data------->",data)
    try{
        let response=await fetch(baseUrl+"auth/passenger-signUp",{
            method:"POST",
            body:JSON.stringify(data),
            headers:{
                'content-type': 'application/json'
            }
        })
        .then(async(res)=>{
            let jsonData=await res.json()
            return jsonData
            
        })
        .then((res)=>{
            console.log("response--->",res)
           if(res.code==200)
           {
            props.navigation.navigate("OnComplete")
           }
           else if(res.code==409)
           {
            setTimeout(()=>{
              toastShow("This Phone Number is already exists",colorConstant.theme)
              // alert("ok")
            },1000)
            
           }
            // props.navigation.navigate("Login",{text:"signup"})
        })
        console.log(data)
    }
    catch(error){
        console.log(error)
    }
}
  const openCamera=async()=>{
    // alert('hello')
    let options={
        mediaType:'photo',
        cameraType:'back',
        saveToPhotos:true	
    }
    // launchImageLibrary(options,(response)=>{
    //     console.log('response', response)
    // })
    // launchCamera(options,(res)=>{
    //     console.log(res)
    // })
    launchCamera(options,(response)=>{
      if (response.didCancel) {
        // toast('User cancelled ', colorConstant.darkRed)
        console.log('User cancelled image picker');
    } else if (response.error) {
        // toast('Something went wrong', colorConstant.darkRed)
        console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);

    } else {
      // console.log("response",response);
      getsignUpData()
      // props.navigation.navigate("OnComplete")
        // console.log('response from gallery------------------->', JSON.stringify(response));
        // let formDataReq = new FormData();
        // let data = {
        //     uri:response?.assets?.[0]?.uri,
        //     type:response?.assets?.[0]?.type,
        //     name:response?.assets?.[0]?.fileName
        // }
        // formDataReq.append("file",data);
        // updateProfileImage(formDataReq);
    }

    })
    
   
}
const handleTouch=()=>{
  const optionalConfigObject = {
    unifiedErrors: false ,// use unified error messages (default false)
    passcodeFallback: false // if true is passed, itwill allow isSupported to return an error if the device is not enrolled in touch id/face id etc. Otherwise, it will just tell you what method is supported, even if the user is not enrolled.  (default false)
  }
   
  TouchID.isSupported(optionalConfigObject)
    .then(biometryType => {
      // Success code
      if (biometryType === 'FaceID') {
          console.log('FaceID is supported.');
      } else {
          console.log('TouchID is supported.');
      }
    })
    .catch(error => {
      // Failure code
      console.log(error);
    });
}
 const handleFace=()=>{
  const rnBiometrics = new ReactNativeBiometrics()
  rnBiometrics.isSensorAvailable()
  .then((resultObject) => {
    const { available, biometryType } = resultObject

    if (available && biometryType === BiometryTypes.TouchID) {
      alert('TouchID is supported')
    } else if (available && biometryType === BiometryTypes.FaceID) {
      alert('FaceID is supported')
    } else if (available && biometryType === BiometryTypes.Biometrics) {
      console.log('Biometrics is supported')
    } else {
      alert('Biometrics not supported')
    }
  })

}
console.log("object","+"+callingCode+userData?.Phone)
  return (
    <View style={styles.main}>
        <CustomHeader
         onPressBack={()=>props.navigation.navigate('IdPicture')} 
        />
        <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{backgroundColor:'#FFFFFF',alignItems:'center',marginTop:'8%',paddingBottom:50}}
        >
      <Text style={styles.welcomText}>LadyCar requires facial recognition</Text>
      <Image source={imageConstant.info}
        style={{marginTop:'5%',height:24,width:24}}
      />
      <Image
      source={imageConstant.FaceRec}
      style={styles.img}
      />
       <Image source={imageConstant.info}
        style={{marginTop:'40%',left:'43%',top:'5%',height:24,width:24}}
      />
      <CustomButton
      buttonName={'Set Up Your Face ID'}
      marginTop={'0%'}
      width={'75%'}
      borderRadius={10}
      OnButtonPress={()=>openCamera()}
      // OnButtonPress={()=>handleTouch()}
      marginBottom={20}
      />
      </ScrollView>
    </View>
  )
}


export default FaceIds
const styles=StyleSheet.create({
    main:{
        flex:1,
       
        backgroundColor:'#FFFFFF'
    },
    welcomText:{
        color:'#6C00C0',
        fontFamily:fontConstant.semibold,
        fontSize:29,
        width:'80%',
        lineHeight:37,
        textAlign:'center',
        marginTop:'9%'
    },
    img:{
      marginTop:'10%',
      backgroundColor:'#F5F5F5'
    }
})