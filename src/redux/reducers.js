import * as types from './actionTypes'
import allState from './states'

export default parentFlowReducer = (state = allState.localStates, action) => {
    console.log("inside--->", state)
    switch (action.type) {
        case types.INTRO_STATUS:
            return {
                ...state,
                introStatus: action.payload.introStatus
            }
        case types.JWT_TOKEN:
            return {
                ...state,
                jwtToken: action.payload.jwtToken
            }
        case types.CAB_TYPE:
            return {
                ...state,
                cabType: action.payload.cabType
            }
        case types.PASSENGER_COUNT:
            return {
                ...state,
                passengerCount: action.payload.passengerCount
            }
        case types.RequestID:
            return {
                ...state,
                requestId: action.payload.requestId
            }
            case types.DRIVER_ID:
            return {
                ...state,
                driverId: action.payload.driverId
            }
            case types.START_LOCATION:
                return {
                    ...state,
                    startLocation: action.payload.startLocation
                }
                case types.END_LOCATION:
                    return {
                        ...state,
                        endLocation: action.payload.endLocation
                    }
        default: {
            return state
        }
    }

}

export const action = {
    setIntroStatus: introStatus => {
        return {
            type: types.INTRO_STATUS,
            payload: { introStatus: introStatus }
        }
    },
    setJwtToken: jwtToken => {
        return {
            type: types.JWT_TOKEN,
            payload: { jwtToken: jwtToken }
        }
    },
    setCabType: cabType => {
        return {
            type: types.CAB_TYPE,
            payload: { cabType: cabType }
        }
    },
    setPassengerCount: passengerCount => {
        return {
            type: types.PASSENGER_COUNT,
            payload: { passengerCount: passengerCount }
        }
    },
    setRequestId: requestId => {
        return {
            type: types.RequestID,
            payload: { requestId: requestId }
        }
    },
    setDriverId: driverId => {
        return {
            type: types.DRIVER_ID,
            payload: { driverId: driverId }
        }
    },
    setStartLocation: startLocation => {
        return {
            type: types.START_LOCATION,
            payload: { startLocation: startLocation}
        }
    },
    setEndLocation: endLocation => {
        return {
            type: types.END_LOCATION,
            payload: { endLocation: endLocation}
        }
    },

}