import { View, Text, StyleSheet, Image, Pressable,BackHandler } from 'react-native'
import React from 'react'
import MapView, { PROVIDER_GOOGLE, Marker, Polyline } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Width } from '../../dimensions/dimension';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { useEffect } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import BottomSheet from '@gorhom/bottom-sheet';
import { useRef } from 'react';
import { useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MapViewDirections from 'react-native-maps-directions';
import { googleKey } from '../../utils/Urls';
import { useDispatch } from 'react-redux';
import { action } from '../../redux/reducers';


const Request = (props) => {
    const [profileData,setProfileData]=useState(null)
    const dispatch=useDispatch()
    let val,location,detail,vehicleType,distance,startPoint,endPoint
    if(props?.route?.params !==null){
     val=props?.route?.params?.val
     location=props?.route?.params?.location
     detail=props?.route?.params?.detail
     vehicleType=props?.route?.params?.vehicleType
     distance = props?.route?.params?.distance
     startPoint=props?.route?.params?.start
     endPoint=props?.route?.params?.data
    console.log(vehicleType)
    }

    console.log(props?.route?.params,'----------------');

useFocusEffect(()=>{
    setTimeout(()=>{
        // alert(vehicleType)
        props.navigation.navigate('DriverSelect',{val:val,location:location,vehicleType:vehicleType,detail:detail,distance:distance,startPoint:startPoint,endPoint:endPoint})
        vehicleType=null
        dispatch(action.setStartLocation(location))
        dispatch(action.setEndLocation(detail))
    },4000)
})
useEffect(()=>{
    getProfile()
    BackHandler.addEventListener('hardwareBackPress',handleBackClick);
    return ()=>{
        BackHandler.removeEventListener('hardwareBackPress',handleBackClick)
    }
},[])

const bottomShet=useRef()
    const mapStyle = [
        { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
        {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{ color: '#263c3f' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#6b9a76' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{ color: '#38414e' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#212a37' }],
        },
        {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#9ca5b3' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{ color: '#746855' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#1f2835' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#f3d19c' }],
        },
        {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{ color: '#2f3948' }],
        },
        {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{ color: '#17263c' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#515c6d' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{ color: '#17263c' }],
        },
    ];

    const handleBackClick=()=>{
        props.navigation.navigate("ScheduleTrip")
        return true
    }
    const getProfile=async()=>{
let data= await AsyncStorage.getItem("Profile")
setProfileData(JSON.parse(data))
    }
    console.log("request pr",profileData)
    return (
        <View style={styles.main}>
            <Text>Request</Text>
            <MapView
                zoomEnabled={true}
                zoomTapEnabled={true}
                style={styles.mapStyle}
                region={{
                    latitude:parseFloat(location?.latitude),
                    longitude:parseFloat(location?.longitude) ,
                    latitudeDelta:0.4,
                    longitudeDelta:0.5
                  }}
                customMapStyle={mapStyle}>

                {/* <Marker
                    // icon={imageConstant.location}
                    draggable
                    coordinate={{
                        latitude: location?.latitude,
                        longitude: location?.longitude,
                    }}
                    onDragEnd={
                        (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                    }
                    title={'Test Marker'}
                    description={'This is a description of the marker'}
                />
                <Polyline
               coordinates={[{
                latitude:location?.latitude,
                longitude:location?.longitude,
               },
               {
                latitude:detail?.lat,
                longitude:detail?.lng,
               }
              ]}
              lineDashPattern={[1]}
              strokeColor="red" // fallback for when `strokeColors` is not supported by the map-provider
          strokeColors={['red']}
          strokeWidth={6}
              /> */}
               <Marker
              // icon={imageConstant.location}
              width={30}
              height={30}
              draggable
              coordinate={{
                latitude: location?.latitude,
                longitude: location?.longitude,
              }}
              onDragEnd={
                (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
              }
              title={'Start Location'}
              description={''}
            >
                <Image resizeMode='contain' source={imageConstant.startPoint} style={{height:32,width:32}}/>

              </Marker>
              <MapViewDirections
                               origin={{latitude:location?.latitude,longitude:location?.longitude}}
                               destination={{latitude:detail?.lat,longitude:detail?.lng}}
                               apikey={googleKey}
                               strokeColor={'red'}
                               strokeWidth={3}
                             />
                             <Marker
                             icon={imageConstant.endPoint}
                             // image={imageConstant.location}
                             width={50}
                             height={50}
                                 draggable
                                 coordinate={{
                                     latitude: detail?.lat,
                                     longitude: detail?.lng,
                                 }}
                                 onDragEnd={
                                     (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                                 }
                                 title={'End Location'}
                                 description={''}
                             >
                              <Image resizeMode='contain' source={imageConstant.endPoint} style={{height:32,width:32}}/>
                              </Marker>
            </MapView>
            <View style={styles.view}>
                <Pressable onPress={()=>props.navigation.toggleDrawer()}>
                <Image
                    source={imageConstant.menuIcon}
                    style={{height:42,width:42}}
                    resizeMode="contain"
                />
                </Pressable>
                <Pressable onPress={()=>props.navigation.navigate("Profile")}>
                    <Image
                        style={styles.img1}
                        source={profileData?.userData?.profilePic ? {uri:profileData?.userData?.profilePic} : imageConstant.profileDemo}
                        resizeMode="contain"
                    />
                </Pressable>
            </View>
            <Image
                style={styles.img2}
                source={imageConstant.Zooms}
            />
            {/* <BottomSheet
            ref={bottomShet}
            > */}
            <Pressable style={styles.view1}>
                <Image
                    resizeMode='contain'
                    style={styles.img3}
                    source={imageConstant.Rect}
                />
                <Text style={styles.text}>LADYCAR REQUESTED</Text>
                <View style={styles.view2}>
                    <View style={styles.view3}>
                <Text style={styles.text2}>Looking for nearby drivers</Text>
                <Image
                source={imageConstant.Tesla}
                />
                </View>
                {/* <Pressable
                style={{flexDirection:'row',alignItems:'center',marginTop:'15%',marginBottom:'5%',alignSelf:'center'}}
                onPress={()=>props.navigation.navigate("ScheduleTrip")}>
                <Text style={styles.cancelText}>Cancel?   </Text>
                <Image
                resizeMode='contain'
                style={{height:24,width:24}}
                source={imageConstant.info}
                />
                </Pressable> */}
            </View>
          
            </Pressable>
        </View>
    )
}

export default Request
const styles = StyleSheet.create({
    main: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    mapStyle: {
        flex: 2,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: '35%',
    },
    view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Width
    },
    view1: {
        backgroundColor: '#FFFFFF',
        height: '40%',
        position: 'absolute',
        top: '60%',
        width: Width,
        alignItems: 'center',
        borderTopRightRadius:40,
        borderTopLeftRadius:40

    },
    text: {
        color:'#000000',
        fontFamily:fontConstant.bold,
        fontSize:17,
        marginTop:10,
    },
    view2:{
        width:'80%',
        backgroundColor:'#FFFFFF',
        height:'70%',
        borderTopWidth:1,
        marginTop:'3%',
        
        // marginTop:'5%'
    },
    text2:{
        color:colorConstant.theme,
        fontFamily:fontConstant.bold,
        fontSize:24,
        width:'60%'
    },
    view3:{
        flexDirection:'row',
        justifyContent:'space-between',
        // alignItems:'center',
        marginTop:'8%'
    },
    cancelText:{
        alignSelf:'center',
        color:colorConstant.theme,
        // marginTop:'15%'
    },
    img1:{
        borderWidth: 2, 
        borderColor: '#FFFFFF', 
        borderRadius: 100, 
        right: 5,
        height:33,
        width:33
    },
    img2:{
        alignSelf: 'flex-end', 
        right: 5,
         top: 15
    },
    img3:{
        marginTop: '10%',
         width: 77,
          height: 6
    }
})