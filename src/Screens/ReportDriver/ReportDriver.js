import { View, Text, StyleSheet, TextInput, Image, Pressable, ScrollView } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import { Width } from '../../dimensions/dimension'
import CustomButton from '../../custom/CustomButton'
import { useSelector } from 'react-redux'
import { useState } from 'react'
import { baseUrl } from '../../utils/Urls'
import toastShow from '../../utils/Toast'

const ReportDriver = (props) => {
  const [topic, setTopic]=useState("")
  const [comment,setCommet]=useState("")
  let { jwtToken, driverId } = useSelector((state) => state.reducers)
  console.log("report Driver",driverId)


  const handleReport=async()=>{
    let data={
      "driverId":driverId,
      "topic":topic,
      "comment":comment
    }
    if(topic!=="" && comment!==""){
    try{
      
      await fetch(baseUrl+"user/report-driver",{
        method:"POST" ,
        headers:{
          'Content-Type':'application/json',
          'Authorization':jwtToken
        },
        body:JSON.stringify(data)
           })
           .then(async(res)=>{
              let jsonData=await res.json()
              return jsonData;
           })
           .then((res)=>{
            // alert("ok")
            console.log("report driver",res)
            if(res.code==200)
            {
              props.navigation.goBack()
            }
            else{
              toastShow(res.message)
            }
           })
    }
    catch(err)
    {
      console.log(err)
    }
  }
  else{
    toastShow("please provide complete deatails")
  }
  }
  return (
   
    <View style={style.main}>
      <View style={style.view1}>
        <View style={style.view2}>
          <Pressable onPress={()=>props.navigation.navigate("TripSummary")}>
      <Image
        source={imageConstant.Arrow}
        />
        </Pressable>
        {/* <Pressable onPress={()=>props.navigation.toggleDrawer()}>
        <Image
        source={imageConstant.menuIcon}
        />
        </Pressable> */}
        </View>
         {/* <Image
        source={imageConstant.profile}
        /> */}
      </View>
      <ScrollView
      bounces={false}
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{backgroundColor:'#FFFFFF',alignItems:'center'}}>
      <Text style={style.welcomText}>Report the driver</Text>
      <Text style={style.text1}>Topic</Text>
      <TextInput
      style={style.textInput1}
      onChangeText={(e)=>setTopic(e)}
      placeholder={'topic'}
      placeholderTextColor='#5C5C5C'
      />
      <Text style={[style.text1,{marginTop:'5%'}]}>Comments</Text>
      <TextInput
      style={[style.textInput1,{height:204,textAlignVertical:'top'}]}
      multiline={true}
      onChangeText={(e)=>setCommet(e)}
      placeholderTextColor='#5C5C5C'
      placeholder={'add a comment...'}
      />
      <CustomButton
      buttonName={'REPORT'}
      marginTop={'25%'}
      borderRadius={10}
      marginBottom={30}
      OnButtonPress={handleReport}
      />
      </ScrollView>
    </View>
    
  )
}

export default ReportDriver
const style=StyleSheet.create({
    main:{
        flex:1,
        // alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
    welcomText:{
        color:'#6C00C0',
        fontFamily:fontConstant.bold,
        fontSize:24,
        width:'80%',
        lineHeight:37,
        textAlign:'center',
        marginTop:'10%',
       
    },
    text1:{
        color:'#000000',
        fontFamily:fontConstant.regular,
        fontSize:14,
        alignSelf:'flex-start',
        left:'7%',
        marginTop:'10%'
    },
    textInput1:{
        // backgroundColor:'#DBDBDB',
        width:Width * 0.85,
        height:53,
        marginTop:'5%',
        borderRadius:10,
        borderWidth:1,
        paddingHorizontal:10,
        borderColor:colorConstant.theme
    },
    view1:{
      width:Width,
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      marginTop:'5%',
      paddingHorizontal:20
    },
    view2:{
      flexDirection:'row',
      alignItems:'center',
      width:'25%',
      justifyContent:'space-between',
      
      
    }
})