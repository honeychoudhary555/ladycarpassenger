import { View, Text, SafeAreaView, Modal, TouchableOpacity,Alert } from 'react-native'
import React, { useEffect } from 'react'
import CustomButton from './src/custom/CustomButton'
import SplashScreen from 'react-native-splash-screen';
import {} from './src/asset/images/logo.jpg'
import Landing from './src/Screens/Landing/Landing';
// import Welcome from './src/Screens/WelCome/Welcome';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { isReadyRef, navigationRef } from "react-navigation-helpers";

// import StartAdventure from './src/Screens/StartAdventure/StartAdventure';
// import VerificationCode from './src/Screens/VerifcationCode/VerificationCode';
// import CustomHeader from './src/custom/CustomHeader';
// import SignUp from './src/Screens/SignUp/SignUp';
// import IdPicture from './src/Screens/IdPicture/IdPicture';
// import Login from './src/Screens/Login/Login';
// import StateId from './src/Screens/AllId/StateId';
// import Passport from './src/Screens/AllId/Passport';
// import DriverLicense from './src/Screens/AllId/DriverLicense';
// import StudentId from './src/Screens/AllId/StudentId';
// import FaceId from './src/Screens/FaceRecognition/FaceId';
// import WriteAbout from './src/Screens/WriteAbout/WriteAbout';
import ScheduleTrip from './src/Screens/ScheduleTrip/ScheduleTrip';
import Destination from './src/Screens/Destination/Destination';
import OnComplete from './src/Screens/AllId/OnComplete';
import CabBooking from './src/Screens/BookCab/CabBooking';
import Payment from './src/Screens/Payment/Payment';
import AddCard from './src/Screens/AddCard/AddCard';
import Request from './src/Screens/Request/Request';
import DriverSelect from './src/Screens/DriverSelect/DriverSelect';
import DriverApproval from './src/Screens/DriverApproval/DriverApproval';
import RequestSuccess from './src/Screens/RequestSuccess/RequestSuccess';
import RequestCancel from './src/Screens/RequestCancel/RequestCancel';
import ConfirmBooking from './src/Screens/ConfirmBooking/ConfirmBooking';
import IdVerification from './src/Screens/IdVerfication/IdVerification';
import OnTheWay from './src/Screens/OnTheWay/OnTheWay';
import TripSummary from './src/Screens/TripSummary/TripSummary';
import ReportDriver from './src/Screens/ReportDriver/ReportDriver';
import ScheduleRide from './src/Screens/ScheduleRide/ScheduleRide';
import History from './src/Screens/History/History';
import TripDetails from './src/Screens/TripDetails/TripDetails';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContainer from './src/Screens/DrawerContainer/DrawerContainer';
import { Height } from './src/dimensions/dimension';
import MyAccount from './src/Screens/MyAccount/MyAccount';
import MyWallet from './src/Screens/MyWallet/MyWallet';
import ForgetPassword from './src/Screens/ForgetPassword/ForgetPassword';
import ResetPassword from './src/Screens/ResetPassword/ResetPassword';
import CancelReason from './src/Screens/CancelReason/CancelReason';
// import HelpCenter from './src/Screens/HelpCenter/HelpCenter';
import Chat from './src/Screens/Chat/Chat';
import Profile from './src/Screens/Profile/Profile';
import HelpCenter from './src/Screens/HelpCenter/HelpCenter';
import DiscountCode from './src/Screens/DiscountCode/DiscountCode';
import AddPromoCode from './src/Screens/AddPromoCode/AddPromoCode';
import CommentsAboutYou from './src/Screens/Comments/CommentsAboutYou';
import Comments from './src/Screens/Comments/Comments';
import ConfirmSchedule from './src/Screens/ConfirmSchedule/ConfirmSchedule';
import BookingProcess from './src/Screens/ProcessBooking/BookingProcess';
import BookingConfirm from './src/Screens/ProcessBooking/BookingConfirm';
import AddMoney from './src/Screens/AddMoney/AddMoney';
import Payment2 from './src/Screens/Payment/Payment2';
import PaymentSuccess from './src/Screens/Payment/PaymentSuccess';
import Helpcenter2 from './src/Screens/HelpCenter/Helpcenter2';
import MyComments from './src/Screens/Comments/MyComments';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import CommentNew from './src/Screens/CommentNew/CommentNew';
import RecivedComment from './src/Screens/CommentNew/RecivedComment';
import MyCommentNew from './src/Screens/CommentNew/MyCommentNew';
import RootNavigatior from './src/navigator/RootNavigatior';

const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();

const Tab = createMaterialTopTabNavigator();
const App = () => {
  const [modalVisible, setModalVisible] = React.useState(false);
  useEffect(()=>{
    setTimeout(
      function () {
        SplashScreen.hide()
      }
        .bind(this),
      1000
    );
  },[])
  const DrawerStack=()=>(
    <>
    <Drawer.Navigator
    initialRouteName='Landing'
   
    screenOptions={{
      headerShown: false,
      drawerType:"front",
      drawerStyle: {
        width:'50%',
       height:Height * 0.80,
      //  backgroundColor:'blue',
        borderBottomRightRadius:70
      },}}
      drawerContent={props=><DrawerContainer {...props}/>}
    
    >
    <Drawer.Screen  name="Landing" screenOptions={{headerShown:false}} component={Landing}/>
    <Drawer.Screen name="ScheduleTrip" component={ScheduleTrip} options={{headerShown:false}} />
    <Drawer.Screen name="Request" component={Request} options={{headerShown:false}} />

    <Drawer.Screen name="DriverSelect" component={DriverSelect} options={{headerShown:false}} />
      <Drawer.Screen name="DriverApproval" component={DriverApproval} options={{headerShown:false}} />
      <Drawer.Screen name="RequestSuccess" component={RequestSuccess} options={{headerShown:false}} />
      <Drawer.Screen name="ConfirmBooking" component={ConfirmBooking} options={{headerShown:false}} />
      <Drawer.Screen name="IdVerification" component={IdVerification} options={{headerShown:false}} />
      <Drawer.Screen name="OnTheWay" component={OnTheWay} options={{headerShown:false}} />
      <Drawer.Screen name="TripSummary" component={TripSummary} options={{headerShown:false}} />
      <Drawer.Screen name="ReportDriver" component={ReportDriver} options={{headerShown:false}} />
      <Stack.Screen name="RecivedComment" component={RecivedComment} options={{headerShown:false}} />


    </Drawer.Navigator>

    </>
  )

  return (
    <NavigationContainer>
      <SafeAreaView
        style={{ flex: 1 }}
      >
        <RootNavigatior />
      </SafeAreaView>
    </NavigationContainer>
  
  )
}

export default App
