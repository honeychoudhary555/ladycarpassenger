import { View, Text, StyleSheet, Pressable, Image ,TextInput, Platform, Keyboard} from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import { Width } from '../../dimensions/dimension'
import CustomButton from '../../custom/CustomButton'
import { ScrollView } from 'react-native-gesture-handler'
import { useEffect } from 'react'
import { useState } from 'react'
import validation from '../../utils/validation'
import { baseUrl } from '../../utils/Urls'
import toastShow from '../../utils/Toast'
import PhoneInput from 'react-native-phone-number-input'
const ForgetPassword = (props) => {
  const [padding,setPadding] = useState(10)
  const [callingCode,setCallingCode]=useState(1)
  const [num, setNum]=useState("")
    let navtext=props?.route?.params.text

    // *******Manage keyboard*************
    useEffect(()=>{
      let sub = Keyboard.addListener('keyboardWillShow',(e)=>{
        setPadding(e.endCoordinates.height+10)
      })
      let unsubs = Keyboard.addListener('keyboardWillHide',()=>{
        setPadding(10)
      })
      return ()=>{
        sub.remove();
        unsubs.remove();
      }
    },[])
    const handleChange=async()=>{
    if(validation.phoneValid(num))
    {

      data={
        email:"",
        phoneNumber:callingCode+num,
    }
    console.log("data",data)
   
        try{
            let response=await fetch(baseUrl+"auth/check-passenger",
            {
            method:'POST',
            body:JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        }
            )
            .then(async(res)=>{
             
                let jsonData=await res.json()
                return jsonData

            })
            .then((res)=>{
                console.log("response--->",res)
               if(res.code==200)
               {
                props.navigation.navigate("VerificationCode", { id: "ForgetPassword", text: navtext ,phone:num,callingCode:callingCode})
               }
               if(res.code==401)
               {
                toastShow("Phone number does not exist")
               }
            })
        }
        catch(err)
        {
            console.log(err)
        }
      // 
    }
    }
  return (
    <View style={styles.main}>
      <Pressable
      onPress={()=>props.navigation.goBack()}
      style={styles.pres}>
        <Image
        resizeMode='contain'
        source={imageConstant.Arrow}
        />
      </Pressable>
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          alignItems: 'center',
          paddingBottom: padding
        }}
      >
        <Text style={styles.text1}>Forgot Password</Text>
        <Image
          resizeMode='contain'
          style={styles.img1}
          source={imageConstant.forgetPassword}
        />
        {/* <TextInput
          style={styles.input}
          placeholder='Phone Number'
          keyboardType="numeric"
          onChangeText={(e)=>setNum(e)}
        /> */}
         <PhoneInput
          // defaultValue={phoneNumber}
          defaultCode="US"
          onChangeCountry={(c) => {
            setCallingCode(c.callingCode[0])
            // setCountry(c.name)
          }}
          layout="first"

          onChangeText={(text) => {
            setNum(text);
            // setUserData({ ...userData, Phone: text })
          }}
          placeholder='Phone Number'

          //  onChangeFormattedText={(text) => {
          //    // setFormattedValue(text);
          //  }}
          withDarkTheme
          //   withShadow
          //   autoFocus
          textInputStyle={{
            paddingVertical: 0,
            height: 20

          }}
          containerStyle={styles.phoneContainer}
          flagButtonStyle={styles.flag}
          codeTextStyle={{ height: 0, width: 0 }}
          textContainerStyle={styles.textContainer}
        />
        <CustomButton
          buttonName={"CONFIRM"}
          width={Width * 0.80}
          marginTop={'6%'}
          borderRadius={10}
          // marginBottom={20}
          // OnButtonPress={() => props.navigation.navigate("VerificationCode", { id: "ForgetPassword", text: navtext })}
          OnButtonPress={()=>handleChange()}
        />
      </ScrollView>
    </View>
  )
}

export default ForgetPassword

const styles=StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
    pres:{
        alignSelf:'flex-start',
        left:'6%',
        marginTop:'7%'
    },
    text1:{
        fontSize:29,
        color:colorConstant.theme,
        fontFamily:fontConstant.semibold,
        marginTop:'5%'
    },
    img1:{
        height:316,
        width:160,
        marginTop:'7%'
    },
    input:{
        width:Width * 0.80,
        borderWidth:1,
        height:45,
        borderRadius:10,
        borderColor:colorConstant.theme,
        marginTop:'13%',
        paddingLeft:15
    },
    phoneContainer: {
      marginTop: 20,
      borderWidth: 1.5,
      borderColor: colorConstant.theme,
      borderRadius: 10,
      width: "100%",
      height: 45
  
    },
    flag: {
      paddingVertical: 0,
      borderRadius: 10,
      // height:50,
      height: 35,
    },
    textContainer: {
      borderTopRightRadius: 10,
      borderBottomRightRadius: 10
    },
})