import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity, Pressable, Platform,BackHandler } from 'react-native'
import React from 'react'
import MapView, { PROVIDER_GOOGLE, Marker, Polyline } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Width } from '../../dimensions/dimension';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CheckBox from '@react-native-community/checkbox';
import { useState } from 'react';
import CustomButton from '../../custom/CustomButton';
import CustomModal from '../../custom/CustomModal';
import { baseUrl, googleKey } from '../../utils/Urls';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import { action } from '../../redux/reducers';
import MapViewDirections from 'react-native-maps-directions';




const DriverSelect = (props) => {
    const [check, setCheck] = useState(true)
    const [SelectedId, setSelectedId] = useState(0)
    const [status, setStatus] = useState(false)
    const [value, setValue] = useState(true)
    const [modalVisible, setModalVisible] = useState(false)
    const [driverData,setDriverData]=useState(null)
    const [profileData,setProfileData]=useState(null)
    const [btn,setBtn]=useState(false)
    const [deriverId,setDeriverId]=useState("")
    const isFocus=useIsFocused()
    const dispatch=useDispatch()
    const [count,setCount]=useState(20)
    let { jwtToken,cabType,passengerCount } = useSelector((state) =>state.reducers)
    let val = props?.route?.params?.val
    let location = props?.route?.params?.location
    let vehicleType = props?.route?.params?.vehicleType
    let detail=props?.route?.params?.detail
    let distance = props?.route?.params?.distance
    let startPoint=props?.route?.params?.startPoint
    let endPoint=props?.route?.params?.endPoint
    // useFocusEffect(
    //     React.useCallback(()=>{
           
    //     },[isFocus])
        

    useEffect(() => {
        if (isFocus) {
          const intervalId = setInterval(() => {
            setCount((count) => count - 1);
          }, 1000);
    
          return () => clearInterval(intervalId);
        } else {
          setCount(0);
        }
      }, [isFocus]);
    
    //   useEffect(() => {
    //     if (isFocus) {
    //       if (count === 0) {
    //         // getPassengerRequestList();
    //         setCount(20);
    //       }
    //     }
    //     return () => {
    //         console.log('Cleaning up effect');
    //       };
    //   }, [count, isFocus]);
    // console.log(detail,'final page')

    useEffect(() => {
        if(isFocus)
        {
        getDriverList()
        getProfile()
        }
        return () => {
            console.log('Cleaning up effect');
          };

    }, [isFocus])
 

    //  useEffect(()=>{
    //     let subs = props.navigation.addListener("focus",()=>{
    //         getDriverList(props?.route?.params?.vehicleType)
    //         getProfile()
    //     })
    //     return()=>subs;
    //  },[])
    useEffect(()=>{
        BackHandler.addEventListener('hardwareBackPress',handleBackClick);
        return ()=>{
            BackHandler.removeEventListener('hardwareBackPress',handleBackClick)
        }
    },[])



    const markerData=[{
        id:1,
        latitude: location?.latitude,
        longitude: location?.longitude,
    },
    {
        id:2,
        latitude: location?.latitude,
        longitude: location?.longitude,
    },
    {
        id:3,
        latitude: location?.latitude,
        longitude: location?.longitude,
    },
    {
        id:4,
        latitude: location?.latitude,
        longitude: location?.longitude,
    },
    ]

    const handleBackClick=()=>{
        props.navigation.navigate("ScheduleTrip")
        return true
    }
    const getProfile=async()=>{
        let data= await AsyncStorage.getItem("Profile")
        setProfileData(JSON.parse(data))
            }

    // console.log("location for driver", location, vehicleType,jwtToken)
    const handleRoute = () => {
        if (val == "advance") {
            // props.navigation.navigate('BookingProcess')
        }
        else {
            // props.navigation.navigate('DriverApproval')
            sendRequest()

        }
    }

    const getDriverList = async () => {
        console.log("vichle type",vehicleType,'this is final')
        let data = {
            lat: location?.latitude,
            long: location?.longitude,
            vehicleType: vehicleType
        }
        console.log(data,'This is url body')
        await fetch(baseUrl+"user/find-driver", {
            method: "POST",
            headers:{
                'Content-Type':'application/json',
                'Authorization':jwtToken
            },
            body:JSON.stringify(data)
        })
        .then(async (res) => {
                let jsonData =await res.json()
                return jsonData
            })
            .then((res) => {
                console.log("driver api wala", res)
                if(res.code==200)
                {
                    setDriverData(res.result)
                }
                else{
                    console.log("driver else")
                    setDriverData("")
                    console.log(data,'error catch')
                }
            })
    }
    // console.log("driverData=====>",driverData)
    const mapStyle = [
        { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
        {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{ color: '#263c3f' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#6b9a76' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{ color: '#38414e' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#212a37' }],
        },
        {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#9ca5b3' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{ color: '#746855' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#1f2835' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#f3d19c' }],
        },
        {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{ color: '#2f3948' }],
        },
        {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{ color: '#17263c' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#515c6d' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{ color: '#17263c' }],
        },
    ];
    let data = [
        {
            id: 1,
            commentImage: imageConstant.profile,
            review: "Sophia is a lovely person, we got in touch after our first trip and we ended up being best friends! ",
            commentRating: 4.3,
            date: "14/02/2021 at 5.24 p.m"
        },
        {
            id: 2,
            commentImage: imageConstant.profile,
            review: "The trip was super cool, Sophia was very nice and funny! I recommend her :) ",
            commentRating: 4.5,
            date: "22/05/2022 at 1.48 p.m"
        }
    ]
    let data2 = [
        {
            id: 1,
            img: imageConstant.taxi,
        },
        {
            id: 2,
            img: imageConstant.taxi3,
        },
        {
            id: 3,
            img: imageConstant.taxi,
        },
        {
            id: 4,
            img: imageConstant.taxi2,
        },
        {
            id: 5,
            img: imageConstant.taxi2,
        },
        {
            id: 6,
            img: imageConstant.taxi,
        },
        {
            id: 7,
            img: imageConstant.taxi3,
        },
        {
            id: 8,
            img: imageConstant.taxi,
        }
    ]

    let data3 = [
        {
            id: 1,
            title: 'Sonia',
            color: 'Grey',
            img: imageConstant.profile2
        },
        {
            id: 2,
            title: 'Sonia',
            color: 'Black',
            img: imageConstant.profile2
        }
    ]
    const handleBack=async()=>{
        const keys = await AsyncStorage.getAllKeys();
      await AsyncStorage.multiRemove(keys);
      await AsyncStorage.clear();
      props.navigation.navigate("ScheduleTrip")
    // props.navigation.navigate('DriverApproval')
    }
    const renderDetails = ({ item, index }) => {
        // setBtn(!btn)
        setBtn(false)
        console.log("flatlist",item)
        return (
            <View key={index}>
                <TouchableOpacity
                    onPress={() => handleClick(index,item?.drivers[0]?._id)}
                    style={[styles.touch, { backgroundColor: SelectedId == index ? 'rgba(108, 0, 192, 0.08)' : '#FFFFFF' }]}>
                    <Pressable onPress={() => setModalVisible(!modalVisible)}>
                        <Image
                        resizeMode='contain'
                        style={{height:40,width:40,borderRadius:20}}
                            source={item?.drivers[0]?.profilePic ? {uri:item?.drivers[0]?.profilePic} :imageConstant.profileDemo} />
                    </Pressable>
                    <View>

                        <Text style={{ color: '#000000', fontSize: 15 }}>{item?.drivers[0]?.name}</Text>
                        <Text style={{ fontSize: 11, color: '#000000' }}>{item?.model}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={imageConstant.star} />
                        <Text style={{ fontSize: 20, color: '#000000' }}>4.3</Text>
                    </View>
                    <View>
                        <Text style={{ color: '#000000', fontSize: 15 }}>{item?.color}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={imageConstant.Timer}
                        />
                        <Text style={{ color: '#000000', fontSize: 15 }}>9 min</Text>
                    </View>
                </TouchableOpacity>

            </View>
        )
    }

    const handleClick = (id,rideId) => {
        setSelectedId(id)
        setStatus(true)
        setDeriverId(rideId)
    }
const sendRequest=async()=>{
    dispatch(action.setDriverId(deriverId))
   
    let data={
        "driverId": deriverId,
    "startLocation": startPoint,
    "startLat": location?.latitude,
    "startLong": location?.longitude,
    "endLat": detail?.lat,
    "endLong": detail?.lng,
    "endLocation":endPoint,
    "noOfPassenger": passengerCount,
    "distance": distance
    }
    console.log("ridewala",data)
    try{
        await fetch(baseUrl+"user/ride-request",{
            method:'POST',
            headers:{
                'Content-Type':'application/json',
                'Authorization':jwtToken
            },
            body:JSON.stringify(data)
        })
        .then(async(res)=>{
            let jsonData=await res.json()
            return jsonData
        })
        .then((res)=>{
            console.log("ride Request----->",res?.addRide?._id)
            dispatch(action.setRequestId(res?.addRide?._id))
            if(res.code==200)
            {
                
                if (val == "advance") {
                    props.navigation.navigate('BookingProcess')
                }
                else {
                    props.navigation.navigate('DriverApproval')
                    // sendRequest()
        
                }
            }
        })
    }
    catch(err)
    {
        console.log(err)
    }
    
}

    return (
        <View style={styles.main}>
            <Text>Request</Text>
            <MapView
                zoomEnabled={true}
                zoomTapEnabled={true}
                style={styles.mapStyle}
                region={{
                    latitude:parseFloat(location?.latitude),
                    longitude:parseFloat(location?.longitude) ,
                    latitudeDelta:0.4,
                    longitudeDelta:0.5
                  }}
                customMapStyle={mapStyle}>
              <Marker
              // icon={imageConstant.location}
              width={30}
              height={30}
              draggable
              coordinate={{
                latitude: location?.latitude,
                longitude: location?.longitude,
              }}
              onDragEnd={
                (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
              }
              title={'Start Location'}
              description={''}
            >
                <Image resizeMode='contain' source={imageConstant.startPoint} style={{height:32,width:32}}/>

              </Marker>
              <MapViewDirections
                               origin={{latitude:location?.latitude,longitude:location?.longitude}}
                               destination={{latitude:detail?.lat,longitude:detail?.lng}}
                               apikey={googleKey}
                               strokeColor={'red'}
                               strokeWidth={3}
                             />
                             <Marker
                             icon={imageConstant.endPoint}
                             // image={imageConstant.location}
                             width={50}
                             height={50}
                                 draggable
                                 coordinate={{
                                     latitude: detail?.lat,
                                     longitude: detail?.lng,
                                 }}
                                 onDragEnd={
                                     (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                                 }
                                 title={'End Location'}
                                 description={''}
                             >
                              <Image resizeMode='contain' source={imageConstant.endPoint} style={{height:32,width:32}}/>
                              </Marker>

                              {markerData?.map((item,index)=>{
                                return(
                                    <Marker
                                    icon={imageConstant.endPoint}
                                    // image={imageConstant.location}
                                    width={50}
                                    height={50}
                                        draggable
                                        coordinate={{
                                            latitude: item?.latitude,
                                            longitude: item?.longitude,
                                        }}
                                        onDragEnd={
                                            (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                                        }
                                        title={'End Location'}
                                        description={''}
                                    >
                                     <Image resizeMode='contain' source={imageConstant.Tesla} style={{height:52,width:52}}/>
                                     </Marker>
                                )
                              })}

            </MapView>
            <View style={styles.view}>
                <Pressable onPress={() => props.navigation.toggleDrawer()}>
                    <Image
                        resizeMode="contain"
                        style={{ height: 42, width: 42 }}
                        source={imageConstant.menuIcon}
                    />
                </Pressable>
                <View >
                    <Pressable onPress={() => props.navigation.navigate("Profile")}>
                        <Image
                            resizeMode='contain'
                            style={styles.img1}
                            source={profileData?.userData?.profilePic ? {uri:profileData?.userData?.profilePic} :imageConstant.profileDemo}
                        />
                    </Pressable>
                </View>
            </View>
            <Image
                style={styles.img2}
                source={imageConstant.Zooms}
            />
            <View style={styles.view1}>
                <Image
                    resizeMode='contain'
                    style={styles.img3}
                    source={imageConstant.Rect}
                />
                <Text style={{ fontSize: 9, marginTop: Platform.OS == "ios" ? 2 : 1 }}>Travel safe</Text>
                {value ?

                    <View style={styles.view2}>
                        <Text style={styles.text2}>Available LadyCars Select your driver</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ color: '#000000', fontSize: 17, fontFamily: fontConstant.semibold }}>Random</Text>
                            <CheckBox
                                value={check}
                                style={{ left: Platform.OS === "ios" ? 5 : 2 }}
                                onValueChange={() => setCheck(!check)}
                            />
                        </View>
                    </View>
                    : null}
                <FlatList
                    data={driverData}
                    renderItem={renderDetails}
                    ListEmptyComponent={()=>{
                        setBtn(true)
                        return(
                            <Text style={{marginTop:'15%',fontSize:16}}>Sorry, no driver available in this area</Text>
                        )
                    }}
                />

            </View>
            {btn ?
            <CustomButton
            buttonName={'Try Again'}
            backgroundColor={colorConstant.theme}
            position={'absolute'}
            bottom={10}
            borderRadius={10}
            // OnButtonPress={() =>props.navigation.navigate("ScheduleTrip")}
            OnButtonPress={handleBack}
            height={38}
        />
        :
            
            <CustomButton
                buttonName={'CONFIRM'}
                backgroundColor={colorConstant.theme}
                position={'absolute'}
                bottom={10}
                borderRadius={10}
                OnButtonPress={() => sendRequest()}
                height={38}
            />
            }
            <CustomModal
                animationType={"slide"}
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    // Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}
                modalText={"hello"}
                modalImage={imageConstant.modalImage}
                driverName={"SONIA"}
                rating={4.9}
                intro={"I’m Sophia from Nevada. I love swimming. Riding with people is a passion for me and it allows me to meet new people and discover new places. "}
                data={data}
                data2={data2}
            //  status={modalVisible}
            />
        </View>
    )
}

export default DriverSelect
const styles = StyleSheet.create({
    main: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    mapStyle: {
        flex: 2,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: '48%',
    },
    view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Width
    },
    view1: {
        backgroundColor: '#FFFFFF',
        height: '40%',
        position: 'absolute',
        top: '52%',
        width: Width,
        alignItems: 'center',


    },
    text: {
        color: '#000000',
        fontFamily: fontConstant.bold,
        fontSize: 17,
        marginTop: 10,
    },
    view2: {
        width: Platform.OS === 'ios' ? '93%' : '95%',
        backgroundColor: '#FFFFFF',
        height: '20%',
        // borderTopWidth:1,
        marginTop: '3%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        // marginTop:'5%',
        alignItems: 'center'
    },
    text2: {
        color: colorConstant.theme,
        fontFamily: fontConstant.bold,
        fontSize: 19,
        width: '60%'
    },
    view3: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // alignItems:'center',
        marginTop: '8%'
    },
    cancelText: {
        alignSelf: 'center',
        color: colorConstant.theme,
        marginTop: '15%'
    },
    img1: {
        borderWidth: 2,
        borderColor: '#FFFFFF',
        right: 5,
        height: 33,
        width: 33,
        borderRadius:16.5,
    },
    img2: {
        alignSelf: 'flex-end',
        right: 5,
        top: 15
    },
    img3: {
        marginTop: '4%',
        width: 77,
        height: 6
    },
    touch: {
        flexDirection: 'row',
        height: 80,
        width: Width,
        justifyContent: 'space-around',
        alignItems: 'center'
    }
})