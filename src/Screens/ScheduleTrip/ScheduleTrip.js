import { View, Text,StyleSheet, TouchableOpacity, Image, Modal, Pressable } from 'react-native'
import React from 'react'
import MapView, { PROVIDER_GOOGLE,Marker, Polyline } from 'react-native-maps';
import { colorConstant, imageConstant } from '../../utils/constant';
import CustomButton from '../../custom/CustomButton';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { useState } from 'react';
import { useEffect } from 'react';
import Geocoder from 'react-native-geocoding';
import Geolocation from '@react-native-community/geolocation';
import { Width } from '../../dimensions/dimension';
import CheckBox from '@react-native-community/checkbox';
import { getDistance,getPreciseDistance } from 'geolib';
import toastShow from '../../utils/Toast';
import { baseUrl, googleKey } from '../../utils/Urls';
import { useDispatch, useSelector } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { action } from '../../redux/reducers';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import MapViewDirections from 'react-native-maps-directions';
import { set } from 'react-native-reanimated';


let data;
  let start;
  let detail;
  let fotDest,startData,startAddress
const ScheduleTrip = (props) => {
  const [location,setLocation]=useState(null)
const [address,setAddress]=useState('')
const [destination,setDestination]=useState('')
const [check,setCheck]=useState(false)
const [latitudes,setLatitudes]=useState(28.5355)
const [longitudes,setLongitude]=useState(77.3910)
const [loading,setLoading]=useState(false)
const [profileData,setProfileData]=useState(null)
const [lets,setLet]=useState(0)
const [lngs,setLng]=useState(0)
const [count, setCount]=useState(1)
const [intialAdd,setInitialAdd]=useState("")
const [coordinates, setCordinates] = useState();
const [destinationData,setDestinationData]=useState("")
const dispatch=useDispatch()
const isFocus=useIsFocused()
const [region ,setRegion]=useState({
  latitude:startData?.lat ? startData?.lat: latitudes,
  longitude: startData?.lng ? startData?.lng : longitudes,
  latitudeDelta: 0.1,
  longitudeDelta: 0.1,
})
 useEffect(()=>{
// getPosition()
getProfileData()

if(!props?.route?.params){
  console.log("focus wala",data)
  data=""
  detail=null
  start=""

  // setAddress(intialAdd)
setLet(latitudes)
setLng(longitudes)
  setDestinationData("")
  console.log("undwer focus wala",data)
}
return ()=>{
  console.log("finish")
}

// data=""
 },[isFocus])
useEffect(()=>{
  getPosition()
},
[])
useEffect(()=>{

  if (props?.route?.params) {
    data = props.route.params.data
    start = props.route.params.start 
    detail = props.route.params.detail,
    startData=props.route.params.startData,
    startAddress=props.route.params.startAddress,
   setInitialAdd(startAddress)
    setLet(startData?.lat ? startData?.lat: latitudes)
    setLng(startData?.lng ? startData?.lng: longitudes)
 fotDest=data
 
 setDestinationData(data)
 console.log("props wala",startData)
  }
  else{
    startData=""
    setInitialAdd("")
  }
},[isFocus])
 let {jwtToken}=useSelector((state)=>state.reducers)
  

  console.log("object", detail)

    const mapStyle = [
        {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
        {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
        {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
        {
          featureType: 'administrative.locality',
          elementType: 'labels.text.fill',
          stylers: [{color: '#d59563'}],
        },
        {
          featureType: 'poi',
          elementType: 'labels.text.fill',
          stylers: [{color: '#d59563'}],
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [{color: '#263c3f'}],
        },
        {
          featureType: 'poi.park',
          elementType: 'labels.text.fill',
          stylers: [{color: '#6b9a76'}],
        },
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [{color: '#38414e'}],
        },
        {
          featureType: 'road',
          elementType: 'geometry.stroke',
          stylers: [{color: '#212a37'}],
        },
        {
          featureType: 'road',
          elementType: 'labels.text.fill',
          stylers: [{color: '#9ca5b3'}],
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry',
          stylers: [{color: '#746855'}],
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry.stroke',
          stylers: [{color: '#1f2835'}],
        },
        {
          featureType: 'road.highway',
          elementType: 'labels.text.fill',
          stylers: [{color: '#f3d19c'}],
        },
        {
          featureType: 'transit',
          elementType: 'geometry',
          stylers: [{color: '#2f3948'}],
        },
        {
          featureType: 'transit.station',
          elementType: 'labels.text.fill',
          stylers: [{color: '#d59563'}],
        },
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [{color: '#17263c'}],
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [{color: '#515c6d'}],
        },
        {
          featureType: 'water',
          elementType: 'labels.text.stroke',
          stylers: [{color: '#17263c'}],
        },
      ];





const  getPosition = async() => {
  // fotDest=null
  Geolocation.getCurrentPosition(
    position => {
      setLocation(position.coords)
      const currentLongitude =
          JSON.stringify(position.coords.longitude);
          console.log("current position",currentLongitude)
          setLatitudes(position.coords.latitude)
          setLongitude(position.coords.longitude)
          setLet(position.coords.latitude)
          setLng(position.coords.longitude)
      console.log('position',position)
      Geocoder.init('AIzaSyAU6KwRMvEmhQbruXnj60cwiORuKQ5BohQ');
      Geocoder.from({
        latitude:position?.coords.latitude,
        longitude:position?.coords.longitude
      })
        .then(json => {
          // var location = json.results[0].geometry.location;
      setAddress(json.results[0].formatted_address);
      setInitialAdd(json.results[0].formatted_address);
      // console.log("final address stringify",JSON.stringify(json.results[0]));
          
        })
        .catch(error =>{
          console.log("error www",error)
        });
    },
    error => {
      console.log("error error",error);
    },
    {enableHighAccuracy:false, timeout: 3000, maximumAge: 3000},
  )
  // console.log("location",location)
   
    }
   
    // console.log('location',)
   const distanceCalculate=()=>{
    console.log("data upside",data)
    if(data!="" && data!=null){
      console.log("data inside",data)
      let locationData={
        latitude:lets,
        longitude:lngs
      }
   let distanceData= getPreciseDistance(
      
        {latitude: location?.latitude, longitude: location?.longitude},
        {latitude: detail?.lat, longitude: detail?.lng},
      
    )
    console.log("distance",distanceData/1000)
   props.navigation.navigate('CabBooking',{distance:distanceData,start:start,data:data,detail:detail,location:locationData})
  //  destination=null
  data=""
  startData=""
  setDestination("")
  setLet(latitudes)
  setLng(longitudes)
  
  startAddress=""
  
  
    }
   else
   {
    // data=""
    toastShow("Please select destination location",colorConstant.theme)
   }
   }


   const getProfileData=async()=>{
    let response=await fetch(baseUrl+"user/passenger-view-profile",{
        method:"GET",
        headers:{
            'Authorization':jwtToken
        }
    })
    .then(async(res)=>{
        let jsonData=await res.json()
        return jsonData
    })
    .then(async(res)=>{
        setProfileData(res)
        await AsyncStorage.setItem("Profile",JSON.stringify(res))
        // console.log("profile ka data",profileData)
        // distance=null
        // fotDest=null
        
    })
  
    }
const handleSub=()=>{
  if(count>1)
  {
    setCount(count-1)
    dispatch(action.setPassengerCount(count))
  }
  
}
const handleAdd=()=>{
  if(count<=3)
  {
    setCount(count+1)
    dispatch(action.setPassengerCount(count))
  }
  
}
console.log("intialAdd",intialAdd)
console.log("Address",address)
  return (
    
    <View style={styles.main}>
     
     {console.log(data,"wwwwwwwwwwwwwwwwww")}
          <MapView
          zoomEnabled={true}
          zoomTapEnabled={true}
          provider={PROVIDER_GOOGLE}
          // mapType='standard'
          // showsUserLocation
              style={styles.mapStyle}
              // initialRegion={region}
              region={{
                latitude:parseFloat(lets),
                longitude:parseFloat(lngs) ,
                latitudeDelta:0.4,
                longitudeDelta:0.5
              }}
              // onRegionChange={(region)=>{
              //   setRegion({region})
              // }}
              customMapStyle={mapStyle}>
     <Marker
             
              width={40}
              height={40}
                  draggable
                  coordinate={{
                      latitude: lets,
                      longitude: lngs,
                  }}
                  onDragEnd={
                      (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                  }
                  title={'Test Marker'}
                  description={'This is a description of the marker'}
              >
                <Image resizeMode='contain' source={imageConstant.startPoint} style={{height:32,width:32}}/>
                </Marker>
            {detail?.lat ?  <>
                               <MapViewDirections
                               origin={{latitude:lets,longitude:lngs}}
                               destination={{latitude:detail?.lat,longitude:detail?.lng}}
                               apikey={googleKey}
                               strokeColor={'red'}
                               strokeWidth={3}
                             />
                             <Marker
                            //  icon={imageConstant.endPoint}
                             // image={imageConstant.location}
                             width={50}
                             height={50}
                                 draggable
                                 coordinate={{
                                     latitude: detail?.lat,
                                     longitude: detail?.lng,
                                 }}
                                 onDragEnd={
                                     (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                                 }
                                 title={'Test Marker'}
                                 description={'This is a description of the marker'}
                             >
                              <Image resizeMode='contain' source={imageConstant.endPoint} style={{height:32,width:32}}/>
                              </Marker>
                             </>
              :null}
          </MapView>
          <View style={styles.view}>
            

        <Pressable
        style={{marginTop:'3%'}}
         onPress={() => props.navigation.toggleDrawer()}>
          <Image
            source={imageConstant.menuIcon}
            style={{height:42,width:42}}
            resizeMode='contain'
          />
        </Pressable>

                <Pressable
                style={{marginTop:'3%'}}
                onPress={()=>props.navigation.navigate("Profile")} >
                    <Image
                        style={styles.img1}
                        source={profileData?.userData?.profilePic ? {uri:profileData?.userData?.profilePic} : imageConstant.profileDemo}
                        resizeMode='contain'
                    />
                </Pressable>
            </View>
            <Image
                style={styles.img2}
                source={imageConstant.Zooms}
            />
          <View style={{flex:2,alignItems:'center',backgroundColor:'#FFFFFF'}}>
          </View>
          <View style={styles.view1}>
            <Text style={{color:'#555555',fontSize:17,left:7}}>Number of passenger?</Text>
            {/* <Image
            source={imageConstant.redRight}
            /> */}
            {/* <CheckBox
            disabled={false}
            value={check}
            onValueChange={()=>setCheck(!check)}
            /> */}
            
            <View style={{flexDirection:'row',justifyContent:'space-between',width:'25%',alignItems:'center'}}>
              <Text onPress={()=>handleAdd()} style={{fontSize:20,paddingHorizontal:5}}>+</Text>
              <Text style={{fontSize:20,color:colorConstant.theme}}>{count}</Text>
              <Text onPress={()=>handleSub()} style={{fontSize:20,paddingHorizontal:5}}>-</Text>
            </View>
            </View>
            <TouchableOpacity activeOpacity={0.8}  onPress={getPosition} style={[styles.touch,{bottom:'33%',flexDirection:'row',alignItems:'center',justifyContent:'space-around'}]}>
              <Image
              resizeMode='contain'
              source={imageConstant.Car}
             style={{height:26,width:37,top:3}}
              />
                <Text numberOfLines={2} style={address ? {fontSize:12,width:150}:{color:'#5C5C5C',fontSize:18,width:150}} >{intialAdd ? intialAdd : address ? address : "Start"}</Text>
                <Image
                resizeMode='contain'
              source={imageConstant.polygon}
              style={{width:15,height:14}}
              
              />
            </TouchableOpacity>
            <TouchableOpacity 
            activeOpacity={0.8}
            onPress={()=>props.navigation.navigate('Destination',{address:address})}
            style={[styles.touch,{bottom:'24%',flexDirection:'row',justifyContent:'space-around'}]}>
            <Image
              resizeMode='contain'
              source={imageConstant.Car}
             style={{height:26,width:37,top:3}}
              />
                <Text numberOfLines={2} style={data ?{fontSize:12,color:'#5C5C5C',width:150}:{color:'#5C5C5C',fontSize:18,width:150}} >{destinationData ? destinationData : "Destination"}</Text>
                <Image
                resizeMode='contain'
              source={imageConstant.downArrow}
              style={{width:15,height:14}}
              
              />
            </TouchableOpacity>
            <Pressable
            onPress={()=>props.navigation.navigate("ScheduleRide")}
            style={{position:'absolute',bottom:'13%',}}>
            <Image
            resizeMode='contain'
            source={imageConstant.calen}
            style={{height:50,width:50,alignSelf:'center'}}
            />
            <Text style={{color:'#555555',fontSize:14,marginTop:-0}}>Schedule a Trip</Text>
            </Pressable>
            
            <CustomButton
            buttonName={'CONFIRM'}
            position={'absolute'}
            bottom={'5%'}
            borderRadius={10}
            // OnButtonPress={()=>props.navigation.navigate('CabBooking')}

            OnButtonPress={()=>distanceCalculate()}
            />
  

    </View>
  )
}

export default ScheduleTrip
const styles=StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
    mapStyle: {
        flex:2,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: '50%',
      },
      view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Width
    },
      img1: {
        borderWidth: 2,
        borderColor: '#FFFFFF',
        borderRadius: 100,
      height:33,
      width:33,
        right: 5
    },
    img2: {
      alignSelf: 'flex-end',
      right: 5,
      top: 15,
  },
      touch:{
        position:'absolute',
        width:313,
        backgroundColor:'#DEDEDE',
        alignItems:'center',
        justifyContent:'center',
        height:49,
        borderRadius:10
      },
      view1:{
        position:'absolute',
        bottom:'44%',
        flexDirection:'row',
        width:Width *0.8,
        justifyContent:'space-between',
        alignItems:'center',
      }
  
})