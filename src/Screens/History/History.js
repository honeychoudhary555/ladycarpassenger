import { View, Text, StyleSheet, Image, Pressable } from 'react-native'
import React from 'react'
import { fontConstant, imageConstant } from '../../utils/constant'
import { FlatList } from 'react-native-gesture-handler'
import { Width } from '../../dimensions/dimension'
import CustomHeader from '../../custom/CustomHeader'

const History = (props) => {
  const data = [
    {
      id: 1,
      dollar: 13,
      status:"Completed",
      img:imageConstant.tick
    },
    {
      id: 2,
      dollar: 24.88,
      status:"Canceled",
      img:imageConstant.cancelSmall
    },
    {
      id: 3,
      dollar: 24.88,
      status:"Completed",
      img:imageConstant.tick
    },
    {
      id: 4,
      dollar: 24.88,
      status:"Canceled",
      img:imageConstant.cancelSmall
    }
  ]
  const renderData = ({ item, index }) => {
    return (
      <Pressable
      onPress={()=>props.navigation.navigate("TripDetails",{status:item.status})}
      style={[styles.view,{marginTop:25}]}>
        <Image
        resizeMode='contain'
        style={{width:46,height:46,left:'10%'}}
          source={imageConstant.profile2}
        />
        <View>
          <Text style={styles.text2}>57 Washington Rd</Text>
          
          <View style={{flexDirection:'row',width:'70%',justifyContent:'space-around',alignItems:'center'}}>
          <Text style={styles.text3}>$13</Text>
          <Text style={styles.dateText}>Sep 19  12:25 PM</Text>
          <View style={{flexDirection:'row',alignItems:'center',width:'30%'}}>
            <Image
            resizeMode='contain'
           style={{height:10,width:10}}
            source={item.img}
            />
            <Text style={styles.status}>{item.status}</Text>
          </View>
          </View>
        </View>
        {/* <Image
          source={imageConstant.check} /> */}
        <Image
          resizeMode='contain'
          style={styles.nextArrow}
          source={imageConstant.nextArrow}
        />
      </Pressable>
    )
  }
  return (
    <View style={styles.main}>
      <CustomHeader
      title={"History"}
      onPressBack={()=>props.navigation.goBack()}

      />
      {/* <Image
        style={styles.img1}
        source={imageConstant.menuIcon}
      />
      <Text style={styles.welcomText}>History</Text> */}
      <View style={{flexDirection:'row',alignItems:'center',alignSelf:'flex-start',left:'2%',width:Width * 0.8,justifyContent:'space-between',top:'10%'}}>
      <Text style={[styles.text1,{left:0,marginTop:'3%'}]}>Upcoming trips</Text>
      {/* <View style={{flexDirection:'row',alignItems:'center',width:'50%',justifyContent:'space-evenly'}}>
      <Image
        style={{height:38,width:38}}
        source={imageConstant.chrono}
        />
        <Image
        style={{height:25,width:25}}
        source={imageConstant.Phone}
        />
        <Image
        style={{height:25,width:25}}
        source={imageConstant.Speech}
        />
      </View> */}
      </View>
      <Pressable
      onPress={()=>props.navigation.navigate("TripDetails",{status:"Upcoming"})}
      style={styles.view}>
        <Image
        resizeMode='contain'
        style={{width:46,height:46,left:'10%'}}
          source={imageConstant.profile2}
        />
        <View>
          <Text style={styles.text2}>57 Washington Rd</Text>
          
          <View style={{flexDirection:'row',width:'70%',justifyContent:'space-around',alignItems:'center'}}>
          <Text style={styles.text3}>$13</Text>
          <Text style={styles.dateText}>Sep 19  12:25 PM</Text>
          <View style={{flexDirection:'row',alignItems:'center',width:'30%'}}>
            <Image
            source={imageConstant.Ellipse}
            />
            <Text style={styles.status}>Upcoming</Text>
          </View>
          </View>
        </View>
        {/* <Image
          source={imageConstant.check} /> */}
        <Image
          resizeMode='contain'
          style={styles.nextArrow}
          source={imageConstant.nextArrow}
        />
      </Pressable>
      <Text style={styles.text1}>Last Trips</Text>

      <FlatList
      bounces={false}
        data={data}
        renderItem={renderData}
        
      />
    </View>
  )
}

export default History
const styles = StyleSheet.create({
  main: {
    flex: 1,
    // alignItems: 'center',
    backgroundColor: '#FFFFFF'
  },
  img1: {
    alignSelf: 'flex-start',
    left: 5,
    top: 5
  },
  welcomText: {
    color: '#6C00C0',
    fontFamily: fontConstant.bold,
    fontSize: 36,
    width: '80%',
    lineHeight: 37,
    marginTop: '10%',

  },
  text1: {
    color: '#000000',
    fontSize: 16,
    fontFamily: fontConstant.bold,
    alignSelf: 'flex-start',
    left: '4%',
    marginTop: '10%'
  },
  text2: {
    color: '#000000',
    fontSize: 16,
    fontFamily: fontConstant.bold
  },
  view: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '15%',
    justifyContent: 'space-around',
    width:'100%',
    height:60,
    // borderBottomWidth:0.2
  },
  nextimg: {
    width: 18,
    height: 30
  },
  text3: {
    color: '#6C00BF',
    fontWeight:'600'
  },
  dateText:{
    fontSize:12,
    color:'#000000'
  },
  status:{
    fontSize:10,
    color:'#FF7F09',
    left:6,
    alignSelf:'flex-end'
  },
  nextArrow:{
    right:'35%',
    height:13,width:13
  }
})