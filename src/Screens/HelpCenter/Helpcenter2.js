import { View, Text, StyleSheet, Image } from 'react-native'
import React from 'react'
import CustomHeader from '../../custom/CustomHeader'
import { FlatList } from 'react-native-gesture-handler'
import { Width } from '../../dimensions/dimension'
import { colorConstant, imageConstant } from '../../utils/constant'

const Helpcenter2 = (props) => {
    const data2=[]
   const data=[
        {
            id:1,
            status:"Pending"
        },
        {
            id:2,
            status:"Resolved"
        },
        {
            id:3,
            status:"Resolved"
        }
    ]
    const renderDetails=({item,index})=>{
        return(
            
<View key={index}
style={styles.view1}>
            <View style={styles.view2}>
            <View>
        <Text style={styles.text1}>TRIPS, PASSENGERS OR SAFEFY ISSUES</Text>
        <Text style={styles.text2}>Ticket ID: <Text style={{color:colorConstant.theme}}>#165465</Text></Text>
        </View>
        <Text style={styles.text3}><Image resizeMode='contain' source={imageConstant.Ellipse}/> {item.status}</Text>
        </View>
        <Image
        resizeMode='contain'
        style={styles.img1}
        source={imageConstant.map}
        />
        <View style={styles.view3}>
            <Image
            resizeMode='contain'
            style={styles.img2}
            source={imageConstant.profile2}
            />
            <View style={{right:24}}>
                <Text style={styles.text4}>SONIA</Text>
                <Text style={styles.text5}><Image source={imageConstant.carRoof}/>  Chevrolet Bolt EV</Text>
            </View>
            <View>
                <Text style={styles.text6}>Fare: <Text style={styles.text7}>$13</Text></Text>
            </View>
        </View>
        </View>
        )
    }
    const emptyData=()=>{
        return(
            <View>
                <Text style={styles.text8}>You did’t raised any complaint yet</Text>
                <Text style={styles.text9}>If you have any complaint please go to booking history for help & suppot</Text>
            </View>
        )
    }
  return (
    <View style={styles.main}>
        <CustomHeader
        title={"Help Center"}
        marginBottom={20}
        onPressBack={()=>props.navigation.goBack()}
        />
        
        <FlatList
        data={data}
        renderItem={renderDetails}
        contentContainerStyle={{marginTop:'5%'}}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={emptyData}
        />
    </View>
  )
}

export default Helpcenter2
const styles=StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
    view1:{
        height:318,
        width:Width * 0.9,
        backgroundColor:'#FFFFFF',
        borderRadius:15,
        borderWidth:1,
        borderColor:'#979797',
        marginTop:'5%',
        
        
        
    },
    text1:{
        color:'#000000',
        fontSize:13
    },
    text2:{
        color:'#000000',
        fontSize:12
    },
    text3:{
        color:'#FF7F09',
        fontSize:10
    },
    view2:{
        width:'90%',
        flexDirection:'row',
        justifyContent:'space-between',
        // paddingHorizontal:20,
        alignItems:'center',
        alignSelf:'center',
        marginTop:'4%'
    },
    img1:{
        height:173,
        width:'100%',
        marginTop:'5%'
    },
    view3:{
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
        marginTop:'5%'
    },
    img2:{
        height:40,
        width:40
    },
    text4:{
        fontSize:15,
        color:'#000000',
        fontWeight:'600'
    },
    text5:{
        fontSize:11,
        color:'#000000'
    },
    text6:{
        fontSize:12,
        color:'#000000'
    },
    text7:{
        fontSize:14,
        color:'#6C00BF',
        fontWeight:'600'
    },
    text8:{
        fontSize:18,
        fontWeight:'700',
        alignSelf:'center',
        color:'#393939',
        marginTop:'55%'
    },
    text9:{
        fontSize:15,
        fontWeight:'400',
        alignSelf:'center',
        color:'#222B45',
        marginTop:'5%',
        width:Width * 0.85,
        textAlign:'center'
    }
})