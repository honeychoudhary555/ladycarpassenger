import { View, Text } from 'react-native'
import React from 'react'
import { useState } from 'react'
import { useSelector } from 'react-redux'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Landing from '../Screens/Landing/Landing'
import IntroStack from './IntroStack'
import AuthStack from './AuthStack'
import { useFocusEffect } from '@react-navigation/native'
import MainStack from './MainStack'

const Stack=createNativeStackNavigator()
const RootNavigatior = () => {
    const [introStatus, setIntroStatus] = useState();
    let data = useSelector((state) => state.reducers);
    // useFocusEffect(() => {
    //     setIntroStatus(data?.introStatus);
    // })

    console.log("datadata",data)
  return (
   <Stack.Navigator 
    screenOptions={{
    headerShown: false
}}>
{
    data?.introStatus==null &&(
    <Stack.Screen  name="IntroStack" screenOptions={{headerShown:false}} component={IntroStack}/>

    )
}
{data?.introStatus=="login" &&(
    <Stack.Screen name='AuthStack' component={AuthStack}/>
)}
{data?.introStatus=="main" &&(
    <Stack.Screen name='MainStack' component={MainStack}/>
)}
   </Stack.Navigator>
  )
}

export default RootNavigatior