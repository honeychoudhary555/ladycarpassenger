import { View, Text,StyleSheet, Image, TouchableOpacity, Pressable } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import { Width } from '../../dimensions/dimension'
import { useState } from 'react'
import CustomButton from '../../custom/CustomButton'

const Payment = (props) => {
    const [isSelected, setSelection] = useState(false);

  return (
    <View style={styles.main}>
      <Pressable 
      style={styles.pres1}
      onPress={()=>props.navigation.goBack()}>
      <Image
      resizeMode='contain'
      style={styles.image}
      source={imageConstant.Arrow}/>
      </Pressable>
      <View style={styles.view1}>
        <View style={styles.view2}>
      <Image
        source={imageConstant.PayPal}
        />
        <Text style={styles.paypalText}>Paypal</Text>
        </View>
        <TouchableOpacity activeOpacity={0.9} onPress={()=>setSelection(!isSelected)} style={{width:20,height:20,borderWidth:1.5,borderRadius:10,borderColor:colorConstant.theme,
                    backgroundColor:isSelected ? colorConstant.theme : null}}></TouchableOpacity>
      </View>
      <Text style={styles.creditText}>Credit & Debit Cards</Text>
      <Image
      resizeMode='contain'
      style={styles.image1}
      source={imageConstant.CreditCard}
      />
   <View style={styles.view3}>
    <Pressable 
    onPress={()=>props.navigation.navigate('AddCard')}
    style={styles.view4}>
        <Text style={styles.text1}>+</Text>
    </Pressable>
    <Text style={styles.text2}>Add New Card</Text>
   </View>
   <CustomButton
   buttonName={'SET PAYMENT METHOD'}
   top={'25%'}
   borderRadius={10}
   OnButtonPress={()=>props.navigation.navigate("CabBooking")}
   />
    </View>
  )
}

export default Payment
const styles=StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
  pres1:{
    alignSelf:'flex-start',
        left:'4%',
        marginTop:'5%'

  },
    image:{
        
        width:27,
        height:30,
    },
    view1:{
        flexDirection:'row',
        height:38,
        width:Width * 0.9,
        justifyContent:'space-between',
        borderWidth:1,
        borderColor:'#E8E8E8',
        alignItems:'center',
        borderRadius:10,
        marginTop:'10%',
        paddingHorizontal:20
    },
    view2:{
        flexDirection:'row',
        width:'30%',
        justifyContent:'space-between'
    },
    paypalText:{
        color:'#555555',
        fontSize:15,
        fontFamily:fontConstant.semibold
    },
    creditText:{
        color:'#000000',
        fontSize:19,
        fontFamily:fontConstant.bold,
        alignSelf:'flex-start',
        left:'5%',
        marginTop:'10%'
    },
   image1:{
    marginTop:'10%'
   },
   view3:{
    width:Width * 0.4,
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    marginTop:'5%'
   },
   view4:{
    height:30,
    width:30,
    borderRadius:7,
    backgroundColor:'#F7EDFF',
    justifyContent:'center',
    alignItems:'center'
   },
   text1:{
    fontSize:22,
    color:'#6C00C0'
   },
   text2:{
    fontSize:17,
    color:'#606060'
   }
})