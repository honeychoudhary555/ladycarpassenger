import { View, Text,StyleSheet, TextInput, Alert, Modal, Button, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import { colorConstant, fontConstant } from '../../utils/constant'
import CustomButton from '../../custom/CustomButton'
// import {
//     CodeField,
//     Cursor,
//     useBlurOnFulfill,
//     useClearByFocusCell,
//   } from 'react-native-confirmation-code-field';
import CustomModal from '../../custom/CustomModal';
import { heightPercentageToDP } from '../../utils/responsiveFIle';
import CustomHeader from '../../custom/CustomHeader';
import { ScrollView } from 'react-native-gesture-handler';
import { Height } from '../../dimensions/dimension';
import { useEffect } from 'react';
import auth from '@react-native-firebase/auth';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import toastShow from '../../utils/Toast';
import LottieView from 'lottie-react-native';
import { baseUrl } from '../../utils/Urls';
import AsyncStorage from '@react-native-async-storage/async-storage';
import validation from '../../utils/validation';
import { useSelector } from 'react-redux';
  const CELL_COUNT = 4;

const VerificationCode = (props) => {
    const [value, setValue] = useState('');
    const [modalVisible,setModalVisible]=useState(false)
    const [confirm,setConFirm]=useState(null)
    const [code,setCode] = useState("");
    const [status,setStatus]=useState(true)
    const [visible,setVisible]=useState(false)
    const [seconds, setSeconds] = useState(30);
    const [isTimerRunning, setIsTimerRunning] = useState(true)
    const { jwtToken } = useSelector((state) => state.reducers)
    let id,navtext,phone
    if(props?.route?.params)
    {
      id=props?.route?.params?.id
      navtext=props?.route?.params.text
     phone=props?.route?.params?.phone
     callingCode=props?.route?.params?.callingCode
    }
   
    // const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
    // const [prop, getCellOnLayoutHandler] = useClearByFocusCell({
    //   value,
    //   setValue,
    // });

    useEffect(()=>{
      // verfiyPhoneNumber()
      // setInterval(()=>{})
    },[])
  
    useEffect(() => {
      let interval = null;
      if (isTimerRunning) {
        interval = setInterval(() => {
          setSeconds((seconds) => {
            if (seconds === 0) {
              clearInterval(interval);
              setIsTimerRunning(false);
              return 30;
            }
            return seconds - 1;
          });
        }, 1000);
      }
      return () => clearInterval(interval);
    }, [isTimerRunning, seconds]);
    const handleNavigate=()=>{
      console.log("phoen",phone)
      if(id=="ForgetPassword")
      {
        props.navigation.navigate("ResetPassword",{text:navtext,phone:phone,userInfo:null,callingCode:callingCode})
      }
      if(id=="signup")
      {
        props.navigation.navigate("SignUp",{phone:phone,userInfo:null,id:"phoneSiginIn",callingCode:callingCode})
      }
      if(id=="emailSignIn")
      {
        props.navigation.navigate('IdPicture')
      }
      if(id=="ChangePhone")
      {
        setProfileData()
      }
    }



  const setProfileData = async () => {
   
    if(validation.phoneValid(phone)
    ){
    let data = {
        phoneNumber:callingCode+phone, 
    }
console.log(data)
    await fetch(baseUrl+ "user/passenger-edit-profile",
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': jwtToken
            },
            body:JSON.stringify(data)
        })
        .then(async(res)=>{
         
          let jsonData=await res.json()
          return jsonData
        })
        .then(async(res)=>{
         
            console.log("setProfile wala",res)
            // setStatus(false)
            // getProfile()
            if(res.code==200){
            await AsyncStorage.setItem("Profile",JSON.stringify(res))
            props.navigation.navigate("MyAccount")
           
            }
        })
    }
}




    const verfiyPhoneNumber=async()=>{
      let num="+"+callingCode+phone
      let confirmation=await auth().verifyPhoneNumber(num)
      // var appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');/

      console.log("confirm wala",confirmation)
      setConFirm(confirmation)
    }

    const resendOtp = () => {
      setIsTimerRunning(true)
    verfiyPhoneNumber(phone);
      // setCode("");
      // setConFirm(null);
    };

    const confirmCode=async()=>{
      // setVisible(true)
      try{
          const cred= auth.PhoneAuthProvider.credential(confirm?.verificationId,value)
          console.log("credn new",cred)
          let userData = await auth().signInWithCredential(cred);
            console.log("******&&&check new uid", userData);
            // setVisible(false)
            
            if(id=="ForgetPassword")
            {
              props.navigation.navigate("ResetPassword",{text:navtext,phone:phone,callingCode:callingCode})
            }
            if(id=="signup")
            {
              props.navigation.navigate("SignUp",{phone:phone,userInfo:null,aplpleRes:"",callingCode:callingCode})
            }
            if(id=="emailSignIn")
            {
              props.navigation.navigate('IdPicture')
            }
            if(id=="ChangePhone")
            {
              setProfileData()
            }
      }
      catch(err)
      {
          console.log(err)
          // toastShow("Please enter valid OTP")
      }
    }
    console.log("+"+callingCode+phone)
  return (
    <View style={styles.main}>
      <ScrollView
      bounces={false}
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{backgroundColor:'#FFFFFF',alignItems:'center'}}
      >
      <Text style={styles.welcomText}>Verification code</Text>
      <Text style={styles.txt}>A code has just been sent to your phone number</Text>
      {/* <View style={{flexDirection:'row'}}>
        <TextInput style={styles.textinput}/>
        <TextInput style={styles.textinput}/>
        <TextInput style={styles.textinput}/>
        <TextInput style={styles.textinput}/>
      </View> */}
    
       {/* <CodeField
        ref={ref}
        {...props}
        // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
        value={value}
        onChangeText={setValue}
        cellCount={6}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"
        renderCell={({index, symbol, isFocused}) => (
          <Text
            key={index}
            style={[styles.cell, isFocused && styles.focusCell]}
            onLayout={getCellOnLayoutHandler(index)}>
            {symbol || (isFocused ? <Cursor /> : null)}
          </Text>
        )}
      /> */}
      <OTPInputView
      pinCount={6}
      autoFocusOnLoad={true}
      codeInputFieldStyle={styles.underlineStyleBase}
      codeInputHighlightStyle={styles.underlineStyleHighLighted}
      style={{
        width: '90%',
        height: Height * 0.090,
        alignSelf: 'center',
        marginTop: 10
    }}
    onCodeFilled = {(code => {
      setValue(code)
      setTimeout(()=>{
        setStatus(false)
      },2000)
      
      console.log(`Code is ${code}, you are good to go!`)
})}
      />
         <Text style={styles.text3}>Dont receive the Otp? <Text style={{color:colorConstant.theme}}>{seconds} Sec</Text> </Text>
          <TouchableOpacity onPress={resendOtp}>
            {<Text
              style={[
                styles.text3,
                {
                  color:isTimerRunning ? 'gray'  : colorConstant.theme,
                  fontSize: 16,
                  fontFamily: fontConstant.bold,
            
                },
              ]}
            >
              {"  "}Resend OTP
            </Text>}
          </TouchableOpacity>
      {/* <Button title='ok' onPress={verfiyPhoneNumber}/> */}
      {/* {status ? */}
        <CustomButton
      buttonName={'Continue'}
      marginTop={"99%"}
      // position={'absolute'}
      borderRadius={10}
      // bottom={'8%'}
      // disabled={status ? true :false}
      OnButtonPress={()=>handleNavigate()}
          alignSelf={'center'}
      />
      
      {/* // :null} */}
      <CustomModal
       animationType={'none'}
       transparent={true}
       modalText={'Invalid Code'}
       visible={modalVisible}
       onRequestClose={()=>setModalVisible(!modalVisible)}
/>

</ScrollView>
<Modal
        visible={visible}
        transparent={true}
        onRequestClose={()=>setVisible(!visible)}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            backgroundColor:'rgba(0,0,0,0.6)'
          }}
        >
          <View>
            <LottieView source={require('../../asset/images/loader.json')} style={{ height:100 }} autoPlay />
          </View>
        </View>
      </Modal>
      </View>
     
    
  )
}

export default VerificationCode
const styles=StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
    welcomText:{
        color:'#6C00C0',
        fontFamily:fontConstant.semibold,
        fontSize:33,
        width:'80%',
        lineHeight:37,
        textAlign:'center',
        marginTop:'17%'
    },
    txt:{
        fontSize:11,
        color:'#7A7A7A',
        fontFamily:fontConstant.semibold,
        marginTop:'10%',
        // fontWeight:'700'
        
    },
    title: {textAlign: 'center', fontSize: 30},
  codeFieldRoot: {marginTop: 20},
  cell: {
    width: 50,
    height: 50,
    lineHeight: 38,
    fontSize: 24,
    borderWidth: 2,
    borderColor: '#00000030',
    textAlign: 'center',
    margin:5,
    borderRadius:10,
    paddingTop:7
  },
  focusCell: {
    borderColor: '#000',
  },
  mainModal:{
    
    justifyContent:"center",
    alignItems:"center",
    backgroundColor:'#D9D9D9',
    height:112,
    borderRadius:15,
    width:'80%',
    alignSelf:'center'
},
text:{
    color:'#5C5C5C',
    fontSize:17
},
underlineStyleBase: {
  width:50,
  height:50,
  fontSize:22,
  borderWidth:1,
  color:colorConstant.black,
  borderColor:"#D4D4D4",
  borderRadius:10,
  fontFamily:fontConstant.semibold,
  // backgroundColor:'pink'
},
underlineStyleHighLighted: {
  borderColor:colorConstant.theme,
  color:colorConstant.theme,
  borderWidth:2
},

text3: {
  fontSize: 14,
  fontFamily: fontConstant.semi,
  color: colorConstant.textGary,
}
    // textinput:{
    //     borderWidth:1,
    //     borderColor:'#D4D4D4',
    //     margin:10,
    //     width:55,
    //     height:41,
    //     borderRadius:10
    // }
})