import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import CustomHeader from '../../custom/CustomHeader'

const TermAndService = (props) => {
  return (
    <View style={styles.main}>
      <CustomHeader
      onPressBack={()=>props.navigation.goBack()}
      title={'Term & Service'}
      />
    </View>
  )
}

export default TermAndService
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF'
    }
})