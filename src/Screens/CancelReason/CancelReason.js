import { View, Text, StyleSheet, Pressable, Image, TouchableOpacity, TextInput } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import { Width } from '../../dimensions/dimension'
import { useState } from 'react'
import { ScrollView } from 'react-native-gesture-handler'
import CustomButton from '../../custom/CustomButton'
import { useSelector } from 'react-redux'
import toastShow from '../../utils/Toast'
import { baseUrl } from '../../utils/Urls'

const CancelReason = (props) => {
    const [isSelected, setSelected] = useState(0)
    const [status, setStatus] = useState(false)
    const [reason, setReason] = useState("")
    const [comment, setComment] = useState("")
    let { jwtToken, requestId } = useSelector((state) => state.reducers)

    console.log("cancelwala", requestId)

    const handleCancel = async () => {
        let data= {
            "requestId": requestId,
            "cancelReason": reason,
            "cancelComment": comment
        }
        console.log("data",data)
        // alert("ok")
        if (reason !== "" || comment !== "") {
            console.log("new", reason, comment)
            try {
                
                await fetch(baseUrl+"user/cancel-request", {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': jwtToken
                    },
                    body:JSON.stringify(data)
                })
                .then(async(res)=>{
                    let jsonData=await res.json()
                    return jsonData
                })
                .then((res)=>{
                    console.log("cancel api",res)
                    if(res.code==200)
                    {
                        props.navigation.navigate("ScheduleTrip")
                    }
                    else{
                        toastShow(res.message)
                    }
                })
            }
            catch (err) {
                console.log(err)
            }
        }
        else {
            toastShow("please select a reason")
        }

    }
    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            style={{ backgroundColor: '#FFFFFF' }}>
            <View style={styles.main}>
                <Pressable
                    onPress={() => props.navigation.goBack()}
                    style={styles.pres}>
                    <Image
                        resizeMode='contain'
                        source={imageConstant.Arrow}
                    />
                </Pressable>
                <Text style={styles.text1}>Select Reason for
                    Cancel</Text>
                <View style={[styles.view1, { marginTop: '10%' }]}>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={() => {
                            setStatus(false)
                            setReason("My driver and I couldn't connect")
                            setSelected(1)
                        }}
                        style={[styles.touch, { backgroundColor: isSelected == 1 ? colorConstant.theme : '#FFFFFF' }]} />
                    <Text style={styles.text2}>My driver and I couldn't connect</Text>

                </View>
                <View style={styles.view1}>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={() => {
                            setStatus(false)
                            setReason("My pickup location was incorrect")
                            setSelected(2)
                        }}
                        style={[styles.touch, { backgroundColor: isSelected == 2 ? colorConstant.theme : '#FFFFFF' }]} />
                    <Text style={styles.text2}>My pickup location was incorrect</Text>

                </View>
                <View style={styles.view1}>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={() => {
                            setStatus(false)
                            setReason("My driver cancelled")
                            setSelected(3)
                        }}
                        style={[styles.touch, { backgroundColor: isSelected == 3 ? colorConstant.theme : '#FFFFFF' }]} />
                    <Text style={styles.text2}>My driver cancelled</Text>

                </View>
                <View style={styles.view1}>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={() => {
                            setStatus(false)
                            setReason("The ETA was too long")
                            setSelected(4)
                        }}
                        style={[styles.touch, { backgroundColor: isSelected == 4 ? colorConstant.theme : '#FFFFFF' }]} />
                    <Text style={styles.text2}>The ETA was too long </Text>

                </View>
                <View style={styles.view1}>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={() => {
                            setStatus(false)
                            setReason("The ETA was too short")
                            setSelected(5)
                        }}
                        style={[styles.touch, { backgroundColor: isSelected == 5 ? colorConstant.theme : '#FFFFFF' }]} />
                    <Text style={styles.text2}>The ETA was too short </Text>

                </View>
                <View style={styles.view1}>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={() => {
                            setStatus(false)
                            setReason("My driver asked me to cancel")
                            setSelected(6)
                        }}
                        style={[styles.touch, { backgroundColor: isSelected == 6 ? colorConstant.theme : '#FFFFFF' }]} />
                    <Text style={styles.text2}>My driver asked me to cancel</Text>

                </View>
                <View style={[styles.view1, { marginBottom: status ? 0 : '76.3%' }]}>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={() => {
                            setSelected(7)
                            setStatus(true)
                        }}
                        style={[styles.touch, { backgroundColor: isSelected == 7 ? colorConstant.theme : '#FFFFFF' }]} />
                    <Text style={styles.text2}>My reason is not listed</Text>

                </View>
                {status ?
                    <TextInput
                        style={[styles.input, { marginBottom: status ? '30%' : 0 }]}
                        placeholder="Write your reason"
                        multiline={true}
                        onChangeText={(e) => {
                            setReason("")
                            setComment(e)
                        }}
                    />
                    : null
                }

                <CustomButton
                    buttonName={'Cancel'}
                    width={'80%'}
                    position={'absolute'}
                    // marginTop={30}
                    bottom={10}
                    borderRadius={10}
                    // OnButtonPress={()=>}
                    OnButtonPress={handleCancel}

                />
            </View>
        </ScrollView>
    )
}

export default CancelReason
const styles = StyleSheet.create({
    main: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    pres: {
        alignSelf: 'flex-start',
        left: '6%',
        marginTop: '7%'
    },
    text1: {
        fontSize: 29,
        width: Width * 0.7,
        color: colorConstant.theme,
        fontFamily: fontConstant.semibold,
        marginTop: '5%',
        textAlign: 'center'
    },
    touch: {
        height: 20,
        width: 20,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: colorConstant.theme
    },
    view1: {
        flexDirection: 'row',
        width: '80%',
        // justifyContent:'space-between',
        alignItems: 'center',
        marginTop: '3%'
    },
    text2: {
        fontSize: 17,
        color: '#000000',
        fontFamily: fontConstant.semibold,
        left: 20
    },
    input: {
        width: '80%',
        borderWidth: 1,
        borderColor: colorConstant.theme,
        height: 154,
        borderRadius: 19,
        marginTop: '7%',
        paddingLeft: 10,
        textAlignVertical: 'top'

    }
})