import { View, Text, Image,Pressable,StyleSheet, Keyboard } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant,imageConstant } from '../../utils/constant'
import CustomInput from '../../custom/CustomInput'
import CustomButton from '../../custom/CustomButton'
import { ScrollView } from 'react-native-gesture-handler'
import { useEffect } from 'react'
import { useState } from 'react'
import validation from '../../utils/validation'
import toastShow from '../../utils/Toast'
import { baseUrl } from '../../utils/Urls'

const ResetPassword = (props) => {
  let navtext=props?.route?.params.text
    let phone=props?.route?.params?.phone
    let callingCode=props?.route?.params?.callingCode
  const [padding,setPadding] = useState(10)
  const [userData,setUserData]=useState({
    password:"",
    confirmPassword:""
  })
    console.log("resetwala",phone)
    useEffect(()=>{
      let sub = Keyboard.addListener('keyboardWillShow',(e)=>{
        setPadding(e.endCoordinates.height+10)
      })
      let unsubs = Keyboard.addListener('keyboardWillHide',()=>{
        setPadding(10)
      })
      return ()=>{
        sub.remove();
        unsubs.remove();
      }
    },[])
    const handleData=async()=>{
     
      if(validation.passwordValid(userData.password))
      {
        if(userData.confirmPassword=="")
        {
          toastShow("please enter confirm password")
        }
       else if(userData.password===userData.confirmPassword)
        {
          console.log("phone number inside==",phone)
          data={
           phoneNumber:callingCode+phone,
           password:userData.password
           
         }
          await fetch(baseUrl+"auth/passenger-forgot-password",{
            method:"POST",
            headers:{
              'Content-Type': 'application/json',
            },
            body:JSON.stringify(data),
            
          })
          .then(async(res)=>{
            let jsonData=await res.json()
            return jsonData;
          })
          .then((res)=>{
            console.log(res)
            if(res.code==200)
            {
            toastShow(res.message)
            setTimeout(() => {
              props.navigation.navigate("Login",{text:""})
            }, 2000);
            
            }
          })
        }
        else{
          toastShow("confirm password not match")
        }
      }
    }
    console.log("navtext",navtext)
  return (
    
    <View style={styles.main}>
      <Pressable
      onPress={()=>props.navigation.goBack()}
      style={styles.pres}>
        <Image
        resizeMode='contain'
        source={imageConstant.Arrow}
        />
      </Pressable>
      <ScrollView
      bounces={false}
      showsVerticalScrollIndicator={false}
       contentContainerStyle={{backgroundColor:'#FFFFFF',alignItems:'center',paddingBottom:padding}}>
      <Text style={styles.text1}>Reset Password</Text>
      <Image
      resizeMode='contain'
      style={styles.img1}
      source={imageConstant.resetPassword}
      />
        <CustomInput
       height={42}
       imageName={imageConstant.Eye}
      imgFlag={true}
      borderColor={colorConstant.theme}
      marginTop={'7%'}
      width={'85%'}
      onChangeText={(text)=>setUserData({...userData,password:text})}
      imgmarginTop={12}
      placeholder="Password"/>
      <CustomInput
       height={42}
    //    imageName={imageConstant.Eye}
    //   imgFlag={true}
      borderColor={colorConstant.theme}
      width={'85%'}
      marginTop={'5%'}
      onChangeText={(text)=>setUserData({...userData,confirmPassword:text})}
      placeholder="Confirm Password"/>
      <CustomButton
      buttonName={"Continue"}
      width={'85%'}
      borderRadius={10}
      marginTop={'5%'}
      // OnButtonPress={()=>props.navigation.navigate("Login",{text:navtext})}
      OnButtonPress={()=>handleData()}
      marginBottom={20}
      />
       </ScrollView>
    </View>
   
  )
}

export default ResetPassword

const styles=StyleSheet.create({
    main:{
        flex:1,
        // alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
    pres:{
        alignSelf:'flex-start',
        left:'6%',
        marginTop:'7%'
    },
    text1:{
        fontSize:29,
        color:colorConstant.theme,
        fontFamily:fontConstant.semibold,
        marginTop:'5%'
    },
    img1:{
        height:316,
        width:160,
        marginTop:'7%',
        marginBottom:'5%'
    },
})