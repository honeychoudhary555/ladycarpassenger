import { View, Text, StyleSheet } from 'react-native'
import MapView, { PROVIDER_GOOGLE,Marker } from 'react-native-maps';
import React from 'react'
import { imageConstant } from '../utils/constant';

const CustomMap = (props) => {
    const mapStyle = [
        {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
        {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
        {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
        {
          featureType: 'administrative.locality',
          elementType: 'labels.text.fill',
          stylers: [{color: '#d59563'}],
        },
        {
          featureType: 'poi',
          elementType: 'labels.text.fill',
          stylers: [{color: '#d59563'}],
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [{color: '#263c3f'}],
        },
        {
          featureType: 'poi.park',
          elementType: 'labels.text.fill',
          stylers: [{color: '#6b9a76'}],
        },
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [{color: '#38414e'}],
        },
        {
          featureType: 'road',
          elementType: 'geometry.stroke',
          stylers: [{color: '#212a37'}],
        },
        {
          featureType: 'road',
          elementType: 'labels.text.fill',
          stylers: [{color: '#9ca5b3'}],
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry',
          stylers: [{color: '#746855'}],
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry.stroke',
          stylers: [{color: '#1f2835'}],
        },
        {
          featureType: 'road.highway',
          elementType: 'labels.text.fill',
          stylers: [{color: '#f3d19c'}],
        },
        {
          featureType: 'transit',
          elementType: 'geometry',
          stylers: [{color: '#2f3948'}],
        },
        {
          featureType: 'transit.station',
          elementType: 'labels.text.fill',
          stylers: [{color: '#d59563'}],
        },
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [{color: '#17263c'}],
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [{color: '#515c6d'}],
        },
        {
          featureType: 'water',
          elementType: 'labels.text.stroke',
          stylers: [{color: '#17263c'}],
        },
      ];
  return (
    <View style={{flex:1,alignItems:'center'}}>
       <MapView
          zoomEnabled={true}
          zoomTapEnabled={true}
              style={styles.mapStyle}
              initialRegion={{
                  latitude: props.lat? props.lat:28.5355,
                  longitude:props.long ? props.long : 77.3910,
                  latitudeDelta: props.letDelta ? props.letDelta : 0.0922,
                  longitudeDelta: props.longDelta ? props.longDelta : 0.0421,
              }}
              customMapStyle={mapStyle}>
              <Marker
              icon={imageConstant.location}
                  draggable
                  coordinate={{
                      latitude:props.latCord? props.latCord: 37.78825,
                      longitude:props.longCord? props.longCord : -122.4324,
                  }}
                  onDragEnd={
                      (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                  }
                  title={'Test Marker'}
                  description={'This is a description of the marker'}
              />
          </MapView>
    </View>
  )
}

export default CustomMap
const styles=StyleSheet.create({
    mapStyle: {
        flex:2,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: '50%',
      },
})