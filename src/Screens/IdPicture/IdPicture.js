import { View, Text,Image, StyleSheet, ScrollView} from 'react-native'
import React from 'react'
import CustomHeader from '../../custom/CustomHeader'
import { fontConstant, imageConstant } from '../../utils/constant'
import CustomButton from '../../custom/CustomButton'

const IdPicture = (props) => {
  return (
    
    <View style={styles.main}>
      {/* <CustomHeader
       onPressBack={()=>props.navigation.goBack()}
      /> */}
      <ScrollView 
      bounces={false}
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{backgroundColor:'#FFFFFF',alignItems:'center',marginTop:'8%'}}
      >
      <Text style={styles.welcomText}>Take a picture of
your ID <Image source={imageConstant.info} style={{height:16,width:16}}/></Text>

      <Image
      source={imageConstant.camera}
      style={{marginTop:'10%'}}
      />
      <CustomButton
      buttonName={'Passport'}
      marginTop={40}
      borderRadius={10}
      OnButtonPress={()=>props.navigation.navigate('Passport')}
      />
       <CustomButton
      buttonName={'State ID'}
      marginTop={15}
      borderRadius={10}
      OnButtonPress={()=>props.navigation.navigate('StateId')}
      />
       <CustomButton
      buttonName={'Driver’s license'}
      marginTop={15}
      borderRadius={10}
      OnButtonPress={()=>props.navigation.navigate('DriverLicense')}
      />
      <CustomButton
      buttonName={'Student ID'}
      marginTop={15}
      borderRadius={10}
      OnButtonPress={()=>props.navigation.navigate('StudentId')}
      marginBottom={40}
      />
      </ScrollView>
    </View>
   
  )
}

export default IdPicture
const styles=StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
    welcomText:{
        color:'#6C00C0',
        fontFamily:fontConstant.semibold,
        fontSize:29,
        width:'75%',
        lineHeight:37,
        textAlign:'center',
        marginTop:'3%',
       
    },
})