import { View, Text, StyleSheet, Image } from 'react-native'
import React from 'react'
import { imageConstant } from '../../utils/constant'
import { useEffect } from 'react'

const PaymentSuccess = (props) => {
    useEffect(()=>{
        setTimeout(()=>{
            props.navigation.navigate("MyWallet")
        },3000)
    },[])
  return (
    <View style={styles.main}>
      <Image
      resizeMode='contain'
      source={imageConstant.success}
      style={styles.img1}
      />
      <Text style={styles.text1}>Transaction Successfull</Text>
      <Text style={styles.text2}>Your money addend into your wallet</Text>
    </View>
  )
}

export default PaymentSuccess
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF',
        alignItems:'center',
        // justifyContent:'center'
        
    },
    img1:{
        height:111,
        width:111,
        marginTop:'55%'
    },
    text1:{
        color:'#222B45',
        fontSize:15,
        fontWeight:'700',
        marginTop:'5%'
    },
    text2:{
        color:'#222B45',
        fontSize:14,
        marginTop:'2%',
        textAlign:'center',
        // width:'90%'
    }
})

