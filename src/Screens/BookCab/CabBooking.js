import { View, Text, StyleSheet, FlatList, Image, Pressable, BackHandler, TouchableOpacity } from 'react-native'
import React from 'react'
import CustomMap from '../../custom/CustomMap'
import MapView, { PROVIDER_GOOGLE, Marker, Polyline } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Height, Width } from '../../dimensions/dimension';
import CustomButton from '../../custom/CustomButton';
import { ScrollView } from 'react-native-gesture-handler';
import { useState } from 'react';
import { baseUrl, googleKey } from '../../utils/Urls';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { action } from '../../redux/reducers';
import { useIsFocused } from '@react-navigation/native';
import MapViewDirections from 'react-native-maps-directions';


const CabBooking = (props) => {
  const [Selected, setSelected] = useState(false)
  const [cabData, setCabData] = useState([])
  const [cabType, setCabType] = useState("")
  const dispatch = useDispatch()
  let { jwtToken } = useSelector((state) => state.reducers)
  const isFocused=useIsFocused()
  let val = props?.route?.params?.val
  let distance = props?.route?.params?.distance / 1000
  let start = props?.route?.params?.start
  let data = props?.route?.params?.data
  let location = props?.route?.params?.location
  let detail = props?.route?.params?.detail

  useEffect(() => {
    let mount=true
    if(mount){
      getListData()
      setCabType("")
    }
  
    return () => {
      mount=false
    }
  }, [isFocused])
  

  useEffect(() => {
    getListData()
  }, [])
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackClick)
    }
  }, [])
  const handleBackClick = () => {
    props.navigation.navigate("ScheduleTrip")
    return true
  }
  console.log("location Wala", location, data, start, distance)
  const getListData = async () => {
    const data = {
      "distance": distance
    }
    console.log("distance wala", data)
    await fetch(baseUrl + "user/get-vechile-list",
      {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          Authorization: jwtToken
        },
        body: JSON.stringify(data)
      })
      .then(async (res) => {
        let jsonData = await res.json()
        return jsonData
      })
      .then((res) => {
        console.log("cab type",res)
        setCabData(res)
        setCabType(res.result[0]?.vehicleType)
        console.log("inside wala", res.result[0]?.vehicleType)
        
      })
  }

  console.log("Distance is", distance)
  const Data = [
    {
      id: 1,
      name: 'Standard'
    },
    {
      id: 2,
      name: 'XL'
    },
    {
      id: 3,
      name: 'Auto'
    },
  ]

  const mapStyle = [
    { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
    { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
    { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
    {
      featureType: 'administrative.locality',
      elementType: 'labels.text.fill',
      stylers: [{ color: '#d59563' }],
    },
    {
      featureType: 'poi',
      elementType: 'labels.text.fill',
      stylers: [{ color: '#d59563' }],
    },
    {
      featureType: 'poi.park',
      elementType: 'geometry',
      stylers: [{ color: '#263c3f' }],
    },
    {
      featureType: 'poi.park',
      elementType: 'labels.text.fill',
      stylers: [{ color: '#6b9a76' }],
    },
    {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [{ color: '#38414e' }],
    },
    {
      featureType: 'road',
      elementType: 'geometry.stroke',
      stylers: [{ color: '#212a37' }],
    },
    {
      featureType: 'road',
      elementType: 'labels.text.fill',
      stylers: [{ color: '#9ca5b3' }],
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry',
      stylers: [{ color: '#746855' }],
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry.stroke',
      stylers: [{ color: '#1f2835' }],
    },
    {
      featureType: 'road.highway',
      elementType: 'labels.text.fill',
      stylers: [{ color: '#f3d19c' }],
    },
    {
      featureType: 'transit',
      elementType: 'geometry',
      stylers: [{ color: '#2f3948' }],
    },
    {
      featureType: 'transit.station',
      elementType: 'labels.text.fill',
      stylers: [{ color: '#d59563' }],
    },
    {
      featureType: 'water',
      elementType: 'geometry',
      stylers: [{ color: '#17263c' }],
    },
    {
      featureType: 'water',
      elementType: 'labels.text.fill',
      stylers: [{ color: '#515c6d' }],
    },
    {
      featureType: 'water',
      elementType: 'labels.text.stroke',
      stylers: [{ color: '#17263c' }],
    },
  ];

  console.log(cabType,'his is vD TYPE')
  const toNavigate = (index,d) => {
    // console.log(d.vehicleType)
    setSelected(index);
    setCabType(d);

    // dispatch(action.setCabType(d.vehicleType))
  }
  const onConfirmButton = () => {
  
  props.navigation.navigate('Request', { val: val, location: location, vehicleType: cabType, detail: detail,distance:distance ,start:start, data,data  })
  }
  const renderDetails=({item,index})=>{
    return(
        <Pressable
        onPress={()=>toNavigate(index,item.vehicleType)}
        key={item?._id}
        style={[styles.flatView,{backgroundColor:Selected==index ? "rgba(108, 0, 192, 0.08)" :'#FFFFFF'}]}>
           <View style={{flexDirection:'row',alignItems:'center'}}>
            <Image
            resizeMode='contain'
            source={imageConstant.Car}
            style={{left:5,height:36,width:53}}
            />
            <View style={{left:'25%'}}>
            <Text style={styles.textName}>{item?.vehicleType}</Text>
            <Text style={{top:-2}}><Image source={imageConstant.userIcon}/> 3</Text>
            </View>
            </View>
            <View></View>
            <View style={{left:'60%'}}>
                <Text style={styles.textName}>${item.total}</Text>
                <Text style={[styles.timeText,{color:Selected==index ? colorConstant.theme : '#555555'}]}>5:07 PM</Text>
            </View>
        </Pressable>
    ) 
  }


  return (
    <View style={styles.main}>

      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          // backgroundColor: "#FFFFFF",
          // backgroundColor:'red'

          // paddingBottom: 30
        }}>
        <View style={{ height: Height * 0.55, backgroundColor: '#FFFFFF' }}>
          <MapView
            zoomEnabled={true}
            zoomTapEnabled={true}
            style={styles.mapStyle}
            region={{
              latitude: parseFloat(location?.latitude),
              longitude: parseFloat(location?.longitude),
              latitudeDelta: 0.4,
              longitudeDelta: 0.5
            }}
            customMapStyle={mapStyle}>
            <Marker
              // icon={imageConstant.location}
              width={30}
              height={30}
              draggable
              coordinate={{
                latitude: location?.latitude,
                longitude: location?.longitude,
              }}
              onDragEnd={
                (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
              }
              title={'Start Location'}
              description={''}
            >
                <Image resizeMode='contain' source={imageConstant.startPoint} style={{height:32,width:32}}/>

              </Marker>
              <MapViewDirections
                               origin={{latitude:location?.latitude,longitude:location?.longitude}}
                               destination={{latitude:detail?.lat,longitude:detail?.lng}}
                               apikey={googleKey}
                               strokeColor={'red'}
                               strokeWidth={3}
                             />
                             <Marker
                            //  icon={imageConstant.endPoint}
                             // image={imageConstant.location}
                             width={50}
                             height={50}
                                 draggable
                                 coordinate={{
                                     latitude: detail?.lat,
                                     longitude: detail?.lng,
                                 }}
                                 onDragEnd={
                                     (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                                 }
                                 title={'End Location'}
                                 description={''}
                             >
                              <Image resizeMode='contain' source={imageConstant.endPoint} style={{height:32,width:32}}/>
                              </Marker>
          </MapView>
          <View style={styles.view4}>
            <Image
              resizeMode='contain'
              style={styles.img1}
              source={imageConstant.groupLine}
            />
            <View style={{ width: '85%' }}>
              <View style={styles.veiw1}>
                <Text style={styles.text1}>{start}</Text>

              </View>
              <View style={styles.veiw1}>
                <Text style={styles.text1}>{data}</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.view2}>

          <FlatList
              data={cabData?.result}
              renderItem={renderDetails}
              // contentContainerStyle={{borderBottomWidth:0.5}}
              />


          {/* <View style={{paddingBottom:30}}>
               </View> */}
          <View style={styles.view3} >
            <Pressable
              onPress={() => props.navigation.navigate("Payment")}
              style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={imageConstant.PayPal}
                resizeMode='contain'
                style={{ height: 17, width: 17 }}
              />
              <Text style={{ fontSize: 12, color: '#555555' }}>PayPal</Text>
              <Image source={imageConstant.ExpandArrow}
                resizeMode='contain'
                style={{ height: 17, width: 17 }}
              />
            </Pressable>
            <Pressable
              onPress={() => props.navigation.navigate("ScheduleRide")}
              style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ color: '#555555', fontSize: 12 }}>Schedule a trip</Text>
              <Image source={imageConstant.calen}
                resizeMode='contain'
                style={{ height: 35, width: 35 }}
              />
            </Pressable>

          </View>

          <View>
            {/* {
            cabData?.result?.length>0&&  cabData?.result?.map((d, i) => {
                return (
                  <View>
                    <TouchableOpacity onPress={() => toNavigate(d)}>
                      <Text>{d.vehicleType}</Text>
                    </TouchableOpacity>
                  </View>
                )
              })
            } */}
          </View>

          <CustomButton
            buttonName={'CONFIRM'}
            // top={'5%'}
            height={38}
            paddingBottom={50}
            // position={'absolute'}
            // bottom={'5%'}
            alignSelf={'center'}
            borderRadius={10}
            OnButtonPress={onConfirmButton}
          />
        </View>
      </ScrollView>

    </View>
  )
}

export default CabBooking
const styles = StyleSheet.create({
  main: {
    flex: 1,
    // alignItems:'center'
  },
  mapStyle: {
    flex: 2,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  veiw1: {
    width: '85%',
    borderRadius: 10,
    marginTop: '5%',
    backgroundColor: '#EBE8FC',
    height: 35,
    alignItems: 'center',
    // justifyContent:'space-around',
    flexDirection: 'row',
    paddingHorizontal: 10
  },
  flatView: {
    height: 83,
    width: Width,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',



  },
  text1: {
    color: '#800080',
    fontSize: 13,
    // left:20,

  },
  textName: {
    color: '#000000',
    fontSize: 17,
    fontWeight: '700',
    lineHeight: 22,
    width: 150
  },
  timeText: {
    fontSize: 16,
    color: '#555555'
  },
  view2: {
    backgroundColor: '#FFFFFF',
    // top:'55%',

    // position:'relative',
    paddingBottom: 20

  },
  view3: {
    height: 50,
    alignItems: 'center',
    width: Width * 0.95,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 2,
    borderTopColor: '#0000001A',
    alignSelf: 'center',
    // backgroundColor:"red"
    // marginTop:'5%'
    // bottom:0,
    // position:'absolute',
    // bottom:'10%'
  },
  view4: {
    width: Width,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10
  },
  img1: {
    height: 70,
    width: 15,
    left: '22%',
    top: '3%'
  }
})