import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity, Pressable,ScrollView } from 'react-native'
import React from 'react'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Height, Width } from '../../dimensions/dimension';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CheckBox from '@react-native-community/checkbox';
import { useState } from 'react';
import CustomButton from '../../custom/CustomButton';
// import CountDown from 'react-native-countdown-component';
import CountDownTimer from 'react-native-countdown-timer-hooks';
import { useRef } from 'react';
import { useEffect } from 'react';
import { useIsFocused } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import MapViewDirections from 'react-native-maps-directions';
import { googleKey } from '../../utils/Urls';

const DriverApproval = (props) => {
    const [check, setCheck] = useState(true)
    const [SelectedId, setSelectedId] = useState(null)
    const [status, setStatus] = useState(false)
    const [value, setValue] = useState(true)
    const [count, setCount] = useState(20)
    const refTimer = useRef();
  
    let { jwtToken,startLocation,endLocation} = useSelector((state) =>state.reducers)
const isFocused=useIsFocused()
    useEffect(() => {
        if(isFocused){
          const intervalId = setInterval(() => {
            if(count>0){
                setCount(count => count - 1);
            }
          }, 1000);
        
          return () => clearInterval(intervalId);
        }
        else{
        //   setCount(0)
        //   return null
        }
      }, [isFocused]);
    
      useEffect(() => {
        if(isFocused){
          if (count === 0) {
            setCount(20);
            
            props.navigation.navigate('RequestSuccess')
          }
          return () => {
            console.log('Cleaning up effect');
          };
        }
        // else{
        //     return null
        // }
    
      }, [count,isFocused]);
      console.log("count",count)
    const mapStyle = [
        { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
        {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{ color: '#263c3f' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#6b9a76' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{ color: '#38414e' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#212a37' }],
        },
        {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#9ca5b3' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{ color: '#746855' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#1f2835' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#f3d19c' }],
        },
        {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{ color: '#2f3948' }],
        },
        {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{ color: '#17263c' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#515c6d' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{ color: '#17263c' }],
        },
    ];
    data = [
        {
            id: 1,
            title: 'one',
            img: imageConstant.profile2,
            color: 'gray'
        },
        {
            id: 2,
            title: 'two',
            img: imageConstant.profile2,
            color: 'black'
        }
    ]
console.log("location wala",startLocation,endLocation)
    const renderDetails = ({ item, index }) => {
        console.log(item)
        return (
            <View>
                <TouchableOpacity
                    onPress={() => handleClick(index)}
                    style={[styles.touch, { backgroundColor: SelectedId == index ? '#D9D9D9' : '#FFFFFF' }]}>
                    <Image
                        source={item.img} />
                    <View>
                        <Text style={{ color: '#000000', fontSize: 15 }}>{item.title}</Text>
                        <Text style={{ fontSize: 11, color: '#000000' }}>Celio</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={imageConstant.star} />
                        <Text style={{ fontSize: 20, color: '#000000' }}>4.3</Text>
                    </View>
                    <View>
                        <Text style={{ color: '#000000', fontSize: 15 }}>{item.color}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={imageConstant.Timer}
                        />
                        <Text style={{ color: '#000000', fontSize: 15 }}>9 min</Text>
                    </View>
                </TouchableOpacity>

            </View>
        )
    }

    const handleClick = (id) => {
        setSelectedId(id)
        setStatus(true)
    }

    return (
        <View style={styles.main}>
             <ScrollView
                contentContainerStyle={{
                    backgroundColor: "#FFFFFF",

                    paddingBottom: 90
                }}>
            <View style={{height:Height * 0.52}}>
            <MapView
                zoomEnabled={true}
                zoomTapEnabled={true}
                style={styles.mapStyle}
                initialRegion={{
                    latitude: startLocation?.latitude,
                    longitude: startLocation?.longitude,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
                customMapStyle={mapStyle}>
                <Marker
              // icon={imageConstant.location}
              width={30}
              height={30}
              draggable
              coordinate={{
                latitude: startLocation?.latitude,
                longitude: startLocation?.longitude,
              }}
              onDragEnd={
                (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
              }
              title={'Start Location'}
              description={''}
            >
                <Image resizeMode='contain' source={imageConstant.startPoint} style={{height:32,width:32}}/>

              </Marker>
              <MapViewDirections
                               origin={{latitude:startLocation?.latitude,longitude:startLocation?.longitude}}
                               destination={{latitude:endLocation?.lat,longitude:endLocation?.lng}}
                               apikey={googleKey}
                               strokeColor={'red'}
                               strokeWidth={3}
                             />
                             <Marker
                             icon={imageConstant.endPoint}
                             // image={imageConstant.location}
                             width={50}
                             height={50}
                                 draggable
                                 coordinate={{
                                     latitude: endLocation?.lat,
                                     longitude: endLocation?.lng,
                                 }}
                                 onDragEnd={
                                     (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                                 }
                                 title={'End Location'}
                                 description={''}
                             >
                              <Image resizeMode='contain' source={imageConstant.endPoint} style={{height:32,width:32}}/>
                              </Marker>
            </MapView>
            <View style={styles.view}>
                <Pressable onPress={()=>props.navigation.toggleDrawer()}>
                <Image
                resizeMode="contain"
                style={{height:42,width:42}}
                    source={imageConstant.menuIcon}
                />
                </Pressable>
                <Pressable onPress={()=>props.navigation.navigate("Profile")}>
                    <Image
                    resizeMode="contain"
                        style={styles.img1}
                        source={imageConstant.profile}
                    />
                </Pressable>
            </View>
            <Image
                style={styles.img2}
                source={imageConstant.Zooms}
            />
            </View>
            <View style={styles.view1}>
                <Image
                    resizeMode='contain'
                    style={styles.img3}
                    source={imageConstant.Rect}
                />
                <Text style={{ fontSize: 9,marginTop:'1%' }}>Travel safe</Text>


                <View style={styles.view2}>
                    <Text style={styles.text2}>Wait for your driver’s approval</Text>
         
                </View>


                <TouchableOpacity

                    style={[styles.touch, { backgroundColor: '#D9D9D9' }]}>
                    <Image
                        source={imageConstant.profile2} />
                    <View>
                        <Text style={{ color: '#000000', fontSize: 15 }}>One</Text>
                        <Text style={{ fontSize: 11, color: '#000000' }}>Celio</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={imageConstant.star} />
                        <Text style={{ fontSize: 20, color: '#000000' }}>4.3</Text>
                    </View>
                    <View>
                        <Text style={{ color: '#000000', fontSize: 15 }}>black</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={imageConstant.Timer}
                        />
                        <Text style={{ color: '#000000', fontSize: 15 }}>9 min</Text>
                    </View>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', marginTop: '10%' }}>
                    <Image
                        source={imageConstant.ComingSoon}
                    />
                    {/* <CountDown
                        until={count}
                        onFinish={()=>props.navigation.navigate('RequestSuccess')}
                        size={20}
                        timeLabels={{ s: null }}
                        timeToShow={['S']}

                    /> */}
                    {/* <CountDownTimer
          ref={refTimer}
          timestamp={count}
          timerCallback={()=>props.navigation.navigate('RequestSuccess')}
          
          containerStyle={{
            height: 56,
            width: 120,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 35,
            // backgroundColor: '#2196f3',
          }}
          textStyle={{
            fontSize: 40,
            color: colorConstant.theme,
            fontWeight: '500',
            letterSpacing: 0.25,
            bottom:5
          }}
        /> */}
        <Text style={{ fontSize: 40,
            color: colorConstant.theme,
            fontWeight: '500',
            letterSpacing: 0.25,
            bottom:5}}>{count}</Text>

                </View>
                {/* <TouchableOpacity onPress={()=>props.navigation.navigate('CancelReason')}>
                <Text style={styles.cancelText}>Cancel?</Text>
                </TouchableOpacity> */}
            </View>

            </ScrollView>
        </View>
    )
}

export default DriverApproval
const styles = StyleSheet.create({
    main: {
        flex: 1,
        // alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    mapStyle: {
        flex: 2,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom:0,
    },
    view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Width
    },
    view1: {
        backgroundColor:'#FFFFFF',
        height: '40%',
        // position: 'absolute',
        // top: '52%',
        width: Width,
        alignItems: 'center',


    },
    text: {
        color: '#000000',
        fontFamily: fontConstant.bold,
        fontSize: 17,
        marginTop: 10,
    },
    view2: {
        width: '95%',
        backgroundColor: '#FFFFFF',
        height: '20%',
        // borderTopWidth:1,
        marginTop: '0%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        // marginTop:'5%',
        alignItems: 'center'
    },
    text2: {
        color: colorConstant.theme,
        fontFamily: fontConstant.bold,
        fontSize: 19,
        width: '85%'
    },
    view3: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // alignItems:'center',
        marginTop: '8%'
    },
    cancelText: {
        alignSelf: 'center',
        color: colorConstant.theme,
        marginTop: '10%',
        marginBottom:50
    },
    img1: {
        borderWidth: 2,
        borderColor: '#FFFFFF',
        borderRadius: 100,
        right: 5,
        height:33,
        width:33
    },
    img2: {
        alignSelf: 'flex-end',
        right: 5,
        top: 15
    },
    img3: {
        marginTop: '5%',
        width: 77,
        height: 6
    },
    touch: {
        flexDirection: 'row',
        height: 80,
        width: Width * 0.98,
        borderRadius: 10,
        justifyContent: 'space-around',
        alignItems: 'center',
        marginTop:'2%'
    }
})