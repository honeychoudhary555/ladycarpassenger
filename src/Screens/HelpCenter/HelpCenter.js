import { View, Text, StyleSheet ,Image, TextInput, ScrollView, Keyboard} from 'react-native'
import React from 'react'
import CustomHeader from '../../custom/CustomHeader'
import { Width } from '../../dimensions/dimension'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import { Dropdown } from 'react-native-element-dropdown'
import { useState } from 'react'
import CustomButton from '../../custom/CustomButton'
import { useEffect } from 'react'

const HelpCenter = (props) => {
    const [value, setValue] = useState(null);
    const [padding,setPadding]=useState(10)
    useEffect(()=>{
        let sub=Keyboard.addListener('keyboardWillShow',(e)=>{
            setPadding(e.endCoordinates.height+10)
        })
        let subs=Keyboard.addListener('keyboardWillHide',()=>{
            setPadding(10)
        })
    },[])
    const data = [
        { label: 'Item 1', value: '1' },
        { label: 'Item 2', value: '2' },
        { label: 'Item 3', value: '3' },
        { label: 'Item 4', value: '4' },
        { label: 'Item 5', value: '5' },
        { label: 'Item 6', value: '6' },
        { label: 'Item 7', value: '7' },
        { label: 'Item 8', value: '8' },
      ];
  return (
    
    <View style={styles.main}>
        <CustomHeader
        title={"Choose an Issue"}
        onPressBack={()=>props.navigation.goBack()}
        marginBottom={10}
        />
        <ScrollView contentContainerStyle={{backgroundColor:'#FFFFFF',alignItems:'center',paddingBottom:padding}}>
      <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginTop:'10%',width:Width*0.9}}>
                    <Image
                    resizeMode='contain'
                    style={{width:40,height:40}}
                    source={imageConstant.profile2}
                    />
                    <View style={{left:30}}>
                        <View style={styles.view1}>
                            <Text style={styles.text6}>SONIA</Text>
                            
                        </View>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Image
                            source={imageConstant.carRoof}
                            />
                            <Text style={{left:5,fontSize:11,color:'#000000'}}>Chevrolet Bolt EV</Text>
                        </View>
                    </View>
                    <View style={{position:'absolute',right:5}}>
                        <Text style={styles.text1}>Fare:  <Text style={{fontSize:14,color:colorConstant.theme,fontFamily:fontConstant.semibold}}>$13</Text></Text>
                    </View>
                </View>
                <View style={styles.view4}>
              <View>
                <Image
                source={imageConstant.loc}
                />
                <Image
                style={{left:7}}
                source={imageConstant.verticalLine}
                />
                <Image
                source={imageConstant.loc}
                />
              </View>
              <View style={{left:10}}>
              <View>
                <Text style={styles.text7}>Nashville, TN 37214, USA</Text>
                <Text style={styles.text8}>Sep 19  12:25 PM</Text>
              </View>
              <View style={{marginTop:20}}>
                <Text style={styles.text7}>2720 Old Lebanon Rd, Nashiville, TN, 37214, US</Text>
                <Text style={styles.text8}>Sep 19  12:25 PM</Text>
              </View>
              </View>
              </View>
              <Dropdown
              data={data}
              valueField="value"
              value={value}
              placeholder='Reason'
              maxHeight={300}
              labelField="label"
              style={styles.dropdown}
              onChange={item => {
                setValue(item.value);
              }}
              />
              <TextInput
              placeholder='Description'
              style={styles.input}
              multiline={true}
              />
              <CustomButton
              buttonName={"SUBMIT"}
              width={284}
              borderRadius={10}
              marginTop={'10%'}
              height={38}
              marginBottom={30}
              OnButtonPress={()=>props.navigation.navigate("TripDetails")}
              />
              </ScrollView>
    </View>
    // </ScrollView>
  )
}

export default HelpCenter
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF',
        // alignItems:'center'
    },
    view1:{
        flexDirection:'row',
        width:120,
        justifyContent:'space-between',
        alignItems:'center',
        // width:Width
    },
    text6:{
        color:'#000000',
        fontSize:15,
        fontWeight:'600'
    },
    status:{
        color:'#83D28A',
        fontSize:10
    },
    text1:{
        color:'#000000',
        fontSize:12,
        alignSelf:'flex-end'
    },
    view4:{
        flexDirection:'row',
        alignSelf:'flex-start',
        width:Width * 0.90,
        // justifyContent:'space-between',
        alignItems:'center',
        marginTop:'10%',
        left:'3%'
        
    },
    text7:{
        color:'#000000',
        fontSize:14,
        fontFamily:fontConstant.semibold,
        // width:'100%'
    },
    text8:{
        color:'#000000',
        fontSize:12
    },
    dropdown:{
        width:284,
        height:38,
        borderColor:colorConstant.theme,
        borderWidth:1,
        borderRadius:9,
        marginTop:'10%',
        paddingHorizontal:10
    },
    input:{
        height:221,
        width:284,
        alignItems:'center',
        borderWidth:1,
        borderColor:colorConstant.theme,
        marginTop:'6%',
        borderRadius:15,
        // textAlign:'center',
        textAlignVertical:'top',
        paddingHorizontal:15,
        paddingVertical:15,
    }
})