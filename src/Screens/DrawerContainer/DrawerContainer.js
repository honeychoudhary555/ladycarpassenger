import { View, Text, SafeAreaView, StyleSheet, Image, ImageBackground, TouchableOpacity, Pressable, Modal } from 'react-native'
import React from 'react'
import { ScrollView } from 'react-native-gesture-handler'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import { Height, Width } from '../../dimensions/dimension'
import { useDispatch, useSelector } from 'react-redux'
import { action } from '../../redux/reducers'
import { useEffect } from 'react'
import { useState } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin'
import { useFocusEffect, useIsFocused } from '@react-navigation/native'
import LottieView from 'lottie-react-native';
import { baseUrl } from '../../utils/Urls'
const DrawerContainer = (props) => {
  const [visible,setVisible]=useState(false)
  let [profileData,setProfileData]=useState(null)
  let {jwtToken}=useSelector((state)=>state.reducers)
const isFocused=useIsFocused()
  const dispatch=useDispatch()
useFocusEffect(
  React.useCallback(()=>{
    getProfile()
  },[isFocused])
)
  signOut = async () => {
    
    try {
      dispatch(action.setIntroStatus("login"))
      const keys = await AsyncStorage.getAllKeys();
      await AsyncStorage.multiRemove(keys);
      await AsyncStorage.clear();
     
    //   setloggedIn(false);
    //   setuserInfo([]);
   
    } catch (error) {
      console.error(error);
    }
  };
  const getProfile=async()=>{
    // alert("olk")
    let response=await fetch(baseUrl+"user/passenger-view-profile",{
      method:'GET',
      headers:{
        'Content-Type':'application/json',
        'Authorization':jwtToken
      }
    })
    .then(async(res)=>{
      let jsonData=await res.json()
      return jsonData;
    })
    .then((res)=>{
      // console.log("dwawer wala",res)
      setProfileData(res)
    })
    // let data=JSON.parse(await AsyncStorage.getItem("Profile"))
    
  }
  // console.log("profile by async",profileData)
  return (
   <SafeAreaView style={styles.main}>
      <ScrollView
        contentContainerStyle={{ alignItems: 'center' }}
        bounces={false}
        showsVerticalScrollIndicator={false}>
        <ImageBackground
          source={imageConstant.drawerback}
          style={styles.view1}>

            
         

            <Pressable  style={{
              alignSelf:"flex-end",
              top:15,
              right:25
            }}onPress={() => { props.navigation.navigate("Profile") }}>
              <Text style={styles.text1}>My Profile</Text>
            </Pressable>
    



          <Pressable
          style={{
            marginTop: '13%',
            alignSelf:"center"
          }}
            onPress={() => props.navigation.navigate("Profile")}
          >
            <Image
              style={{ borderWidth: 4, borderColor: '#FFFFFF', borderRadius: 50,height: 77, width: 77 }}
              source={profileData?.userData?.profilePic ? {uri:profileData?.userData?.profilePic}: imageConstant.profileDemo}
            />
          </Pressable>


          <Text style={styles.text2}>{profileData?.userData?.name} {profileData?.userData?.lastName}</Text>
          <View style={styles.view2}>
            <Image
              source={imageConstant.star}
            />
            <Text style={styles.text3}>4.7</Text>
          </View>
        </ImageBackground>


        <TouchableOpacity
          onPress={() => props.navigation.navigate("MyAccount")}
          style={styles.touch}>
          <Text style={styles.text4}>Account</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("MyWallet")}
          style={styles.touch}>
          <Text style={styles.text4}>My Wallet</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("ScheduleRide")}
          style={styles.touch}>
          <Text style={styles.text4}>Book a trip in advance</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("History")}
          style={styles.touch}>
          <Text style={styles.text4}>History</Text>
        </TouchableOpacity>
        {/* <TouchableOpacity
              onPress={()=>props.navigation.navigate("DiscountCode")}
              style={styles.touch}>
                <Text style={styles.text4}>Discount Codes</Text>
              </TouchableOpacity> */}
        {/* <TouchableOpacity style={styles.touch}>
                <Text style={styles.text4}>Events</Text>
              </TouchableOpacity> */}
        <TouchableOpacity
          onPress={() => props.navigation.navigate("Helpcenter2")}
          style={[styles.touch, { borderBottomWidth: 0}]}>
          <Text style={styles.text4}>Help Center</Text>
        </TouchableOpacity>
     
      </ScrollView>
      <TouchableOpacity style={styles.touch2} 
      onPress={()=>signOut()}
      >
          <Text style={styles.text5}>Log out</Text>
          <Image
            style={{ left: 10, top: 2 }}
            source={imageConstant.logout}
          />
        </TouchableOpacity>
        <Modal
        visible={visible}
        transparent={true}
        onRequestClose={()=>setVisible(!visible)}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            backgroundColor:'rgba(0,0,0,0.6)'
          }}
        >
          <View>
            <LottieView source={require('../../asset/images/loader.json')} style={{ height:100 }} autoPlay />
          </View>
        </View>
      </Modal>
   </SafeAreaView>
  )
}

export default DrawerContainer
const styles=StyleSheet.create({
    main:{
        // flex:1,
        // alignItems:'center',
       height:Height*0.8,
        backgroundColor:'#FFFFFF',
        borderBottomRightRadius:70
        // height:700
    },
    view1:{
      width:Width*0.55,
        height:220,
        // alignItems:'center',
        
    },
 
    text1:{
        color:'#FFFFFF',
        fontSize:16,
        // left:65
        alignSelf:'center'
    },
    text2:{
        fontSize:18,
        color:colorConstant.white,
        marginTop:10,
        alignSelf:'center',
    },
    view2:{
        flexDirection:'row',
        marginTop:'2%',
        alignSelf:'center',
        // right:'15%'
    },
    text3:{
        color:'#FFFFFF',
        fontFamily:fontConstant.bold,
        fontSize:14,
        left:3
    },
    touch:{
        height:50,
        justifyContent:'center',
        backgroundColor:'#FFFFFF',
        width:'100%',
        paddingLeft:'15%',
        borderBottomWidth:1,
        borderBottomColor:'gray'
    },
    text4:{
        color:'#000000',
        fontSize:14,
        fontWeight:'400'
    },
    text5:{
      color:'#070707',
      fontFamily:fontConstant.bold
    },
    touch2:{
      height:30,
     width:Width*0.38,
      backgroundColor:'#FFFFFF',
    //  justifyContent:'center',
     alignItems:'center',
      borderBottomRightRadius:80,
      flexDirection:'row',
      alignSelf:'flex-start',
      position:'relative',
      bottom:'20%',
      alignSelf:'center',
      left:'2%'
    }
})