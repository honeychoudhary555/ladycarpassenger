import { View, Text, StyleSheet, FlatList, TouchableOpacity, Pressable, Platform } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import CustomHeader from '../../custom/CustomHeader'
import { Width } from '../../dimensions/dimension'
import CommentNew from '../CommentNew/CommentNew'

const Comments = (props) => {
    const data=[
        {   id:1,
            msg:"You have received 2 comments.",
            time:"22 minutes ago"
        },
        {
            id:2,
            msg:"You have received comments from Maria about your last trip.",
            time:"36 minutes ago"
        }
    ]
    const renderDetails=({item,index})=>{
        return(
            <View style={styles.flatView}>
                <Text style={styles.text2}>{item.msg}</Text>
                <Text style={styles.text3}>Reply</Text>
                <Text style={styles.text4}>{item.time}</Text>
            </View>
        )
    }
    const renderView=()=>(
        <View
        style={{
          backgroundColor: '#979797',
          width:Width*0.8,
          height: 0.5,
        }}
      />
    )
    const footerView=()=>{
        return(
            <View style={[styles.view1,{marginTop:5}]}>
        <Text style={[styles.text2,{marginTop:10}]}>Did you have a great time with Jessy ?<Text style={{color:colorConstant.theme,textDecorationLine:'underline'}}> Give her a comment !</Text></Text>
        <Text style={styles.text5}>No more notifications for the moment...</Text>
        <TouchableOpacity 
        onPress={()=>props.navigation.navigate("CommentsAboutYou")}
        style={styles.touch}>
            <Text style={styles.touchText}>All comments from your passengers</Text>
        </TouchableOpacity>
        <Pressable
        onPress={()=>props.navigation.navigate("MyComments")}
        >
        <Text style={styles.text6}>My Comments</Text>
        </Pressable>
      </View>
        )
    }
  return (
    <View style={styles.main}>
      <CustomHeader
      Viewtop={'3%'}
      title={"Comments"}
      color={colorConstant.theme}
      onPressBack={()=>props.navigation.goBack()}
      />
 
      <CommentNew/>
    </View>
  )
}

export default Comments
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white,
    },
   
})