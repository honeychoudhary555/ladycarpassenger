import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import CustomHeader from '../../custom/CustomHeader'

const PrivacyPolicy = (props) => {
  return (
    <View style={styles.main}>
      <CustomHeader
      onPressBack={()=>props.navigation.goBack()}
      title={'PrivacyPolicy'}
      />
    </View>
  )
}

export default PrivacyPolicy
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF'
    }
})