import { Platform } from 'react-native';
import Toast from 'react-native-root-toast';
import { colorConstant } from './constant';
import { heightPercentageToDP } from './responsiveFIle';
// import { heightPercentageToDP } from './responsiveFIle';

const toastShow=(message,type,time)=>{
    Toast.show(message, {
        duration: time ?? 2000,
        position:Platform.OS =='ios'?  heightPercentageToDP("6%")  : heightPercentageToDP('5%'),
        shadow: true,
        animation: true,
        hideOnPress: true,
        textColor:colorConstant.white,
        delay: 0,
        backgroundColor:'red',
        onShow: () => {
            // calls on toast\`s appear animation start
        },
        onShown: () => {
            // calls on toast\`s appear animation end.
        },
        onHide: () => {
            // calls on toast\`s hide animation start.
        },
        onHidden: () => {
            // calls on toast\`s hide animation end.
        }
    });
}

export default toastShow