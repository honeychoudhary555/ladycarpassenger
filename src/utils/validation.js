import { colorConstant } from "./constant";
import toast from "./Toast";
export default validation={
    nameValid:(data)=>{     
    var nameRegex = /^(?:[a-zA-Z\-]{3,})+\s(?:[a-zA-Z\-]{3,})+$/;
    if(data=="")
    {
        toast("Please Provide  first name")
        return false
    }
    // else if(!nameRegex.test(String(data))){
    //     toast("Enter full name")
    //     return false
    // }
    else{
        return true
    }
    },
    LastnameValid:(data)=>{     
        var nameRegex = /^(?:[a-zA-Z\-]{3,})+\s(?:[a-zA-Z\-]{3,})+$/;
        if(data=="")
        {
            toast("Please Provide last name")
            return false
        }
        // else if(!nameRegex.test(String(data))){
        //     toast("Enter full name")
        //     return false
        // }
        else{
            return true
        }
        },
    dobValid:(data)=>{
        if(data=="")
        {
            toast("Please Enter your Date of birth ")
            return false
        }
        else {
            return true
        }
    }
    
    ,
    phoneValid:(data)=>{
        var phoneRe = /^[0-9]{8,13}$/;
        if(data=="")
        {
            toast("Please Provide your phone number")
            return false
        }
        else if(!phoneRe.test(data)){
            toast("Enter valid Phone Number")
            return false
        }
        else{
            return true
        }
    },
    addressValid:(data)=>{
        let addressRegex=/^[a-zA-Z0-9\s,'-]*$/
        if(data=="")
        {
            toast("Please Enter your Address")
            return false
        }
        else if(!addressRegex.test(String(data))){
            toast("Enter valid Address")
            return false
        }
        else{
            return true
        }
    },
    emailValid:(data)=>{
        var emailRegex=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if(data=="")
        {
            toast("Please Enter  Email")
            return false
        }
        else if(!emailRegex.test(data)){
            toast("Enter valid Email")
            return false
        }
        else{
            return true
        }
    },
    passwordValid:(data)=>{
        let passwordRegex=/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
        if(data=="")
        {
            toast("Please Enter Password")
            return false
        }
        else if(!passwordRegex.test(data)){
            toast("Required minimum 8 length,1 Small,Capital & Special Character")
            return false
        }
        else{
            return true
        }
    },
    newPasswordValid:(data)=>{
        let passwordRegex=/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
        if(data=="")
        {
            toast("Please Enter New Password")
            return false
        }
        else if(!passwordRegex.test(data)){
            toast("Required minimum 8 length,1 Small,Capital & Special Character")
            return false
        }
        else{
            return true
        }
    },
    cityValid: (data) => {
        if (data == "") {
            toast("Please Select City")
            return false
        }
        else
        {
            return true
        }
    },
 stateValid:(data)=>{
    if(data=="")
    {
        toast("Please Select State")
        return false
    }
    else{
        return true
    }
 }
}