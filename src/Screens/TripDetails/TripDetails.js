import { View, Text,StyleSheet, Image, TouchableOpacity, Platform } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import MapView, { Marker } from 'react-native-maps';
import { Width } from '../../dimensions/dimension';
import CustomHeader from '../../custom/CustomHeader';
import { AirbnbRating, Rating } from 'react-native-ratings';
import { useState } from 'react';

const TripDetails = (props) => {
    const value=props?.route?.params?.status
    let status=props?.route?.params?.status
    console.log("status is",status)
    const mapStyle = [
        { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
        {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{ color: '#263c3f' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#6b9a76' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{ color: '#38414e' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#212a37' }],
        },
        {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#9ca5b3' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{ color: '#746855' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#1f2835' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#f3d19c' }],
        },
        {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{ color: '#2f3948' }],
        },
        {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{ color: '#17263c' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#515c6d' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{ color: '#17263c' }],
        },
    ];
  return (
    <View style={styles.main}>
      {/* <Text style={styles.text}>Trip Details</Text> */}
      <CustomHeader
      title={"Trip Details"}
      onPressBack={()=>props.navigation.goBack()}
      />
      <MapView
                zoomEnabled={true}
                zoomTapEnabled={true}
                style={styles.mapStyle}
                initialRegion={{
                    latitude: 28.5355,
                    longitude: 77.3910,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
                customMapStyle={mapStyle}>
                <Marker
                    icon={imageConstant.location}
                    draggable
                    coordinate={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                    }}
                    onDragEnd={
                        (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                    }
                    title={'Test Marker'}
                    description={'This is a description of the marker'}
                />
            </MapView>
            <View style={styles.view2}>
                <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginTop:'5%',width:Width*0.9}}>
                    <Image
                    resizeMode='contain'
                    style={{width:40,height:40}}
                    source={imageConstant.profile2}
                    />
                    <View style={{left:30}}>
                        <View style={styles.view1}>
                            <Text style={styles.text6}>SONIA</Text>
                            <View style={{flexDirection:'row',width:65,alignItems:'center',justifyContent:'space-between'}}>
                            <Image
                            resizeMode='contain'
                            source={value=="Upcoming"? imageConstant.Ellipse : value=="Completed" ?imageConstant.tick :imageConstant.cancelSmall}
                            />
                            <Text style={styles.status}>{value=="Upcoming" ?"Upcoming" :value=="Completed" ? "Completed" :"Canceled"}</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Image
                            source={imageConstant.carRoof}
                            />
                            <Text style={{left:5,fontSize:11,color:'#000000'}}>Chevrolet Bolt EV</Text>
                        </View>
                    </View>
                    <View style={{position:'absolute',right:5}}>
                        <Text style={styles.text1}>Fare:  <Text style={{fontSize:14,color:colorConstant.theme,fontFamily:fontConstant.semibold}}>$13</Text></Text>
                       {value=="Completed" ?<Text style={styles.text1}>Tip:  <Text style={{fontSize:14,color:colorConstant.theme,fontFamily:fontConstant.semibold}}>$13</Text></Text>
                       :null}
                    </View>
                </View>
                {value=="Upcoming"
                ?
                <View style={styles.view7}>
                    <View style={styles.insideView1}>
                        <Text style={styles.text9}>Pickup Time:</Text>
                        <Text style={styles.text10}>Sep 19,  12:25 PM</Text>
                        </View>
                        <View style={styles.insideView2}>
                            <Image
                            resizeMode='contain'
                            style={{height:31,width:31}}
                            source={imageConstant.Phone}
                            />
                             <Image
                            resizeMode='contain'
                            style={{height:31,width:31}}
                            source={imageConstant.Speech}
                            />
                            </View>
                    </View>
                    :
                    <>
               
           {value=="Completed" ?     <View style={styles.view6}>
                <Text style={styles.ratingText}>Rate Your Driver</Text>
                <AirbnbRating
                    size={15}
                    reviews=''
                    selectedColor='#FFD600'
                    // style={{}}
                    ratingCount={5}
                    starContainerStyle={{marginTop:Platform.OS==='ios'?-27 :-40,alignSelf: 'flex-start'}}
                    />
               </View>
               :null}
                    </>
                }
               <View style={styles.view4}>
              <View>
                <Image
                source={imageConstant.loc}
                />
                <Image
                style={{left:7}}
                source={imageConstant.verticalLine}
                />
                <Image
                source={imageConstant.loc}
                />
              </View>
              <View style={{left:10}}>
              <View>
                <Text style={styles.text7}>Nashville, TN 37214, USA</Text>
                <Text style={styles.text8}>Sep 19  12:25 PM</Text>
              </View>
              <View style={{marginTop:20}}>
                <Text style={styles.text7}>2720 Old Lebanon Rd, Nashiville, TN, 37214, US</Text>
                <Text style={styles.text8}>Sep 19  12:25 PM</Text>
              </View>
              </View>
              </View>
              {value=="Completed" || value=="Upcoming" ?
               <View style={{flexDirection:'row',alignItems:'center',marginTop:'8%',alignSelf:'center'}}>
                <Text style={styles.text5}>After your trip, your driver can’t see your pickup or dropoff address details</Text>
                <Image
                resizeMode='contain'
                style={{height:24,width:24}}
                source={imageConstant.info}
                />
            </View>
            :null}
            <TouchableOpacity
            onPress={()=>props.navigation.navigate("HelpCenter")}
            style={styles.touch}>
                <Text style={styles.helpText}>Help & Support</Text>
            </TouchableOpacity>
            </View>
    </View>
  )
}

export default TripDetails
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF'
    },
    text:{
        color:colorConstant.theme,
        fontSize:36,
        left:27,
        top:24,
        fontFamily:fontConstant.bold
    },
    mapStyle: {
        flex: 2,
        position: 'absolute',
        top: '12%',
        left: 0,
        right: 0,
        bottom: '65%',
    },
    view2:{
        position:'relative',
        top:'28%',
        // bottom:'4%',
        // backgroundColor:'red',
        height:'67%'
    },
    view3:{
        flexDirection:'row',
        justifyContent:'space-between',
        width:Width * 0.9,
        alignSelf:'center',
        marginTop:10
    },
    text1:{
        color:'#000000',
        fontSize:12,
        alignSelf:'flex-end'
    },
    text2:{
        color:colorConstant.theme,
        fontSize:14,
        top:5
    },
    img1:{
        width:13,
        height:16,
        // marginTop:-10
    },
    text3:{
        fontSize:14,
        color:'#000000',
        width:'90%',
        textAlign:'justify',
        left:10
    },
    view4:{
        flexDirection:'row',
        // alignSelf:'flex-start',
        width:Width * 0.90,
        // justifyContent:'space-between',
        alignItems:'center',
        marginTop:'5%',
        left:'3%'
        
    },
    view5:{
        flexDirection:'row',
        alignSelf:'flex-start',
        width:Width * 0.90,
        // justifyContent:'space-between',
        alignItems:'center',
        marginTop:'10%',
        left:'10%'
    },
    img2:{
        height:87,
        width:69
    },
    text4:{
        color:'#000000',
        fontSize:12,
        fontFamily:fontConstant.bold
    },
    view6:{
        marginTop:'10%',
        left:'5%',
        alignSelf:'flex-start'
    },
    text5:{
        color:colorConstant.theme,
        width:Width * 0.85,
        alignSelf:'center',
        // fontFamily:fontConstant.bold,
        // marginTop:'8%'
    },
    view1:{
        flexDirection:'row',
        width:120,
        justifyContent:'space-between',
        alignItems:'center'
    },
    text6:{
        color:'#000000',
        fontSize:15,
        fontWeight:'600'
    },
    status:{
        color:'#83D28A',
        fontSize:10
    },
    ratingText:{
        fontSize:16,
        color:'#000000',
        fontWeight:'700'
    },
    text7:{
        color:'#000000',
        fontSize:14,
        fontFamily:fontConstant.semibold,
        // width:'100%'
    },
    text8:{
        color:'#000000',
        fontSize:12
    },
    touch:{
        height:38,
        width:285,
        alignSelf:'center',
        justifyContent:'center',
        alignItems:'center',
        borderWidth:1,
        borderRadius:10,
        backgroundColor:'#FFFFFF',
        // marginTop:'12%'
        position:'absolute',
        bottom:'10%'
    },
    helpText:{
        fontSize:17,
        fontFamily:fontConstant.bold,
        color:'#000000'

    },
    view7:{
        width:Width* 0.9,
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:'10%',
        alignSelf:'center',
        alignItems:'center'
    },
    insideView2:{
        flexDirection:'row',
        width:'30%',
        justifyContent:'space-between',
        alignItems:'center'
    },
    insideView1:{
        width:'35%'
    },
    text9:{
        fontSize:12,
        color:'#000000'
    },
    text10:{
        fontSize:14,
        color:'#000000',
        fontFamily:fontConstant.semibold
    }
})