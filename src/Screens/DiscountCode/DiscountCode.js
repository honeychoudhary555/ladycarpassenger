import { View, Text, StyleSheet, Pressable, Image } from 'react-native'
import React from 'react'
import CustomHeader from '../../custom/CustomHeader'
import { fontConstant, imageConstant } from '../../utils/constant'
import { Width } from '../../dimensions/dimension'

const DiscountCode = (props) => {
  return (
    <View style={styles.main}>
      <CustomHeader
      title={"Discount Codes"}
      onPressBack={()=>props.navigation.goBack()}

      />
      <Pressable 
      onPress={()=>props.navigation.navigate("AddPromoCode")}
      style={styles.pres}>
    <Text style={styles.text}>Add a Promo Code</Text>
    <Image
    source={imageConstant.nextArrow}
    />
      </Pressable>
    </View>
  )
}

export default DiscountCode
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF',
        alignItems:'center'
    },
    pres:{
        flexDirection:'row',
        width:Width * 0.9,
        alignItems:'center',
        justifyContent:'space-between',
        marginTop:'10%',
        borderBottomWidth:1,
        borderColor:'rgba(0,0,0,0.1)',
        height:50
    },
    text:{
        fontSize:16,
        color:'#000000',
        fontFamily:fontConstant.bold
    }
})