import { View, Text, StyleSheet, TextInput, Image, Pressable, TouchableOpacity, ScrollView, Keyboard,Platform,BackHandler } from 'react-native'
import React, { useState } from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import CustomHeader from '../../custom/CustomHeader'
import CustomInput from '../../custom/CustomInput'
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment'
import PhoneInput from 'react-native-phone-number-input'
import CustomButton from '../../custom/CustomButton'
import CustomModal from '../../custom/CustomModal'
import { Height, Width } from '../../dimensions/dimension'
import { useEffect } from 'react'
import validation from '../../utils/validation'
import { Dropdown } from 'react-native-element-dropdown'
import { baseUrl } from '../../utils/Urls'
import AsyncStorage from '@react-native-async-storage/async-storage'
import toastShow from '../../utils/Toast'


const SignUp = (props) => {
 const [callingCode,setCallingCode]=useState(1)
  let phoneNumber=props?.route?.params.phone
  let userName=props?.route?.params.userInfo?.user?.givenName 
  let lastuser=props?.route?.params.userInfo?.user?.familyName
  let userEmail=props?.route?.params.userInfo?.user?.email
  let userID=props?.route?.params.userInfo?.user?.id
  let aplpleRes=props?.route?.params?.aplpleRes
  let id=props?.route?.params?.id
  let code=props?.route?.params?.callingCode
  const [date, setDate] = useState(new Date())
  const [padding, setPadding] = useState(10)
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [country, setCountry] = useState("India")
  const [value, setValue] = useState(null);
  const [isSelected, setSelection] = useState(false);
  const [modalVisible, setModalVisible] = useState(false)
  const [stateData, setStateData]=useState(null)
  const [passwordShow,setPasswordShow]=useState(true) 
  const [cityData,setCityData]=useState(null)
  const [dateStatus,setDateStatus]=useState(false)
  const [userData, setUserData] = useState({
    Name:userName? userName : "",
    LastName:lastuser ? lastuser :"",
    DoB: "",
    Phone: phoneNumber ? phoneNumber :"",
    Address: "",
    Email:userEmail ? userEmail : "" ,
    Password: "",
    confirmPassword: "",
    State:"",
    City:"",
    PrivacyPolicy:"",
    signupType:id=="phoneSiginIn" ? "NORMAL" : id=="emailSignIn" ? "GOOGLE" : id=="appleSignIn" ? "APPLE" : "FACEBOOK",
    socialId:id=="phoneSiginIn" ? "" : id=="emailSignIn" ? userID : id=="appleSignIn" ? aplpleRes?.user : "FACEBOOK",
  })
  useEffect(()=>{
   
    BackHandler.addEventListener('hardwareBackPress',handleBackClick);
    return ()=>{
        BackHandler.removeEventListener('hardwareBackPress',handleBackClick)
    }
},[])
  useEffect(() => {
    let sub = Keyboard.addListener('keyboardWillShow', (e) => {
      setPadding(e.endCoordinates.height + 10)
    })
    let subs = Keyboard.addListener('keyboardWillHide', (e) => {
      setPadding(10)
    })
  }, [])
  useEffect(()=>{
getStateData()
  },[])
  const handleBackClick=()=>{
    props.navigation.navigate("Welcome")
    return true
}
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    setShow(false);
    setDate(currentDate);
    setUserData({ ...userData, DoB: currentDate });
  };

  const showMode = (currentMode) => {
    if (Platform.OS === 'android') {
      setShow(false);
      // for iOS, add a button that closes the picker
    }
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
    setShow(true)
    setDateStatus(true)
  };
  const handleName = (txt) => {
    setUserData({ ...userData, Name: txt })

  }
  const handleLastName=(txt)=>{
    setUserData({ ...userData, LastName: txt })
  }
  const hanldeAddress = (txt) => {
    setUserData({...userData,Address:txt})
  }
  const handleEmail = (txt) => {
    setUserData({...userData,Email:txt})
  }
  const handlePasswsord = (txt) => {
    setUserData({...userData,Password:txt})
  }
  const handleConfirmePassword = (txt) => {
    setUserData({...userData,confirmPassword:txt})
  }


  /// ****  Get State Data **
  const getStateData=async()=>{
    try{
      let response=await fetch(baseUrl+"static/state-list",{method:"GET"})
      if(response.status==200)
      {
        let res= await response.json()
        
        setStateData(res)
        // console.log("res==>",stateData)
      }
      else{
        console.log("somthing went wrong")
      }
    }
    catch(err){
      console.log("err===>",err)
    }
  }

  //***** get City Data */

  const getCityData=async(id)=>{
    try{
      let response= await fetch(baseUrl+`static/city-list?stateId=${id}`,{
        method:'GET'
      })
      if(response.status==200)
      {
        let res=await response.json()
        setCityData(res)
        
        // console.log("response",res)
      }
      else{
        console.log("Somthing went wrong")
      }

    }
    catch(err){
      console.log(err)
    }
  }


  // ***** Submit ***
  const handleChange = () => {
if( 
  validation.nameValid(userData.Name)
  &&
  validation.dobValid(userData.DoB)
  &&
  validation.phoneValid(userData.Phone)
  &&
  validation.stateValid(userData.State)
  &&
  validation.cityValid(userData.City)
  &&
  validation.addressValid(userData.Address)
  &&
  validation.emailValid(userData.Email)
  &&
  validation.passwordValid(userData.Password)
  
  )
{
  
  if(userData.confirmPassword=="")
  {
    toastShow("Please enter confirm password")
  }
  else if
  (userData.Password==userData.confirmPassword){
    if(userData.PrivacyPolicy=="Yes"){
  verifyEmail()
    console.log("userData", userData)
  
   
    }
    else{
      toastShow("Please  accept Term & Condition")
    }
  }
    else{
      toastShow("Password and confirm Password not match")
    }
  }
  
  }
  
  console.log("value",value)
  // console.log("userInfo===>",userName,userEmail)
// *** email Verify ***

const verifyEmail=async()=>{
  if(id!="emailSignIn")
  {
  data={
    email:userData.Email,
    phoneNumber:""
  }}
  else if(id=="appleSignIn")
  {
    data={
      email:userData.Email,
      phoneNumber:userData?.Phone
    }
  }
  else{ 
    data={
      email:"",
      phoneNumber:callingCode+userData.Phone
    }
  }
  // if(validation.emailValid(userData.Email))
  // {
  try{
    // alert("ok")
    await AsyncStorage.setItem("user",JSON.stringify(userData) )
    await fetch(baseUrl+"auth/check-passenger",
    {method:'POST',
    headers:{
      'Content-Type':'application/json'
    },
    body:JSON.stringify(data)
}
    )
    .then(async(res)=>{
        let jsonData=await res.json()
        return jsonData
        
    })
    .then(async(res)=>{
        console.log("response--->",res)
        const userData = await AsyncStorage.getItem("user")
        const currentData=JSON.parse(userData)
        console.log("data from AsyncStorage",currentData)
        if(res.code==200)
        {
          toastShow(res.message)
        }
        else{
          if(id=="emailSignIn")
          {
             await AsyncStorage.setItem("callingCode",JSON.stringify(callingCode))
            props.navigation.navigate('VerificationCode',{id:id,text:"signup",phone:userData?.Phone,callingCode:callingCode})
          }
          else{
            props.navigation.navigate('IdPicture')
          }
        }
    })
   
}
catch(err)
{
    console.log(err)
}
}

  return (

    <View style={styles.main}>
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ backgroundColor: '#FFFFFF', paddingBottom: padding, alignItems: 'center' }}
      >
        {/* <CustomHeader
        onPressBack={()=>props.navigation.goBack()} 
        /> */}
        <Text style={styles.welcomText}>Welcome to LadyCar!</Text>

        <CustomInput
          width={"82%"}
          height={45}
          // value={userData.Name}
          marginTop={'5%'}
          onChangeText={handleName}
          placeholder={'First name'}
          borderColor={colorConstant.theme}
          value={userData?.Name}
        />
                <CustomInput
          width={"82%"}
          height={45}
          // value={userData.Name}
          marginTop={'5%'}
          onChangeText={handleLastName}
          placeholder={'Last name'}
          borderColor={colorConstant.theme}
          value={userData.LastName}
        />
        <Pressable onPress={showDatepicker}>
          <View style={{ width: Width }} pointerEvents='none'>
            <TextInput
              placeholderTextColor='gray'
              placeholder={'Birthday Date'}
              style={styles.textinput}
              value={dateStatus ?  moment(date).format('LL') :null}

            />
            <Image source={imageConstant.polygon} resizeMode='contain' style={{ position: 'absolute', widht: 11, height: 7, right: '13%', top: 40 }} />

          </View>
        </Pressable>
        {show && (
          <DateTimePicker
            //   testID="dateTimePicker"
            // style={{marginTop:Platform.OS=='ios' ? -20 :0}}
            value={date}
            mode={mode}
            display={Platform.OS=='ios' ? 'spinner' :'default'}
            is24Hour={true}
            onChange={onChange}
            minimumDate={new Date(moment().subtract(120, "years"))} 
            maximumDate={new Date()}
          />
        )}
        {id!="emailSignIn" && id!="appleSignIn" ?
<View pointerEvents='none'>
<CustomInput
          width={"82%"}
          height={45}
          // value={userData.Name}
          marginTop={'5%'}
          onChangeText={handleName}
          // placeholder={'Last name'}
          borderColor={colorConstant.theme}
          value={"+"+code+" "+phoneNumber}
        />
        

        </View>
        :
        <PhoneInput
        
        defaultValue={phoneNumber}
     defaultCode="US"
     onChangeCountry={(c) =>{
      setCallingCode(c.callingCode[0])
      setCountry(c.name)}}
     layout="first"
    
     onChangeText={(text) => {
       setValue(text);
       setUserData({ ...userData, Phone: text })
     }}
     placeholder='Phone Number'
     
    //  onChangeFormattedText={(text) => {
    //    // setFormattedValue(text);
    //  }}
     withDarkTheme
     //   withShadow
     //   autoFocus
     textInputStyle={{
       paddingVertical: 0,
       height: 20

     }}
     containerStyle={styles.phoneContainer}
     flagButtonStyle={styles.flag}
     codeTextStyle={{ height: 0, width: 0 }}
     textContainerStyle={styles.textContainer}
   />
    }
        {/* <View style={{width:'82%',marginTop:'5%'}}> */}
          <Dropdown
          data={stateData?.stateList}
          // valueField="value"
          value="_id"
          placeholder='State'
          maxHeight={Height * 0.40 }
          labelField="name"
          style={styles.dropdown}
          onChange={item => {
            setValue(item._id);
            getCityData(item._id)
            setUserData({...userData,State:item._id,City:""})
            // setUserData({...userData,City:""})
            // alert("ok")
            console.log(item._id)
          }}
          />
          {
            value==null ? null
            :
            <Dropdown
            data={cityData?.askedCity}
            // valueField="value"
            value="_id"
            placeholder='City'
            maxHeight={Height * 0.40 }
            labelField="cityName"
            style={styles.dropdown}
            onChange={item => {
              // setValue(item._id);
              setUserData({...userData,City:item._id})
              console.log(item._id)
            }}
            />
          }
        {/* </View> */}

        <CustomInput
          width={"82%"}
          height={45}
          placeholder={'Address'}
          borderColor={colorConstant.theme}
          marginTop={'5%'}
          onChangeText={hanldeAddress}
        />
        {id=="emailSignIn"  ?
        <View pointerEvents='none'>
        <CustomInput
          width={"82%"}
          height={45}
          marginTop={'5%'}
          // editable={ ? false :true}
          placeholder={'Email address'}
          borderColor={colorConstant.theme}
          onChangeText={handleEmail}
          value={userData.Email}
        />
        </View>
        :
        <CustomInput
          width={"82%"}
          height={45}
          marginTop={'5%'}
          // editable={ ? false :true}
          placeholder={'Email address'}
          borderColor={colorConstant.theme}
          onChangeText={handleEmail}
          value={userData.Email}
        />}
        <CustomInput
          width={'82%'}
          height={45}
          marginTop={'5%'}
          imgmarginTop={4}
          imageName={imageConstant.Eye}
          imgFlag={true}
          placeholder={'Password'}
          borderColor={colorConstant.theme}
          paddingTop={5}
          secureTextEntry={passwordShow ? true : false}
          onChangeText={handlePasswsord}
          onPress={()=>setPasswordShow(!passwordShow)}
        />
        <CustomInput
          width={"82%"}
          height={45}
          marginTop={'5%'}
          placeholder={'Confirm password'}
          borderColor={colorConstant.theme}
          onChangeText={handleConfirmePassword}
        />


        <View style={{
          width: "70%",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          marginTop: 20

        }}>
          <Text style={{ width: 280, lineHeight: 23, right: 15, textAlign: 'justify', color: colorConstant.black, fontSize: 14, fontFamily: fontConstant.regular }}>I accept the LadyCar <Text  style={{ color: colorConstant.theme, textDecorationLine: 'underline' }}><Text onPress={()=>props.navigation.navigate('TermAndService')}>Terms of Service,</Text> Community Guidelines, and <Text onPress={()=>props.navigation.navigate('PrivacyPolicy')}>Privacy Policy.</Text> </Text>(Required)</Text>
          <TouchableOpacity activeOpacity={0.9} onPress={() =>{
            setSelection(!isSelected)
            setUserData({...userData,PrivacyPolicy:isSelected ? "No" : "Yes"})
          } } style={{
            width: 20, height: 20, borderWidth: 1.5, borderRadius: 10, borderColor: colorConstant.theme,
            backgroundColor: isSelected ? colorConstant.theme : null
          }}></TouchableOpacity>
        </View>

        <CustomButton
          buttonName={'Continue'}
          marginTop={20}
          width={296}
          height={42}
          borderRadius={10}
          marginBottom={30}
          OnButtonPress={handleChange}
        //  OnButtonPress={()=>props.navigation.navigate('IdPicture')}
        />
        <CustomModal
          animationType={'none'}
          transparent={true}
          modalText={'Your passwords do not match. Please try again.'}
          visible={modalVisible}
          onRequestClose={() => setModalVisible(!modalVisible)}
        />
      </ScrollView>
    </View>

  )
}

export default SignUp
const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  },
  welcomText: {
    color: '#6C00C0',
    fontFamily: fontConstant.semibold,
    fontSize: 29,
    width: '82%',
    lineHeight: 37,
    textAlign: 'center',
    marginTop: '19%',

  },
  textinput: {
    borderColor: colorConstant.theme,
    borderWidth: 1.5,
    width: '82%',
    marginTop: 20,
    borderRadius: 9,
    height: 45,
    // justifyContent:'space-around',
    paddingLeft: 18,
    alignSelf: 'center'
  },
  phoneContainer: {
    marginTop: 20,
    borderWidth: 1.5,
    borderColor: colorConstant.theme,
    borderRadius: 10,
    width: "82%",
    height: 45

  },
  flag: {
    paddingVertical: 0,
    borderRadius: 10,
    // height:50,
    height: 35,
  },
  textContainer: {
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10
  },
  dropdown:{
    width:'82%',
    height:45,
    borderColor:colorConstant.theme,
    borderWidth:1,
    borderRadius:9,
    marginTop:'5%',
    paddingHorizontal:10
},
})