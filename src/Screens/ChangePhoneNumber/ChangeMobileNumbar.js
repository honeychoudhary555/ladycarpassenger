import {
    Image,
    Text,
    TouchableOpacity,
    View,
StyleSheet
} from "react-native";
import { colorConstant, fontConstant, imageConstant, } from "../../utils/constant";


import { moderateScale, scale } from "react-native-size-matters";
import {
    SafeAreaView,
} from "react-native-safe-area-context";
import PhoneInput from "react-native-phone-number-input";
// import CustomHeader from "../../customComponents/CustomHeader";
import { useState } from "react";
// import { checkUserExist } from "../../services/AuthServices";
// import toastShow from "../../utils/toastShow";
import { KeyboardAwareScrollView } from "@codler/react-native-keyboard-aware-scroll-view";
import CustomHeader from "../../custom/CustomHeader";
import toastShow from "../../utils/Toast";
import { baseUrl } from "../../utils/Urls";



const ChangeMobileNumbar = (props) => {
    const [num,setNum]=useState("")
    const [callingCode,setCallingCode]=useState(1)
    let paramData= props?.route?.params?.phoneNumber
    console.log(paramData,num)
    const inputHandler = (text) => {
        setNum(text)
      };
   


    const checkUserExistFunction=async()=>{
        if(num==" "){
            toastShow("Please enter number",'red')
            return
        }
        if(num.length<8){
            toastShow('please enter valid number','red')
            return
        }
        let objToSend={
            phoneNumber:num,
            email:""
        }
        try {
            const response =await checkUserExist(objToSend)
            console.log(response.data,'dhdhdhd')
            toastShow("Number already exits ",'red')
        } catch (error) {
            props.navigation.navigate('OtpVerify',{flow:'ChangeNumber',number:num})
        }
    }



    const handleNumber=async()=>{
        // alert("ok")
        console.log("phone number",num)
          data={
              email:"",
              phoneNumber:callingCode+num,
          }
  
          var phoneRe = /^[0-9]{8,13}$/;
          if(validation.phoneValid(num))
          {
              try{
                  let response=await fetch(baseUrl+"auth/check-passenger",
                  {
                  method:'POST',
                  body:JSON.stringify(data),
                  headers: { 'Content-Type': 'application/json' },
              }
                  )
                  .then(async(res)=>{
                      let jsonData=await res.json()
                      return jsonData
  
                  })
                  .then((res)=>{
                      console.log("response--->",res)
                      if(res.code==200)
                      {
                          toastShow("This Phone Number is already exist")
                      }
                      else
                      {
                          props.navigation.navigate('VerificationCode',{id:"ChangePhone",text:"ChangePhone",phone:num,callingCode:callingCode})
  
                      }
                  })
                 
  
                  setTimeout(()=>{
                  },2000)
              }
              catch(err)
              {
                  console.log("ERRROR",err)
              }
          }
       
      }

    return (
        <SafeAreaView style={{ flex: 1,  }}>
            <View style={{ flex: 1, alignItems: "center",backgroundColor:'#FFFFFF' }}>

                
            <CustomHeader
            color={colorConstant.theme}
                marginTop={35}
              img={imageConstant.CustomHeader}
              tintColor={colorConstant.theme}
              title={'Change phone number'}
              navigation = {props.navigation}
              fontSize={22}
              left={20}
          />
          <KeyboardAwareScrollView 
          showsVerticalScrollIndicator={false}
          style={{flex:1,width:'100%'}}>

                <Image
                    source={imageConstant.boarding2}
                    style={{ marginVertical: moderateScale(70),alignSelf:"center" }}
                />
                <View style={{alignSelf:'center'}}>
             <PhoneInput
          // defaultValue={phoneNumber}
          defaultCode="US"
          onChangeCountry={(c) => {
            setCallingCode(c.callingCode[0])
            // setCountry(c.name)
          }}
          layout="first"

          onChangeText={(text) => {
            setNum(text);
            // setUserData({ ...userData, Phone: text })
          }}
          placeholder='Phone Number'

          //  onChangeFormattedText={(text) => {
          //    // setFormattedValue(text);
          //  }}
          withDarkTheme
          //   withShadow
          //   autoFocus
          textInputStyle={{
            paddingVertical: 0,
            height: 20

          }}
          containerStyle={styles.phoneContainer}
          flagButtonStyle={styles.flag}
          codeTextStyle={{ height: 0, width: 0 }}
          textContainerStyle={styles.textContainer}
        />
        </View>

                <TouchableOpacity
                 onPress={handleNumber}
                    style={{
                        backgroundColor: colorConstant.theme,
                        width: "80%",
                        borderRadius: 10,
                        paddingVertical: moderateScale(13),
                        alignItems: "center",
                        marginTop: moderateScale(30),
                        alignSelf:'center'
                    }}
                >
                    <Text
                        style={{
                            color: colorConstant.white,
                            fontFamily: fontConstant.semi,
                            fontSize: moderateScale(15),
                        }}
                    >
                        Continue
                    </Text>
                </TouchableOpacity>
                </KeyboardAwareScrollView>
            </View>
        </SafeAreaView>
    )
}

export default ChangeMobileNumbar
const styles=StyleSheet.create({
    phoneContainer: {
        marginTop: 20,
        borderWidth: 1.5,
        borderColor: colorConstant.theme,
        borderRadius: 10,
        width: "80%",
        height: 43
    
      },
      flag: {
        paddingVertical: 0,
        borderRadius: 10,
        // height:50,
        height: 35,
      },
      textContainer: {
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10
      },
})