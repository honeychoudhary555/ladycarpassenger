import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { Width } from '../dimensions/dimension'

const CustomHeader = (props) => {
  return (
    <View style={{width:Width,top: props.Viewtop  ? props.Viewtop : '5%',marginBottom:props.marginBottom,backgroundColor:"#FFFFFF",padding:props.padding ? props.padding :5}}>
      <TouchableOpacity
      style={{alignSelf:'flex-start',left:'5%'}}
       onPress={props.onPressBack ? props.onPressBack :()=>props.navigation.goBack()}
      >
      <Image resizeMode='contain' source={props.imgName ? props.imgName : imageConstant.Arrow} style={{width:27}}/>
      
      </TouchableOpacity>
      <Text style={{alignSelf:'center',fontSize:24,fontFamily:fontConstant.bold,top:props.top?props.top:-25,color:props.color?props.color:colorConstant.theme,left:props.left}}>{props.title}</Text>
    </View>
  )
}

export default CustomHeader