import { View, Text, StyleSheet, Image, ScrollView, Modal } from 'react-native'
import React from 'react'
import { fontConstant, imageConstant } from '../../utils/constant'
import CustomHeader from '../../custom/CustomHeader'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import CustomButton from '../../custom/CustomButton'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { baseUrl } from '../../utils/Urls';
import { useState } from 'react';
import LottieView from 'lottie-react-native';
import toastShow from '../../utils/Toast';
import { ImageUploadInBucket } from '../../custom/ImageUploadInBucket';

const StateId = (props) => {
  const [modalVisible,setModalVisible]=useState(false)
  const [status, setStatus]=useState(false)
  const openCamera = async () => {
    // alert('hello')
    let options = {
      mediaType: 'photo',
      cameraType: 'back',
      saveToPhotos: true,
      quality:0.9,
      maxHeight:500,
      maxWidth:500,
    }
    // launchImageLibrary(options,(response)=>{
    //     console.log('response', response)
    // })
    // launchCamera(options,(res)=>{
    //     console.log(res)
    // })
    launchCamera(options, async(response) => {
      if (response.didCancel) {
        // toast('User cancelled ', colorConstant.darkRed)
        console.log('User cancelled image picker');
      } else if (response.error) {
        // toast('Something went wrong', colorConstant.darkRed)
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);

      } else {
       
        console.log("response",response);
       
        console.log('response from gallery------------------->', JSON.stringify(response));
        let formDataReq = new FormData();
        let data = {
            uri:response?.assets?.[0]?.uri,
            type:response?.assets?.[0]?.type,
            name:response?.assets?.[0]?.fileName
        }
        setModalVisible(true)
        try{
        let imageData=await ImageUploadInBucket(data)
        console.log("image front wala",imageData)
        await AsyncStorage.setItem("idBack",JSON.stringify(imageData?.postResponse?.location))
        // setModalVisible(false)
        setTimeout(() => {
          setStatus(true)
          setModalVisible(false)
          props.navigation.navigate("FaceId",{id:"State"})
        }, 3000);
      
        }
        catch(err){
          setModalVisible(false)
      toastShow("please upload image again")
        }
      
      }

    })


  }
  const openCameras = async () => {
    let options = {
      mediaType: 'photo',
      cameraType: 'back',
      saveToPhotos: true,
      quality:0.9,
      maxHeight:500,
      maxWidth:500,
    }
    // launchImageLibrary(options,(response)=>{
    //     console.log('response', response)
    // })
    launchCamera(options, async(response) => {
      if (response.didCancel) {
        // toast('User cancelled ', colorConstant.darkRed)
        console.log('User cancelled image picker');
      } else if (response.error) {
        // toast('Something went wrong', colorConstant.darkRed)
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);

      } else {
        console.log("response",response);
        // props.navigation.navigate("FaceId")
        console.log('response from gallery------------------->', JSON.stringify(response));
        let formDataReq = new FormData();
        let data = {
            uri:response?.assets?.[0]?.uri,
            type:response?.assets?.[0]?.type,
            name:response?.assets?.[0]?.fileName
        }

        setModalVisible(true)
        try{
        let imageData=await ImageUploadInBucket(data)
        console.log("image front wala",imageData)
        await AsyncStorage.setItem("idFront",JSON.stringify(imageData?.postResponse?.location))
        setTimeout(() => {
          setModalVisible(false)
          setStatus(true)
        }, 5000);

        }
        catch(err)
        {
          setModalVisible(false)
      toastShow("please upload image again")
        }
        // formDataReq.append("file",data);
        // updateProfileImage(formDataReq);
        // getImageData(formDataReq._parts[0][1])
        // await AsyncStorage.setItem("idFront",JSON.stringify(response))
      }
    })
  }

  const getImageData=async(imageName)=>{
    // let data={
    //   fileName:imageName
    // }
    setModalVisible(true)
    try{

    }
    catch(err){
      setModalVisible(false)
      toastShow("please upload image again")
    }
    let finalFormData=new FormData()
    finalFormData.append("fileName",imageName)
    console.log("image wala",finalFormData)
   await fetch(baseUrl+"auth/image-upload",
   {
    method:"POST",
    headers:{
      'Content-Type': 'multipart/form-data',
      'Accept':'application/json'
    },
    body:finalFormData
   }
   )
   .then(async(res)=>{
    setStatus(true)
    let jsonData=await res.json()
    return jsonData;
   })
   .then(async(res)=>{
    console.log("resssss",res)
    await AsyncStorage.setItem("idFront",JSON.stringify(res.data))
    setModalVisible(false)
    
   })
  }
  
  const getImageData2=async(imageName)=>{
    // let data={
    //   fileName:imageName
    // }
    setModalVisible(true)
    try{
      let finalFormData=new FormData()
      finalFormData.append("fileName",imageName)
      console.log("image wala",finalFormData)
     await fetch(baseUrl+"auth/image-upload",
     {
      method:"POST",
      headers:{
        'Content-Type': 'multipart/form-data',
        'Accept':'application/json'
      },
      body:finalFormData
     }
     )
     .then(async(res)=>{
      let jsonData=await res.json()
      return jsonData;
     })
     .then(async(res)=>{
      console.log("resssss",res)
      await AsyncStorage.setItem("idBack",JSON.stringify(res.data))
      setModalVisible(false)
      setStatus(true)
      props.navigation.navigate("FaceId",{id:"State"})
     })
    }
    catch(err){
      setModalVisible(false)
      toastShow("please upload image again")
    }
    
  }
  return (
    <View style={styles.main}>
      <CustomHeader
        onPressBack={() =>props.navigation.goBack()}
      />
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ backgroundColor: '#FFFFFF', alignItems: 'center', marginTop: '8%',paddingBottom:50 }}
      >
        <Text style={styles.welcomText}>Take a picture of
          your ID <Image source={imageConstant.info} style={{ height: 16, width: 16 }} /></Text>
        <Image
          source={imageConstant.camera}
          style={styles.img}
          resizeMode='contain'
        />
        <CustomButton
          buttonName={'State ID front'}
          marginTop={'20%'}
          borderRadius={10}
          // imageRightFlag={true}
          txtmarginTop={'1%'}
          OnButtonPress={() => openCameras()}

          imageNames={status ? imageConstant.check : null}
        />
        <CustomButton
          buttonName={'State ID back'}
          marginTop={'5%'}
          borderRadius={10}
          OnButtonPress={() => openCamera()}
          // imageName={imageConstant.check}
          disabled={status ? false : true}
          // imageRightFlag={status ? false : true}
          marginBottom={20}
          txtmarginTop={'1%'}
        />
      </ScrollView>
      <Modal
        visible={modalVisible}
        transparent={true}
        onRequestClose={()=>setModalVisible(!modalVisible)}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            backgroundColor:'rgba(0,0,0,0.6)'
          }}
        >
          <View>
            <LottieView source={require('../../asset/images/loader.json')} style={{ height:100 }} autoPlay />
          </View>
        </View>
      </Modal>
    </View>
  )
}

export default StateId
const styles = StyleSheet.create({
  main: {
    flex: 1,
    // alignItems:'center',
    backgroundColor: '#FFFFFF'
  },
  img: {
    width: 240,
    height: 287,
    marginTop: '10%'
  },
  welcomText: {
    color: '#6C00C0',
    fontFamily: fontConstant.semibold,
    fontSize: 29,
    width: '75%',
    lineHeight: 37,
    textAlign: 'center',
    marginTop: '3%',

  },
})