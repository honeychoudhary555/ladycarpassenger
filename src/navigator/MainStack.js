import { View, Text } from 'react-native'
import React from 'react'
import Landing from '../Screens/Landing/Landing'
import ScheduleTrip from '../Screens/ScheduleTrip/ScheduleTrip'
import DriverSelect from '../Screens/DriverSelect/DriverSelect'
import DriverApproval from '../Screens/DriverApproval/DriverApproval'
import RequestSuccess from '../Screens/RequestSuccess/RequestSuccess'
import ConfirmBooking from '../Screens/ConfirmBooking/ConfirmBooking'
import IdVerification from '../Screens/IdVerfication/IdVerification'
import OnTheWay from '../Screens/OnTheWay/OnTheWay'
import TripSummary from '../Screens/TripSummary/TripSummary'
import ReportDriver from '../Screens/ReportDriver/ReportDriver'
import RecivedComment from '../Screens/CommentNew/RecivedComment'
import Destination from '../Screens/Destination/Destination'
import OnComplete from '../Screens/AllId/OnComplete'
import CabBooking from '../Screens/BookCab/CabBooking'
import Payment from '../Screens/Payment/Payment'
import AddCard from '../Screens/AddCard/AddCard'
import RequestCancel from '../Screens/RequestCancel/RequestCancel'
import ScheduleRide from '../Screens/ScheduleRide/ScheduleRide'
import TripDetails from '../Screens/TripDetails/TripDetails'
import MyAccount from '../Screens/MyAccount/MyAccount'
import MyWallet from '../Screens/MyWallet/MyWallet'
import ForgetPassword from '../Screens/ForgetPassword/ForgetPassword'
import ResetPassword from '../Screens/ResetPassword/ResetPassword'
import CancelReason from '../Screens/CancelReason/CancelReason'
import Chat from '../Screens/Chat/Chat'
import Profile from '../Screens/Profile/Profile'
import HelpCenter from '../Screens/HelpCenter/HelpCenter'
import History from '../Screens/History/History'
import DiscountCode from '../Screens/DiscountCode/DiscountCode'
import AddPromoCode from '../Screens/AddPromoCode/AddPromoCode'
import CommentsAboutYou from '../Screens/Comments/CommentsAboutYou'
import ConfirmSchedule from '../Screens/ConfirmSchedule/ConfirmSchedule'
import BookingProcess from '../Screens/ProcessBooking/BookingProcess'
import BookingConfirm from '../Screens/ProcessBooking/BookingConfirm'
import AddMoney from '../Screens/AddMoney/AddMoney'
import Payment2 from '../Screens/Payment/Payment2'
import PaymentSuccess from '../Screens/Payment/PaymentSuccess'
import Helpcenter2 from '../Screens/HelpCenter/Helpcenter2'
import MyComments from '../Screens/Comments/MyComments'
import Comments from '../Screens/Comments/Comments'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { Height } from '../dimensions/dimension'
import DrawerContainer from '../Screens/DrawerContainer/DrawerContainer'
import Request from '../Screens/Request/Request'
import VerificationCode from '../Screens/VerifcationCode/VerificationCode'
import WriteAbout from '../Screens/WriteAbout/WriteAbout'
import AwsS3 from '../Screens/Aws/AwsS3'
import ChangeMobileNumbar from '../Screens/ChangePhoneNumber/ChangeMobileNumbar'
import Changepassword from '../Screens/ChangePassword/Changepassword'
// import PrivacyPolicy from '../Screens/PrivacyPolicy/PrivacyPolicy'

const Stack = createNativeStackNavigator();
// const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

    const DrawerStack=()=>(
        <>
        <Drawer.Navigator
        initialRouteName='ScheduleTrip'
       
        screenOptions={{
          headerShown: false,
          drawerType:"front",
          drawerStyle: {
            width:'50%',
           height:Height * 0.80,
          //  backgroundColor:'blue',
            borderBottomRightRadius:70
          },}}
          drawerContent={props=><DrawerContainer {...props}/>}
        
        >
        <Drawer.Screen  name="Landing" screenOptions={{headerShown:false}} component={Landing}/>
        <Drawer.Screen name="ScheduleTrip" component={ScheduleTrip} options={{headerShown:false}} />
        <Drawer.Screen name="Request" component={Request} options={{headerShown:false}} />
        <Drawer.Screen name="DriverSelect" component={DriverSelect} options={{headerShown:false}} />
          <Drawer.Screen name="DriverApproval" component={DriverApproval} options={{headerShown:false}} />
          <Drawer.Screen name="RequestSuccess" component={RequestSuccess} options={{headerShown:false}} />
          <Drawer.Screen name="ConfirmBooking" component={ConfirmBooking} options={{headerShown:false}} />
          <Drawer.Screen name="IdVerification" component={IdVerification} options={{headerShown:false}} />
          <Drawer.Screen name="OnTheWay" component={OnTheWay} options={{headerShown:false}} />
          <Drawer.Screen name="TripSummary" component={TripSummary} options={{headerShown:false}} />
          <Drawer.Screen name="ReportDriver" component={ReportDriver} options={{headerShown:false}} />
          <Drawer.Screen name="RecivedComment" component={RecivedComment} options={{headerShown:false}} />
    
    
        </Drawer.Navigator>
    
        </>
      )
const MainStack = () => {

  return (
 <Stack.Navigator initialRouteName='DrawerStack' screenOptions={{ headerShown: false }}>
    <Stack.Screen name="DrawerStack" component={DrawerStack}  />
     <Stack.Screen name="Destination" component={Destination}  />
      <Stack.Screen name="OnComplete" component={OnComplete}  />
      <Stack.Screen name="CabBooking" component={CabBooking}  />
      <Stack.Screen name="Payment" component={Payment}  />
      <Stack.Screen name="AddCard" component={AddCard}  />
      <Stack.Screen name="RequestCancel" component={RequestCancel}  />
      <Stack.Screen name="ScheduleRide" component={ScheduleRide}  />
      <Stack.Screen name="TripDetails" component={TripDetails}  />
      <Stack.Screen name="MyAccount" component={MyAccount}  />
      <Stack.Screen name="MyWallet" component={MyWallet}  />
      <Stack.Screen name="ForgetPassword" component={ForgetPassword}  />
      <Stack.Screen name="VerificationCode" component={VerificationCode}  />

      <Stack.Screen name="ResetPassword" component={ResetPassword}  />
      <Stack.Screen name="CancelReason" component={CancelReason}  />
      <Stack.Screen name="Chat" component={Chat}  />
      <Stack.Screen name="Profile" component={Profile}  />
      <Stack.Screen name="HelpCenter" component={HelpCenter}  />
      <Stack.Screen name="History" component={History}  />
      <Stack.Screen name="DiscountCode" component={DiscountCode}  />
      <Stack.Screen name="AddPromoCode" component={AddPromoCode}  />
      <Stack.Screen name="CommentsAboutYou" component={CommentsAboutYou}  />
      <Stack.Screen name="ConfirmSchedule" component={ConfirmSchedule}  />
      <Stack.Screen name="BookingProcess" component={BookingProcess}  />
      <Stack.Screen name="BookingConfirm" component={BookingConfirm}  />
      <Stack.Screen name="AddMoney" component={AddMoney}  />
      <Stack.Screen name="Payment2" component={Payment2}  />
      <Stack.Screen name="PaymentSuccess" component={PaymentSuccess}  />
      <Stack.Screen name="Helpcenter2" component={Helpcenter2}  />
      <Stack.Screen name="MyComments" component={MyComments}  />
      <Stack.Screen name="Comments" component={Comments}  />
      <Stack.Screen name="WriteAbout" component={WriteAbout}  />
      <Stack.Screen name="ChangeMobileNumbar" component={ChangeMobileNumbar}  />
      <Stack.Screen name="Changepassword" component={Changepassword}  />
      
      
 </Stack.Navigator>
  )
}

export default MainStack