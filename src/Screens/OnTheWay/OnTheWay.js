import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity, Pressable } from 'react-native'
import React from 'react'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Height, Width } from '../../dimensions/dimension';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CheckBox from '@react-native-community/checkbox';
import { useState } from 'react';
import CustomButton from '../../custom/CustomButton';
import { useFocusEffect } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';


const OnTheWay = (props) => {
    const [check, setCheck] = useState(true)
    const [SelectedId, setSelectedId] = useState(null)
    const [status, setStatus] = useState(false)
    const [value, setValue] = useState(true)
    useFocusEffect(() => {
        setTimeout(() => {
            props.navigation.navigate('TripSummary')
        }, 4000)
    })
    const mapStyle = [
        { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
        {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{ color: '#263c3f' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#6b9a76' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{ color: '#38414e' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#212a37' }],
        },
        {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#9ca5b3' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{ color: '#746855' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#1f2835' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#f3d19c' }],
        },
        {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{ color: '#2f3948' }],
        },
        {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{ color: '#17263c' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#515c6d' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{ color: '#17263c' }],
        },
    ];




    return (
        <View style={styles.main}>
            <ScrollView
                bounces={false}
                showsVerticalScrollIndicator={false}
                style={{ backgroundColor: '#FFFFFF' }}>
                <View style={{ height: Height * 0.6 }}>
                    <MapView
                        zoomEnabled={true}
                        zoomTapEnabled={true}
                        style={styles.mapStyle}
                        initialRegion={{
                            latitude: 28.5355,
                            longitude: 77.3910,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                        customMapStyle={mapStyle}>
                        <Marker
                            icon={imageConstant.location}
                            draggable
                            coordinate={{
                                latitude: 37.78825,
                                longitude: -122.4324,
                            }}
                            onDragEnd={
                                (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                            }
                            title={'Test Marker'}
                            description={'This is a description of the marker'}
                        />
                    </MapView>
                    <View style={styles.view}>
                        <Pressable onPress={() => props.navigation.toggleDrawer()}>
                            <Image
                                resizeMode='contain'
                                style={{ height: 42, width: 42 }}
                                source={imageConstant.menuIcon}
                            />
                        </Pressable>
                        <Text style={styles.text}>You’re on your way!</Text>
                        <Pressable onPress={() => props.navigation.navigate("Profile")}>
                            <Image
                                resizeMode='contain'
                                style={styles.img1}
                                source={imageConstant.profile}
                            />
                        </Pressable>
                    </View>
                    <Image
                        style={styles.img2}
                        source={imageConstant.Zooms}
                    />
                </View>
                <View style={styles.view1}>
                    <View style={styles.view4}>
                        <Text style={styles.text1}>5 minutes <Text style={{ fontFamily: fontConstant.regular }}>left to your destination</Text></Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('ConfirmBooking')}
                        style={styles.touch}>
                        <Image
                            style={{ borderWidth: 2, borderColor: '#FFFFFF', borderRadius: 50 }}
                            source={imageConstant.profile2} />
                        <View>
                            <View style={styles.view7}>
                                <Text style={{ color: '#000000', fontSize: 18, fontFamily: fontConstant.bold }}>SONIA </Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image
                                        source={imageConstant.star} />
                                    <Text style={{ fontSize: 15, color: '#000000', left: 5 }}>4.3</Text>


                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                            resizeMode='contain'
                            style={{height:24,width:24}}
                                source={imageConstant.info}
                            />
                            <Image
                                resizeMode='contain'
                                style={{ height: 50, width: 70, top: 2, left: 10 }}
                                source={imageConstant.emergency}
                            />
                        </View>

                    </TouchableOpacity>
                    <View style={styles.view6}>
                        <Image
                            style={{ marginTop: 10 }}
                            resizeMode='contain'
                            source={imageConstant.passengercar}
                        />
                        <View>
                            <Text style={styles.text3}>4 min</Text>
                            <Image
                                source={imageConstant.Arrow5}
                            />
                            <Text style={styles.text4}>Your Location</Text>
                        </View>
                        <Image
                            resizeMode='contain'
                            style={{ height: 30, width: 27 }}
                            source={imageConstant.gpspoint}
                        />
                        <View>
                            <Text style={styles.text3}>7 min</Text>
                            <Image
                                source={imageConstant.Arrow5}
                            />
                            <Text style={styles.text4}>Destination</Text>
                        </View>
                        <Image
                            resizeMode='contain'
                            style={{ height: 30, width: 27 }}
                            source={imageConstant.newLoc}
                        />
                        <Text style={styles.text5}>$9</Text>
                    </View>
                    <TouchableOpacity

                        style={styles.view5}>
                        <Text style={styles.cancelText}>Clio 5 black</Text>
                        <Image
                            source={imageConstant.passengercar}
                        />
                        <View style={{ flexDirection: 'row', height: 30, alignItems: 'center', borderWidth: 1, backgroundColor: '#FFFFFF' }}>
                            <View style={{ width: 13, backgroundColor: colorConstant.theme, height: 30, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 9, color: '#FFFFFF', alignSelf: 'center' }}>Ro</Text>
                            </View>
                            <Text style={{ color: '#000000', fontFamily: fontConstant.semibold, fontSize: 12, paddingHorizontal: 5 }}>PH-333-EV</Text>
                        </View>
                    </TouchableOpacity>
                </View>

            </ScrollView>
        </View>
    )
}

export default OnTheWay
const styles = StyleSheet.create({
    main: {
        flex: 1,
        // alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    mapStyle: {
        flex: 2,
        position: 'absolute',
        top: '28%',
        left: 0,
        right: 0,
        bottom: 0,
    },
    view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Width * 0.95,
        marginTop: '2%'
    },
    view1: {
        backgroundColor: '#FFFFFF',
        height: '40%',
        // position: 'absolute',
        // top: '60%',
        width: Width,
        alignItems: 'center',


    },
    text: {
        fontSize: 36,
        color: colorConstant.theme,
        width: '80%',
        top: 25,
        fontFamily: fontConstant.semibold,
        height: 90,
        textAlign: 'center',

    },
    text1: {
        color: '#FFFFFF',
        fontSize: 15,
        fontFamily: fontConstant.bold
    },
    view2: {
        width: '95%',
        backgroundColor: '#FFFFFF',
        height: '20%',
        // borderTopWidth:1,
        marginTop: '3%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        // marginTop:'5%',
        alignItems: 'center'
    },
    text2: {
        color: colorConstant.theme,
        fontFamily: fontConstant.bold,
        fontSize: 19,
        width: '85%'
    },
    view3: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // alignItems:'center',
        marginTop: '8%'
    },
    cancelText: {
        alignSelf: 'center',
        color: colorConstant.black,
        // marginTop:'5%'
    },
    img1: {
        borderWidth: 2,
        borderColor: '#FFFFFF',
        borderRadius: 100,
        right: 5,
        height: 40,
        width: 40
    },
    img2: {
        alignSelf: 'flex-end',
        right: 5,
        top: '10%'
    },
    img3: {
        marginTop: '5%',
        width: 77,
        height: 6
    },
    touch: {
        flexDirection: 'row',
        height: 80,
        backgroundColor: '#FFFFFF',
        width: Width * 0.95,
        borderRadius: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10
    },
    view4: {
        backgroundColor: '#6C00C0',
        width: Width,
        height: 22,
        alignItems: 'center',
        justifyContent: 'center',
        // borderRadius:20,
        shadowColor: '#000000',
        elevation: 5,
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 3

    },
    view5: {
        backgroundColor: 'rgba(108, 0, 192, 0.08)',
        width: Width,
        // shadowColor:'#000000',
        // elevation:5,
        // shadowOffset:{width:2,height:2},
        // shadowOpacity:1,
        // shadowRadius:5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: 70,
        // borderRadius:20,
        marginTop: '7%',
        marginBottom: 20
        // bottom:20,

    },
    text3: {
        fontSize: 14,
        color: '#000000'
    },
    text4: {
        fontSize: 8,
        color: '#000000'
    },
    text5: {
        fontSize: 24,
        color: '#000000',
        fontFamily: fontConstant.semibold
    },
    view6: {
        flexDirection: 'row',
        width: Width * 0.9,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        marginTop: '5%'
    },
    view7: {
        right: '50%'
    }
})