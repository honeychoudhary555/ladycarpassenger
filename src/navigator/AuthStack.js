import { View, Text } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import StartAdventure from '../Screens/StartAdventure/StartAdventure';
import VerificationCode from '../Screens/VerifcationCode/VerificationCode';
import CustomHeader from '../custom/CustomHeader';
import SignUp from '../Screens/SignUp/SignUp';
import IdPicture from '../Screens/IdPicture/IdPicture';
import Login from '../Screens/Login/Login';
import StateId from '../Screens/AllId/StateId';
import Passport from '../Screens/AllId/Passport';
import DriverLicense from '../Screens/AllId/DriverLicense';
import StudentId from '../Screens/AllId/StudentId';
import FaceId from '../Screens/FaceRecognition/FaceId';
import WriteAbout from '../Screens/WriteAbout/WriteAbout';
import Welcome from '../Screens/WelCome/Welcome'
import OnComplete from '../Screens/AllId/OnComplete';
import ForgetPassword from '../Screens/ForgetPassword/ForgetPassword';
import ResetPassword from '../Screens/ResetPassword/ResetPassword';
import PrivacyPolicy from '../Screens/PrivacyPolicy/PrivacyPolicy';
import TermAndService from '../Screens/TermAndService/TermAndService';


const Stack=createNativeStackNavigator()
const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false ,animation:"slide_from_right"}} initialRouteName="Welcome">
     <Stack.Screen name="Welcome" component={Welcome}  />
      <Stack.Screen name="StartAdventure" component={StartAdventure}  />
      <Stack.Screen name="VerificationCode" component={VerificationCode}  />
      <Stack.Screen name="CustomHeader" component={CustomHeader}  />
      <Stack.Screen name="SignUp" component={SignUp}  />
      <Stack.Screen name="IdPicture" component={IdPicture}  />
      <Stack.Screen name="Login" component={Login}  />
      <Stack.Screen name="StateId" component={StateId}  />
      <Stack.Screen name="ForgetPassword" component={ForgetPassword}  />
      <Stack.Screen name="Passport" component={Passport}  />
      <Stack.Screen name="DriverLicense" component={DriverLicense}  />
      <Stack.Screen name="StudentId" component={StudentId}  />
      <Stack.Screen name="FaceId" component={FaceId}  />
      <Stack.Screen name="WriteAbout" component={WriteAbout}  />
      <Stack.Screen name="OnComplete" component={OnComplete} />
      <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy}  />
      <Stack.Screen name="ResetPassword" component={ResetPassword}  />
      <Stack.Screen name="TermAndService" component={TermAndService}  />
    </Stack.Navigator>
  )
}

export default AuthStack