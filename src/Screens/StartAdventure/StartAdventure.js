import { View, Text,StyleSheet,Image, ScrollView, Platform } from 'react-native'
import React, { useRef, useState } from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import CustomButton from '../../custom/CustomButton'
import PhoneInput from 'react-native-phone-number-input'
import { Width } from '../../dimensions/dimension'
import { baseUrl, webClientId } from '../../utils/Urls'
import toast from '../../utils/Toast'
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin'
import { useEffect } from 'react'
import validation from '../../utils/validation'
import { appleAuth } from '@invertase/react-native-apple-authentication';
import { LoginButton, AccessToken, Profile, Settings, LoginManager,GraphRequest,GraphRequestManager } from 'react-native-fbsdk-next';
import AsyncStorage from '@react-native-async-storage/async-storage'

const StartAdventure = (props) => {
    
    const [value, setValue] = useState("")
    const [userInfo,setuserInfo]=('')
    const phoneInput = useRef(null);
    const [callingCode,setCallingCode]=useState(1)
    useEffect(()=>{
GoogleSignin.configure({
    scopes:[],
    webClientId:webClientId,
    offlineAccess:true
})
    },[])
    let id=props?.route?.params.id
    console.log("hehehehe==>",id)

    const handleNumber=async()=>{
      await AsyncStorage.setItem("callingCode",JSON.stringify(callingCode))
      console.log("phone number",value)
        data={
            email:"",
            phoneNumber:callingCode+value,
        }

        var phoneRe = /^[0-9]{8,13}$/;
        if(validation.phoneValid(value))
        {
            try{
                let response=await fetch(baseUrl+"auth/check-passenger",
                {
                method:'POST',
                body:JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' },
            }
                )
                .then(async(res)=>{
                    let jsonData=await res.json()
                    return jsonData

                })
                .then((res)=>{
                    console.log("response--->",res)
                    if(res.code==200)
                    {
                        toast("This Phone Number is already exist")
                    }
                    else
                    {
                        props.navigation.navigate('VerificationCode',{id:id,text:"StartAdventure",phone:value,callingCode:callingCode})

                    }
                })
               

                setTimeout(()=>{
                },2000)
            }
            catch(err)
            {
                console.log("ERRROR",err)
            }
        }
        // else{
        //     toast("Please enter a valid Phone number")
        // }
        
        // verfiyPhoneNumber()
    }
    // const verfiyPhoneNumber=async()=>{
    //     let num="+91"+value
    //     let confirmation=await auth().verifyPhoneNumber(num)
    //     // var appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

    //     console.log("confirm wala",confirmation)
    //     setConFirm(confirmation)
    //   }

    const signIn= async ()=>{
        try{
          await GoogleSignin.hasPlayServices();
          console.log("reached google sign in");
    
          // await GoogleSignin.signOut()
        const userInf=  await GoogleSignin.signIn();
  
          console.log(userInf)
        //   setTimeout(()=>{
        //     onVerify()
        //   },500)
          onVerify(userInf)
        }
        catch(err){
          if(err.code===statusCodes.IN_PROGRESS)
          {
            console.log('Signin in progress');
    
          }
          else if(err.code===statusCodes.PLAY_SERVICES_NOT_AVAILABLE)
          {
            console.log('PLAY_SERVICES_NOT_AVAILABLE')
          }
          else{
            console.log("err",err)
            // alert(err)
          }
        }
      }
      const onVerify= async(userInfo)=>{
        
        data={
            email:userInfo?.user?.email,
            phoneNumber:"",
        }
            try{
                let response=await fetch(baseUrl+"auth/check-passenger",
                {
                method:'POST',
                body:JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' },
            }
                )
                .then(async(res)=>{
                    let jsonData=await res.json()
                    return jsonData

                })
                .then((res)=>{
                    console.log("response--->",res)
                    if(res.code==200)
                    {
                        toast("This Email is already exist",colorConstant.theme)
                    signOut()
                    }
                    else
                    {
                        props.navigation.navigate("SignUp",{phone:"",userInfo:userInfo,aplpleRes:"",id:"emailSignIn"})
                        signOut()
                        // props.navigation.navigate('VerificationCode',{id:id,text:"StartAdventure",phone:value})

                    }
                })
            }
            catch(err)
            {
                console.log(err)
            }
        
        // 
      }
      signOut = async () => {
        try {
          await GoogleSignin.revokeAccess();
          await GoogleSignin.signOut();
        //   setloggedIn(false);
        //   setuserInfo([]);
        } catch (error) {
          console.error(error);
        }
      };



      //**** Apple Login *** */
async function onAppleButtonPress() {
  try{
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: appleAuth.Operation.LOGIN,
      requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      
    });
    console.log(appleAuthRequestResponse)
    const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);
  
    // use credentialState response to ensure the user is authenticated
    if (credentialState === appleAuth.State.AUTHORIZED) {
      // user is authenticated
      // alert("ok")
      props.navigation.navigate("SignUp",{phone:"",userInfo:"",aplpleRes:appleAuthRequestResponse,id:"appleSignIn"})
      appleAuth.Operation.LOGOUT()
      appleLogout()
    }
  }
  catch(err)
  {
    appleLogout()
    console.log(err)
  }
  // Start the sign-in request
  
  
}

const appleLogout=async()=>{
  return appleAuth.onCredentialRevoked(async () => {
    console.log('If this function executes, User Credentials have been Revoked');
  });
}


//*** facebook signup */

const fbLogin = (response) => {
  LoginManager.logOut()
  return LoginManager.logInWithPermissions(["email", "public_profile"]).then(
    result => {
      console.log(result, 'jhdjkdfhkjdshfkjdshfkjsdhfkjdsfhkjdsfh')
      if (result.declinedPermissions && result.declinedPermissions.includes("email")) {
        response("Message Email is require")
      }
      if (result.isCancelled) {
        console.log("error")
      }
      else {
        const res = new GraphRequest(
          '/me?fields=email,name,picture',
          null,
          response
        );
        new GraphRequestManager().addRequest(res).start()
      }
    },
    function(error){
      console.log(error);
    }
  )
}

const onFbLogin=async()=>{
  try {
    await fbLogin(_responceCallBack)
  } catch (error) {
    console.log(err
      );
  }
}

const _responceCallBack=async(error,result)=>{
    if(error){
      toastShow("Facebook Login error")
    }
    else{
      const userData= await result
      let objToSend = {
        email: userData.email,
        phoneNumber: "",
      };
      // try {
      //   // const response= await checkUserExist(objToSend)
      //   // if(response&&response.status=="200"){
      //   //   toastShow("This user allready exits")
      //   }
      //   else{
      //     props.navigation.navigate("Register", {
      //       data:userData,
      //       screenName: "Register",
      //       flow: "Facebook",
      //     });
      //   }
    //  } catch (error) {
    //     props.navigation.navigate("Register", {
    //       data:userData,
    //       screenName: "Register",
    //       flow: "Facebook",
    //     });
    //     console.log(error);
    //   }

      console.log(userData,'this is user data')
    }
}
console.log("calling code",callingCode)
  return (
    <ScrollView style={{flex:1,backgroundColor:'#FFFFFF'}}>
    <View style={styles.main}>

      <Text style={styles.welcomText}>Let's start the adventure!</Text>
      <Image
      source={imageConstant.Carrental}
      style={styles.img}
      />
      <Text style={styles.text}>Enter your phone number</Text>
          <PhoneInput
              ref={phoneInput}
              defaultValue={value}
              defaultCode="US"
            //   onChangeCountry={(c)=>console.log("ccccc",c)}
              layout="first"
              onChangeText={(text) => {
                  setValue(text);
                  console.log(text)
                  
              }}
              
            onChangeCountry={(e)=>{
              setCallingCode(e.callingCode[0])

              console.log(e)}}
              // onChangeFormattedText={(text) => {
              //     // setFormattedValue(text);
              //     setValue(text);
              //     console.log(text)
              // }}
              withDarkTheme
            //   withShadow
            //   autoFocus
              textInputStyle={{
                paddingVertical:0,
                height:20,
                fontSize:13
                
              }}
              containerStyle={styles.phoneContainer}
              flagButtonStyle={styles.flag}
              codeTextStyle={{height:0,width:0}}
              textContainerStyle={styles.textContainer}
          />
          <CustomButton
          buttonName={'Continue'}
          marginTop={20}
          borderRadius={10}
          OnButtonPress={()=>handleNumber()}
          />
          
          <Text style={{alignSelf:'flex-start',left:33,marginTop:20}}>
             <Image 
          source={imageConstant.Line}
          style={{left:20}}
          />      OR        <Image 
          source={imageConstant.Line2}
          
          />  </Text>
            <CustomButton
            imageFlag={true}
            buttonName={'Continue with Google'}
            backgroundColor={colorConstant.black}
            marginTop={20}
            borderRadius={10}
            imageName={imageConstant.Google}
            OnButtonPress={signIn}
            />
            <CustomButton
            imageFlag={true}
            buttonName={'Continue with Facebook'}
            backgroundColor={colorConstant.black}
            marginTop={20}
            borderRadius={10}
            imageName={imageConstant.Facebook}
            OnButtonPress={onFbLogin}
            />
           {Platform.OS==='ios' ?  <CustomButton
             imageFlag={true}
            buttonName={'Continue with Apple'}
            backgroundColor={colorConstant.black}
            marginTop={20}
            marginBottom={20}
            borderRadius={10}
            imageName={imageConstant.Apple}
            OnButtonPress={onAppleButtonPress}
            />
          :null}
              </View>
              </ScrollView>
  )
}

export default StartAdventure
const styles=StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#FFFFFF'
    },
    welcomText:{
        color:'#6C00C0',
        fontFamily:fontConstant.semibold,
        fontSize:30,
        width:246,
        lineHeight:37,
        textAlign:'center',
        marginTop:'17%'
    },
    img:{
        marginTop:'7%'
    },
    text:{
        color:colorConstant.theme,
        fontSize:15,
        alignSelf:'flex-start',
        left:35.5,
        marginTop:10,
        fontWeight:'500'
    },
    phoneContainer:{
        marginTop:10,
                borderWidth:1,
                borderColor:colorConstant.theme,
                borderRadius:10,
                width:"85%",
                height:48

    },
    flag:{
        paddingVertical:0,
        borderRadius:10,
        // height:50,
        height:45,
    },
    textContainer:{
        borderTopRightRadius:10,
        borderBottomRightRadius:10
    }
})