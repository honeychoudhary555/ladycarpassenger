import { View, Text, ImageBackground, Modal,Image, StyleSheet, TouchableOpacity,PermissionsAndroid, Alert, Linking, Platform } from 'react-native'
import React, { useEffect, useState } from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import { Width } from '../../dimensions/dimension'
import { useIsFocused } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { action } from '../../redux/reducers';
import Geolocation from '@react-native-community/geolocation';

const Landing = (props) => {
    const [modalVisibles,setModalVisibles]=useState(false)
    const isFocused = useIsFocused();
    const dispatch=useDispatch()

  useEffect(()=>{
    setModalVisibles(true);
    requestLocationPermission();
  
    
    // dispatch(action.setIntroStatus("login"))

    
  },[])



  const requestLocationPermission = async () => {
    try {
      if (Platform.OS === 'android' && Platform.Version >= 23) {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Permission',
            message:
              'This app requires access to your location ' ,
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          getLocation();
          
        } else {
          dispatch(action.setIntroStatus("login"))
          // alert("Location permission denied")
        }
      } else {
       getLocation()
       
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const getLocation=()=>{
    Geolocation.getCurrentPosition((info)=>{
      console.log("Geo",info)
      dispatch(action.setIntroStatus("login"))
    },   (error) => {
     

      if(error.code == 2)
      {
        Linking.openSettings();
        console.log("hello")
      }
      else if(error.code==1)
      {
        dispatch(action.setIntroStatus("login"))
      }

      else{
        Alert.alert('Locations', error.message, [
          {text: 'OK', onPress: () => console.log('OK Pressed',error.code)},
        ]);
        
        // Linking.openSettings();
      }
    },
    { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }      
    )
  }
    const modalClose=()=>{
      setModalVisibles(false);
      dispatch(action.setIntroStatus("login"))
    }
  return (
    <ImageBackground
    source={imageConstant.bgImage}
    style={styles.main}
    >
        {/* <Modal
        visible={modalVisibles}
        transparent={true}
        onRequestClose={() => {
            // Alert.alert("Modal has been closed.");
            setModalVisibles(!modalVisibles);
          }} 
    
        > */}
            {/* <View style={styles.modalView}>
              <Image
              source={imageConstant.location}
              style={styles.img}
              />
                {/* <View style={{alignItems:'center'}}> */}
                    {/* <Text style={styles.text1}>Allow <Text style={{fontWeight:'700'}}>LadyCar</Text> to access this device’s location ?</Text>
                   <TouchableOpacity style={styles.touch1} onPress={modalClose}>
                    <Text style={styles.text2}>While using the App</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touch2} onPress={modalClose}>
                    <Text style={styles.text3}>Only this time</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style9s.touch2} onPress={modalClose}>
                    <Text style={styles.text3}>Deny</Text>
                    </TouchableOpacity> */}
                {/* </View> */}
            

    </ImageBackground>
  )
}

export default Landing
const styles=StyleSheet.create({
  main:{
    flex:1
  },
  modalView:{
    position:'absolute',
    width:Width,
    bottom:0,
    height:320,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:colorConstant.white,
    borderTopRightRadius:52,
    borderTopLeftRadius: 52
  },
  img:{
    position:'absolute',top:0
  },
  text1:{
    color:colorConstant.black,
    lineHeight:20,
    fontSize:18,
    width:315,
    fontWeight:'400',
    marginTop:25
  },
  text2:{
    color:'#1374BA',
    fontFamily:fontConstant.bold,
    lineHeight:24,
    fontSize:16,
    // textShadowColor:'#1374BA',
    // textShadowOffset:{ width:1,height:1},
    // elevation:5,
    // shadowOpacity:0.9,
    // textShadowRadius:3.6,
    
    // marginTop:30
  },
  text3:{
    color:'#1374BA',
    lineHeight:24,
    fontSize:16,
    fontFamily:fontConstant.bold,
    // marginTop:20
  },
  touch1:{
    marginTop:30,
    // shadowColor:'red',
    // shadowOffset:{width:0,height:5},
    // elevation:20,
    // shadowOpacity:2,
    // shadowRadius:20
   
  },
  touch2:{
    marginTop:15
  }
})