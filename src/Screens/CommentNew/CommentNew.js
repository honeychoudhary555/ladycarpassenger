import { View, Text } from 'react-native'
import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import { colorConstant, fontConstant } from '../../utils/constant';
import { Width } from '../../dimensions/dimension';
import RecivedComment from './RecivedComment';
import MyCommentNew from './MyCommentNew';


const Tab = createMaterialTopTabNavigator();
const CommentNew=(props)=> {
const {navigation,route}   = props;
 
 
  return (
    <Tab.Navigator

    screenOptions={{
      tabBarInactiveTintColor:"#929292",
      tabBarActiveTintColor:"#000000",
      tabBarPressColor:"#d3d3d3",
      tabBarItemStyle: { width:Width/2},
      tabBarStyle:{
        elevation:3,
        borderBottomWidth:0,
      },
tabBarIndicatorStyle:{backgroundColor:colorConstant.theme}    
    }}>
      <Tab.Screen name="Received"
        options={{
          tabBarLabel: (props) => {
            console.log("focused",props)
            return (
              <View style={{
                width: Width / 2,
                flexDirection: "row",
                paddingVertical: 12,
                alignItems: "center",
                justifyContent: "center",
              }}>
                <Text style={{
                  color : props.color,
                  fontSize: 14,
                  fontFamily: props.color == "#000000" ?  fontConstant.bold : fontConstant.regular,
                }}>Received Comments</Text>
                

              </View>
            )
          }
        }}
        component={RecivedComment}
         />


      <Tab.Screen name="Own"
        options={{
          tabBarLabel: (props) => {
            console.log("focused----",props)
            return (
              <View style={{
                width: Width / 2,
                flexDirection: "row",
                paddingVertical: 12,
                alignItems: "center",
                justifyContent: "center"
              }}>
                <Text style={{
                   color : props.color,
                  fontSize: 14,
                  fontFamily: props.color == "#000000" ?  fontConstant.bold : fontConstant.regular,
                }}>My Comments</Text>
               
              </View>
            )
          }
        }}
        component={MyCommentNew} />



      
    </Tab.Navigator>
  )
}



  

export default CommentNew