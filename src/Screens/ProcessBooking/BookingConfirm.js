import { View, Text, StyleSheet, Image } from 'react-native'
import React from 'react'
import { imageConstant } from '../../utils/constant'
import { useEffect } from 'react'

const BookingConfirm = (props) => {
    useEffect(()=>{
        setTimeout(()=>{
            props.navigation.navigate("ScheduleTrip")
        },3000)
    },[])
  return (
    <View style={styles.main}>
      <Image
      resizeMode='contain'
      source={imageConstant.success}
      style={styles.img1}
      />
      <Text style={styles.text1}>Your booking is confirmed</Text>
      <Text style={styles.text2}>Please check your scheduled ride detail in booking history.</Text>
    </View>
  )
}

export default BookingConfirm
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF',
        alignItems:'center',
        // justifyContent:'center'
        
    },
    img1:{
        height:111,
        width:111,
        marginTop:'55%'
    },
    text1:{
        color:'#222B45',
        fontSize:15,
        fontWeight:'700',
        marginTop:'5%'
    },
    text2:{
        color:'#222B45',
        fontSize:14,
        marginTop:'2%',
        textAlign:'center',
        width:'90%'
    }
})