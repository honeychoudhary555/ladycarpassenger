import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import CustomHeader from '../../custom/CustomHeader'
import { TextInput } from 'react-native-gesture-handler'
import { Width } from '../../dimensions/dimension'
import CustomButton from '../../custom/CustomButton'

const AddMoney = (props) => {
  return (
    <View style={styles.main}>
      <CustomHeader
      onPressBack={()=>props.navigation.goBack()}
      />
      <Text style={styles.text1}>Add Money</Text>
      <Text style={styles.text2}>Add Amount</Text>
      <TextInput
      placeholder='$50'
      style={styles.input}
      />
      <CustomButton
      buttonName={"Continue to Pay"}
      borderRadius={10}
      height={38}
      width={'75%'}
      position={'absolute'}
      bottom={'8%'}
      OnButtonPress={()=>props.navigation.navigate("Payment2")}
      />
    </View>
  )
}

export default AddMoney
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF',
        alignItems:'center'
    },
    text1:{
        color:'#000000',
        fontSize:24,
        alignSelf:'flex-start',
        fontWeight:'500',
        left:'5%',
        marginTop:'10%'
    },
    text2:{
        color:'#000000',
        fontSize:17,
        alignSelf:'flex-start',
        fontWeight:'400',
        left:'5%',
        marginTop:'10%'
    },
    input:{
        height:48,
        width:Width * 0.9,
        borderWidth:1,
        borderRadius:14,
        borderColor:'#979797',
        marginTop:'5%',
        paddingHorizontal:15
    }
})