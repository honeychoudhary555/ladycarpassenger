import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity, Pressable, TextInput, ScrollView, Platform } from 'react-native'
import React from 'react'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Height, Width } from '../../dimensions/dimension';
import { useState } from 'react';
import CustomButton from '../../custom/CustomButton';
import { Rating, AirbnbRating } from 'react-native-ratings';

const TripSummary = (props) => {
    const [check, setCheck] = useState(true)
    const [SelectedId, setSelectedId] = useState(null)
    const [status, setStatus] = useState(false)
    const [value, setValue] = useState(true)
    const mapStyle = [
        { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
        {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{ color: '#263c3f' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#6b9a76' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{ color: '#38414e' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#212a37' }],
        },
        {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#9ca5b3' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{ color: '#746855' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#1f2835' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#f3d19c' }],
        },
        {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{ color: '#2f3948' }],
        },
        {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{ color: '#17263c' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#515c6d' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{ color: '#17263c' }],
        },
    ];




    return (
        <View style={styles.main}>




            <ScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    backgroundColor: "#FFFFFF",

                    // paddingBottom: 30
                }}>
                <View style={{ height: Height * 0.50}}>
                    <MapView
                        zoomEnabled={true}
                        zoomTapEnabled={true}
                        style={styles.mapStyle}
                        initialRegion={{
                            latitude: 28.5355,
                            longitude: 77.3910,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                        customMapStyle={mapStyle}>
                        <Marker
                            icon={imageConstant.location}
                            draggable
                            coordinate={{
                                latitude: 37.78825,
                                longitude: -122.4324,
                            }}
                            onDragEnd={
                                (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                            }
                            title={'Test Marker'}
                            description={'This is a description of the marker'}
                        />
                    </MapView>
                    <View style={styles.view}>
                        <Pressable onPress={() => props.navigation.toggleDrawer()}>

                            <Image
                            resizeMode='contain'
                            style={{height:42,width:42}}
                                source={imageConstant.menuIcon}
                            />
                        </Pressable>
                        <Pressable onPress={() => props.navigation.navigate("Profile")}>
                            <Image
                            resizeMode='contain'
                                style={styles.img1}
                                source={imageConstant.profile}
                            />
                        </Pressable>
                    </View>

                    <Image
                        style={styles.img2}
                        source={imageConstant.Zooms}
                    />
                </View>
                <View style={styles.view1}>
                    <Image
                        resizeMode='contain'
                        style={styles.img3}
                        source={imageConstant.Rect}
                    />
                    <Text style={styles.text3}>Summary of your trip : </Text>
                    <View style={styles.view6}>
                        <Image
                            style={{ marginTop: 10 }}
                            resizeMode='contain'
                            source={imageConstant.passengercar}
                        />
                        <View>
                            <Text style={styles.text6}>4 min</Text>
                            <Image
                                source={imageConstant.Arrow5}
                            />
                            <Text style={styles.text7}>Your Location</Text>
                        </View>
                        <Image
                            resizeMode='contain'
                            style={{ height: 30, width: 27 }}
                            source={imageConstant.gpspoint}
                        />
                        <View>
                            <Text style={styles.text6}>7 min</Text>
                            <Image
                                source={imageConstant.Arrow5}
                            />
                            <Text style={styles.text7}>Destination</Text>
                        </View>
                        <Image
                            resizeMode='contain'
                            style={{ height: 30, width: 27 }}
                            source={imageConstant.newLoc}
                        />
                        <Text style={styles.text8}>$9</Text>
                    </View>
                    <View style={styles.view7}>
                        <View style={styles.view10}>
                            <Image
                                resizeMode='contain'
                                style={styles.imgProfile}
                                source={imageConstant.profile2} />
                            <View style={{ left: 10 }}>
                                <Text style={styles.nameText}>Shanon</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image
                                        resizeMode='contain'
                                        style={{ height: 22, width: 22, bottom: 2 }}
                                        source={imageConstant.star}
                                    />
                                    <Text style={styles.ratingText}>4.3</Text>
                                </View>
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => props.navigation.navigate('ReportDriver')}>
                            <Text style={{ color: colorConstant.theme, left: '22%' }}>Report this driver</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.view8}>
                        <View style={styles.ratingView}>
                            <Text style={styles.rateText}>Rate your Driver:</Text>
                            <AirbnbRating
                                size={20}
                                reviews=''
                                selectedColor='#FFD600'
                                // style={{}}
                                ratingCount={5}
                                starContainerStyle={{ marginTop: Platform.OS === "ios" ? -27 : -40 }}
                            />
                        </View>
                        <View>
                            <Text style={[styles.rateText, { alignSelf: 'flex-end' }]}>Tip:</Text>
                            <View style={styles.view9}>
                                <TextInput
                                    style={{ width: 96, height: 34 }} />
                                <Text style={styles.dollarText}>$</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.view11}>
                        <Image
                            source={imageConstant.profile}
                        />
                        <TextInput
                            placeholderTextColor='#929292'
                            style={styles.input}
                            placeholder='Add a comment...' />
                    </View>
                    <CustomButton
                        buttonName={"OK"}
                        borderRadius={9}
                        width={'85%'}
                        height={42}
                        marginBottom={30}
                        OnButtonPress={() => props.navigation.navigate("ScheduleTrip")}
                        marginTop={Platform.OS === 'ios' ? 10 : 0}
                    />

                </View>
            </ScrollView>
        </View>
    )
}

export default TripSummary
const styles = StyleSheet.create({
    main: {
        flex: 1,
        // alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    mapStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    text: {
        fontSize: 26,
        color: colorConstant.theme,
        width: 150,
        top: 30,
        fontFamily: fontConstant.semibold,
        height: 90,
        textAlign: 'center'
    },
    text2: {
        fontSize: 20,
        color: '#000000',
        right: 25
    },
    view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Width,
        marginTop: '2%'
    },
    view1: {
        backgroundColor: '#FFFFFF',
        // height: '100%',
        // position: 'relative',
        // top: '35%',
        width: Width,
        alignItems: 'center',


    },

    view2: {
        width: '95%',
        backgroundColor: '#FFFFFF',
        height: '20%',
        // borderTopWidth:1,
        marginTop: '3%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        // marginTop:'5%',
        alignItems: 'center'
    },

    view3: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // alignItems:'center',
        marginTop: '8%'
    },
    cancelText: {
        alignSelf: 'center',
        color: colorConstant.theme,
        marginTop: '15%'
    },
    img1: {
        borderWidth: 2,
        borderColor: '#FFFFFF',
        borderRadius: 100,
        right: 5,
        height:33,
        width:33
    },
    img2: {
        alignSelf: 'flex-end',
        right: 5,
        top: '3%'
    },
    img3: {
        marginTop: '3%',
        width: 77,
        height: 6
    },
    touch: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        width: '20%',
        left: '2%',
        height: 25,
        borderRadius: 10,
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderColor: '#000000',
        elevation: 3,
        shadowOffset: { height: 2, width: 2 }

    },
    text1: {
        color: colorConstant.theme,
        fontFamily: fontConstant.semibold
    },
    text3: {
        color: colorConstant.theme,
        fontSize: 19,
        top: 5,
        width: '90%',
        alignSelf: 'center',
        fontFamily: fontConstant.bold,
        // textAlign:'center'
    },
    view4: {
        flexDirection: 'row'
        , marginTop: '15%',
        width: '70%',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view5: {
        flexDirection: 'row',
        width: '25%',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    driverText: {
        color: '#000000',
        fontSize: 18,
        fontFamily: fontConstant.bold
    },
    imgProfile: {
        borderWidth: 2,
        height: 60,
        width: 60,
        borderColor: '#FFFFFF',
        borderRadius: 50,

    },
    ratingText: {
        left: 2,
        color: '#000000',
        fontSize: 15
    },
    tip: {
        color: colorConstant.theme,
        alignSelf: 'flex-start',
        fontFamily: fontConstant.semibold,
    },
    rateText: {
        color: colorConstant.theme,
        fontSize: 15,
        fontFamily: fontConstant.semibold,

    },
    ratingView: {
        // flexDirection:'row',
        width: Width * 0.42,
        // justifyContent:'space-between',
        alignItems: 'center',
        alignSelf: 'flex-start',
        marginTop: '3%'
    },
    view6: {
        flexDirection: 'row',
        width: Width,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        marginTop: '3%'
    },
    text6: {
        fontSize: 14,
        color: '#000000'
    },
    text7: {
        fontSize: 8,
        color: '#000000'
    },
    text8: {
        fontSize: 24,
        color: '#000000',
        fontFamily: fontConstant.semibold
    },
    view7: {
        width: Width,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginTop: '3%'
    },
    view8: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: Width * 0.95
    },
    view9: {
        borderWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: '#D4D4D4'
    },
    nameText: {
        color: '#000000',
        fontSize: 18,
        fontFamily: fontConstant.bold
    },
    dollarText: {
        right: 10,
        color: '#000000',
        fontFamily: fontConstant.bold,
        fontSize: 15
    },
    input: {
        borderBottomWidth: 1,
        paddingBottom: Platform.OS === 'ios' ? 5 : 0,
        borderColor: 'gray',
        marginBottom: Platform.OS === 'ios' ? -3 : 20,
        width: '80%',
        left: 5
    },
    view10: {
        flexDirection: 'row',
        alignItems: 'center',
        right: 20
    },
    view11: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: Platform.OS == 'ios' ? 20 : 0
    }
})

