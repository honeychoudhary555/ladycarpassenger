import { View, Text } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Landing from '../Screens/Landing/Landing'

const Stack=createNativeStackNavigator()
const IntroStack = () => {
  return (
   <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="">
    <Stack.Screen  name="Landing" screenOptions={{headerShown:false}} component={Landing}/>
   </Stack.Navigator>
  )
}

export default IntroStack