export default {
    localStates:
    {
        introStatus:null,  
        jwtToken:null,
        cabType:null,
        passengerCount:1,
        requestId:null,
        driverId:null,
        startLocation:null,
        endLocation:null
    }
};