import { View, Text, StyleSheet, TextInput } from 'react-native'
import React from 'react'
import CustomHeader from '../../custom/CustomHeader'
import { colorConstant } from '../../utils/constant'
import CustomButton from '../../custom/CustomButton'

const AddPromoCode = (props) => {
  return (
    <View style={styles.main}>
        <CustomHeader
      title={"Add a Promo Code"}
      onPressBack={()=>props.navigation.goBack()}

      />
      <TextInput
      placeholder='Enter your promo code'
      style={styles.input}
      />
      <CustomButton
      buttonName={"ADD"}
      width={'85%'}
      height={42}
      borderRadius={9}
      marginTop={'8%'}
      OnButtonPress={()=>props.navigation.navigate("DiscountCode")}
      />
    </View>
  )
}

export default AddPromoCode
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF',
        alignItems:'center'
    },
    input:{
        borderWidth:1,
        borderColor:colorConstant.theme,
        width:'85%',
        height:42,
        borderRadius:9,
        marginTop:'25%',
        paddingHorizontal:20
    }
})