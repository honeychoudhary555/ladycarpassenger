import { View, Text, Image } from 'react-native'
import React from 'react'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import Geolocation from '@react-native-community/geolocation';
import CustomInput from '../../custom/CustomInput';
import { Height, Width } from '../../dimensions/dimension';
import { useState } from 'react';
import CustomHeader from '../../custom/CustomHeader';
import { fontConstant, imageConstant } from '../../utils/constant';
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable';
import { useEffect } from 'react';
import { googleKey } from '../../utils/Urls';

const Destination = (props) => {
  // const [destination,setDestination]=useState('')
  const [startData, setStartData]=useState('')
  const address = props?.route?.params?.address
  const [destination, setDestination] = useState(address)
  const [startAddress,setStartAddress]=useState("")

  console.log(address)
  console.log(destination)
  console.log("hello",startAddress)
  return (
    <View
      style={{
        flex: 1,
      }}>
      <View style={{ left: 20, top: 15, flexDirection: 'row' }}>
        <Pressable onPress={() => props.navigation.goBack()}>
          <Image
            resizeMode='contain'
            style={{ width: 25, height: 25 }}
            source={imageConstant.leftArrow} />
        </Pressable>
        {/* <Text style={{alignSelf:'center',left:100,color:'#000000',fontFamily:fontConstant.bold}}>Switch Rider</Text> */}
      </View>
      {/* <CustomInput
      borderColor={'white'}
      height={35}
      width={331}
      marginTop={'7%'}
      enableHighAccuracyLocation
      placeholder={'Current location'}
      backgroundColor={'white'}
      value={destination}
      onChangeText={(e)=>setDestination(e)}
      /> */}
      <View style={{position:'absolute',alignSelf:'center',top:40}}>
      <GooglePlacesAutocomplete

placeholder={destination}
placeholderTextColor={'black'}
returnKeyType={'default'}
// renderDescription={}
defaultValue={destination}
renderHeaderComponent={(item) => {
  console.log("item", item)
  return (
    <View>
      </View>
  )
}}
autoFocus={true}

onPress={(data, details = null) => {
  // console.log(data, details)
  setStartData(details?.geometry?.location)
  setStartAddress(details?.formatted_address)
  setDestination(details?.formatted_address)
  console.log("destination wala", JSON.stringify(details?.formatted_address));
 
  // setTimeout(() => {
  //   props.navigation.navigate('ScheduleTrip', { data: data.description, start: destination, detail: details?.geometry?.location, screenName: "Des" })
  // }, 1000)
}}
query={{ key: googleKey }}
setAddressText={destination}
fetchDetails={true}
plache
onFail={error => console.log(error)}
textInputProps={{
  placeholderTextColor: '#000000',
  // returnKeyType: "search"
}}
onNotFound={() => console.log('no results')}
listEmptyComponent={() => (
  <View style={{ flex: 1 }}>
    <Text style={{
      fontSize: 14,
      fontFamily: fontConstant.bold,
      width: "90%",
      alignSelf: "center",
      marginVertical: Height * 0.25,
      textAlign: "center",
     color:'#000000'
    
    }}>No results were found</Text>
  </View>

)}
styles={{
  textInputContainer: {
    width: 331,
    alignSelf: 'center',
    marginTop: 10
  },
  textInput: {
    height: 35,
    marginBottom: 10,
    borderRadius: 10
  }

}}
/>

      </View>

      
      <View style={{position:'absolute',alignSelf:'center',top:90,zIndex:-1}}>
      <GooglePlacesAutocomplete

        placeholder="Where to?"
      
        returnKeyType={'default'}
        // renderDescription={}
        renderHeaderComponent={(item) => {
          console.log("item", item)
          return (
            <></>
          )
        }}
        autoFocus={true}
        onPress={(data, details = null) => {
          // console.log(data, details)
          console.log("destination wala", JSON.stringify(details?.geometry?.location));
          setTimeout(() => {
            props.navigation.navigate('ScheduleTrip', { data: data.description, start: destination, detail: details?.geometry?.location, screenName: "Des",startData:startData,startAddress:startAddress })
          }, 1000)
        }}
        query={{ key:googleKey}}
        fetchDetails={true}
        placeholderTextColor={'#000000'}
        onFail={error => console.log(error)}
        onNotFound={() => console.log('no results')}
        listEmptyComponent={() => (
          <View style={{ flex: 1 }}>
            <Text style={{
              fontSize: 14,

              fontFamily: fontConstant.bold,
              width: "90%",
              alignSelf: "center",
              marginVertical: Height * 0.25,
              textAlign: "center",
              color:'#000000',
              
            }}>No results were found</Text>
          </View>
        )}
        styles={{
          textInputContainer: {
            width: 331,
            alignSelf: 'center',
            marginTop: 10
          },
          textInput: {
            height: 35,
            marginBottom: 10,
            borderRadius: 10
          }

        }}
      />
      </View>
    </View>
  )
}

export default Destination