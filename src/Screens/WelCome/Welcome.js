import { View, Text, StyleSheet,Image } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import CustomButton from '../../custom/CustomButton'

const Welcome = (props) => {
  return (
    <View style={styles.main}>
      <Text style={styles.welcomText}>Welcome to LadyCar!</Text>
      <Image
      source={imageConstant.Carrental}
      style={styles.img}
      />
      <CustomButton
      buttonName={'Sign Up'}
      marginTop={'30%'}
      borderRadius={10}
      height={38}
      OnButtonPress={()=>props.navigation.navigate('StartAdventure',{id:"signup"})}
      />
      <CustomButton
      buttonName={'Log In'}
      marginTop={'10%'}
      borderRadius={10}
      height={38}
      OnButtonPress={()=>props.navigation.navigate('Login',{text:"login"})}
      />
    </View>
  )
}

export default Welcome
const styles=StyleSheet.create({  
    main:{
        flex:1,
        alignItems:'center',
        backgroundColor:colorConstant.white
    },
    welcomText:{
        color:'#6C00C0',
        fontFamily:fontConstant.semibold,
        fontSize:30,
        width:246,
        lineHeight:37,
        textAlign:'center',
        marginTop:'17%'
    },
    img:{
        marginTop:'7%'
    }
})