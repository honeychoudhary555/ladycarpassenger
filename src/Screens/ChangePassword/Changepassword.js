import {
  Image,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { colorConstant, fontConstant, imageConstant } from "../../utils/constant";
import { useState } from "react";
// import { ChangePasswordServices } from "../services/DriverServices";
import { moderateScale, scale } from "react-native-size-matters";
import { SafeAreaView } from "react-native-safe-area-context";

// import toastShow from "../utils/toastShow";
// import { checkPasswordFormat } from "../utils/HelperFunction";
import { useNavigation } from "@react-navigation/native";
// import CustomPasswordInput from "../customComponents/CustomPasswordInput"
import { KeyboardAwareScrollView } from "@codler/react-native-keyboard-aware-scroll-view";
import toastShow from "../../utils/Toast";
import { baseUrl } from "../../utils/Urls";
import { useSelector } from "react-redux";
import validation from "../../utils/validation";
const Changepassword = ({ route,props }) => {
  console.log(route.params, "param data");
  const navigation = useNavigation();
  const [showpassword, setShowpassword] = useState(true);
  const [changePassword, setChangepassword] = useState({
    oldPassword: "",
    newPassword: "",
    confirmPassword: "",
  });
  const [validpassword, setValidPassword] = useState(true);
  const { jwtToken } = useSelector((state) => state.reducers)
  // const paramData = route?.params?.oldPassword;

  const passwordHandler = (text) => {
    // let data = checkPasswordFormat(text);
    let data=text
    console.log(data, "dkjsbfdsjkfh");
    if (data) {
      setChangepassword({
        ...changePassword,
        newPassword: text,
      });
      setValidPassword(data);
    } else {
      setChangepassword({
        ...changePassword,
        newPassword: text,
      });
      setValidPassword(data);
    }
  };

  // change password function
  const changePasswordFunction = async () => {
    // alert('skjdshjk')

  //   if (changePassword.oldPassword == "") {
  //     toastShow("Enter old password",'red')
  //     return
  //   }

  //   if(validation.passwordValid(changePassword.newPassword ))
  //   if (changePassword.newPassword == "") {
  //     toastShow("Enter New Password",'red');
  //     return;
  //   }
  //   if (changePassword.confirmPassword == "") {
  //     toastShow("Enter Confirm password",'red');
  //     return;
  //   }

    
  //  if (changePassword.confirmPassword !== changePassword.newPassword) {
  //     toastShow(" Confirm password not match", 'red');
  //     return;
  //   }
  


    let objToSend = {
      oldPassword: changePassword.oldPassword,
      newPassword: changePassword.newPassword,
    };
   
    console.log(objToSend, "object data");
    if (changePassword.oldPassword == "") {
          toastShow("Enter old password",'red')
          return
        }
        else if (validation.newPasswordValid(changePassword.newPassword ))
        {
          if (changePassword.confirmPassword == "") {
                toastShow("Enter Confirm password",'red');
                return;
              }
      else if (changePassword.confirmPassword !== changePassword.newPassword) {
              toastShow(" Confirm password not match", 'red');
              return;
            }
          else{
            try {
              await fetch(baseUrl+"user/passenger-change-password",{
                method:"POST",
                headers:{
                  'Content-Type': 'application/json',
                  'Authorization': jwtToken
                },
                body:JSON.stringify(objToSend)
              })
              .then(async(res)=>{
                let jsonData=await res.json()
                return jsonData
              })
              .then((res)=>{
                console.log("res",res)
                if(res.code==200)
                {
                  toastShow("password change successfully",'green')
                  navigation.goBack()
                }
                else{
                  toastShow(res.message,'red')
                }
                // 
              })
            } catch (error) {
              console.log(error);
              // toastShow(err)
            }
          }
          }
   
  };
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, alignItems: "center" }}>
        <TouchableOpacity
          style={{
            padding: 5,
            width: "100%",
            paddingHorizontal: moderateScale(20),
          }}
          onPress={() => navigation.goBack()}
        >
          <Image
            resizeMode="contain"
            source={imageConstant.Arrow}
            style={{
              height: moderateScale(30),
              width: scale(30),
            }}
          />
          {/* <Text>kfgskdjfhkj</Text> */}
        </TouchableOpacity>
        <KeyboardAwareScrollView style={{ width: "100%" }}>
          <View style={{ alignItems: "center" }}>
            <Text
              style={{
                fontSize: 29,
                color: colorConstant.theme,
                fontFamily: fontConstant.bold,
                marginTop: moderateScale(20),
              }}
            >
              Change Password
            </Text>
            <Image
              source={imageConstant.boarding2}
              style={{ marginVertical: moderateScale(20) }}
            />

            {/* <View></View>
              <TextInput
              secureTextEntry={showpassword}
                value={paramData}
                placeholder="Old Password"
                keyboardType="number-pad"
                editable={false}
                style={{
                  width: "80%",
                  borderWidth: 1,
                  borderRadius: 10,
                  marginVertical: moderateScale(10),
                  paddingHorizontal: scale(10),
                  color:colorConstant.black
                }}
            
              />

<TouchableOpacity
        // onPress={props?.onPress}
        style={{
          padding: 5,
          position:"absolute"
        }}
      >
        <Image
          source={imageConstant.eye}
          resizeMode="contain"
          style={{
            width: 15,
            height: 15,
          }}
        />
      </TouchableOpacity> */}

            <TextInput
            placeholderTextColor={'gray'}
              value={changePassword.oldPassword}
              placeholder="Old Password"
              // keyboardType="number-pad"
              style={{
                width: "80%",
                borderWidth: 1.5,
                borderRadius: 10,
                marginVertical: moderateScale(10),
                backgroundColor: colorConstant.white,
                paddingVertical: moderateScale(10),
                paddingHorizontal: scale(10),
                borderColor: colorConstant.theme,
                color:colorConstant.black
              }}
              onChangeText={(val) =>
                setChangepassword({ ...changePassword, oldPassword: val })
              }
            />

            {/* <CustomPasswordInput
              marginTop={15}
              placeholder={"Old Password"}
              editable={false}
              value={changePassword.oldPassword}
              secureTextEntry={showpassword}
              onChangeText={(val) =>
                setChangepassword({ ...changePassword, oldPassword: val })
              }
              onPress={() => setShowpassword(!showpassword)}
            /> */}

            <TextInput
                        placeholderTextColor={'gray'}

              value={changePassword.newPassword}
              placeholder="New Password"
              // keyboardType="number-pad"
              style={{
                width: "80%",
                borderWidth: 1.5,
                borderRadius: 10,
                marginVertical: moderateScale(10),
                backgroundColor: colorConstant.white,
                paddingVertical: moderateScale(10),
                paddingHorizontal: scale(10),
                borderColor: colorConstant.theme,
                color:colorConstant.black
              }}
              onChangeText={(val) => passwordHandler(val)}
            />

            {!validpassword && changePassword.newPassword.length > 0 && (
              <Text
                style={{
                  fontSize: 10,
                  fontFamily: fontConstant.regular,
                  color: 'red',
                  width: "80%",
                  marginTop: 5,
                  alignSelf: "center",
                  //   backgroundColor:colorConstant.white
                }}
              >
                Required minimum 8 length,1 Small,Capital & Special Character
              </Text>
            )}

            <TextInput
                        placeholderTextColor={'gray'}

              value={changePassword.confirmPassword}
              placeholder="Confirm Password"
              // keyboardType="number-pad"
              style={{
                width: "80%",
                borderWidth: 1.5,
                borderRadius: 10,
                marginVertical: moderateScale(10),
                backgroundColor: colorConstant.white,
                paddingHorizontal: scale(10),
                paddingVertical: moderateScale(10),
                borderColor: colorConstant.theme,
                color:colorConstant.black
              }}
              onChangeText={(val) =>
                setChangepassword({ ...changePassword, confirmPassword: val })
              }
            />

            <TouchableOpacity
              onPress={changePasswordFunction}
              style={{
                backgroundColor: colorConstant.theme,
                width: "80%",
                borderRadius: 10,
                paddingVertical: moderateScale(13),
                alignItems: "center",
                marginTop: moderateScale(30),

              }}
            >
              <Text
                style={{
                  color: colorConstant.white,
                  fontFamily: fontConstant.semi,
                  fontSize: moderateScale(15),
                }}
              >
                Change Password
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </SafeAreaView>
  );
};

export default Changepassword;
