

import { View, Text ,StyleSheet, FlatList, Image, Pressable} from 'react-native'
import React from 'react'
import CustomHeader from '../../custom/CustomHeader'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import { Width } from '../../dimensions/dimension'
import { color } from 'react-native-reanimated'

const MyComments = (props) => {
    const data=[
        {   id:1,
            name:"3 months ago you wrote about Lie:",
            comment:"excellent trip !!",
            rating:5
        },
        {   id:2,
            name:"3 months ago you wrote about Mary:",
            comment:"Very pleasant !",
            rating:4.5
        },
       

    ]
    const renderDetails=({item,index})=>{
        return(
        <View
        style={styles.view3}
        key={index}>
            <Text style={styles.nameText}>{item.name}</Text>
            <View style={styles.view2}>
            <Text style={styles.text2}>{item.comment}  <Image resizeMode='contain' style={styles.img} source={imageConstant.star}/>{item.rating}</Text>
           <Image
           style={styles.img1}
           source={imageConstant.profile}
           />
            </View>
        </View>
        )
    }
    const renderView=()=>(
        <View
        style={{
          backgroundColor: '#979797',
          width:Width*0.8,
          height: 0.5,
        }}
      />
    )
  return (
    <View style={styles.main}>
      <CustomHeader
      title={"Comments about you"}
      color={"#000000"}
      top={2}
      onPressBack={()=>props.navigation.goBack()}
      />
      <View style={styles.view1}>
        <Text style={styles.text1}>Look at your comments you wrote :</Text>
       
      </View>
      <FlatList
        data={data}
        renderItem={renderDetails}
        ItemSeparatorComponent={renderView}
       />
    </View>
  )
}

export default MyComments
const styles=StyleSheet.create({
main:{
    flex:1,
    alignItems:'center',
    backgroundColor:colorConstant.white
},
view1:{
    width:Width * 0.80,
    borderTopWidth:0.8,
    // borderBottomWidth:1,
    marginTop:'13%',
    borderColor:'#979797',
    // marginTop:
},
text1:{
    fontSize:22,
    color:colorConstant.theme,
    fontFamily:fontConstant.bold,
    lineHeight:28,
    marginTop:'5%',
    width:Width*0.7
},
view2:{
    height:33,
    backgroundColor:colorConstant.theme,
    borderRadius:11,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    paddingHorizontal:10,
    marginTop:10
},
nameText:{
    fontSize:18,
    color:'#000000',
    // fontFamily:fontConstant.bold,

    left:10
},
img:{
    height:12,
    width:12
},
text2:{
    fontSize:16,
    color:'#FFFFFF'
},
view3:{
    marginVertical:15
},
img1:{
    height:20,
    width:20,
    borderWidth:1,
    borderColor:'#FFFFFF',
    borderRadius:10
}
})
