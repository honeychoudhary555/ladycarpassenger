import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native'
import React from 'react'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Width } from '../../dimensions/dimension';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CheckBox from '@react-native-community/checkbox';
import { useState } from 'react';
import CustomButton from '../../custom/CustomButton';


const RequestCancel = (props) => {
const [check, setCheck]=useState(true)
const [SelectedId, setSelectedId]=useState(null)
const [status,setStatus]=useState(false)
const [value,setValue]=useState(true)
    const mapStyle = [
        { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
        {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{ color: '#263c3f' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#6b9a76' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{ color: '#38414e' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#212a37' }],
        },
        {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#9ca5b3' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{ color: '#746855' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#1f2835' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#f3d19c' }],
        },
        {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{ color: '#2f3948' }],
        },
        {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{ color: '#17263c' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#515c6d' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{ color: '#17263c' }],
        },
    ];
data=[
    {
    id:1,
    title:'one',
    img:imageConstant.profile2,
    color:'gray'
},
{
    id:2,
    title:'two',
    img:imageConstant.profile2,
    color:'black'
}
]

const renderDetails=({item,index})=>{
    console.log(item)
    return(
        <View>
            <TouchableOpacity 
            onPress={()=>handleClick(index)}
            style={[styles.touch,{backgroundColor: SelectedId == index ? '#D9D9D9' :'#FFFFFF'}]}>
                <Image
                source={item.img}/>
                <View>
                <Text style={{color:'#000000',fontSize:15}}>{item.title}</Text>
                <Text style={{fontSize:11,color:'#000000'}}>Celio</Text>
                </View>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Image
                    source={imageConstant.star}/>
                    <Text style={{fontSize:20,color:'#000000'}}>4.3</Text>
                </View>
                <View>
                    <Text style={{color:'#000000',fontSize:15}}>{item.color}</Text>
                </View>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Image
                    source={imageConstant.Timer}
                    />
                    <Text style={{color:'#000000',fontSize:15}}>9 min</Text>
                </View>
            </TouchableOpacity>
            
        </View>
    )
}

const handleClick=(id)=>{
    setSelectedId(id)
    setStatus(true)
}

    return (
        <View style={styles.main}>
            <Text>Request</Text>
            <MapView
                zoomEnabled={true}
                zoomTapEnabled={true}
                style={styles.mapStyle}
                initialRegion={{
                    latitude: 28.5355,
                    longitude: 77.3910,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
                customMapStyle={mapStyle}>
                <Marker
                    icon={imageConstant.location}
                    draggable
                    coordinate={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                    }}
                    onDragEnd={
                        (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                    }
                    title={'Test Marker'}
                    description={'This is a description of the marker'}
                />
            </MapView>
            <View style={styles.view}>
                <Image
                    source={imageConstant.menuIcon}
                />
                <View >
                    <Image
                        style={styles.img1}
                        source={imageConstant.profile}
                    />
                </View>
            </View>
            <Image
                style={styles.img2}
                source={imageConstant.Zooms}
            />
            <View style={styles.view1}>
                <Image
                    resizeMode='contain'
                    style={styles.img3}
                    source={imageConstant.Rect}
                />
              <Text style={{fontSize:9}}>Travel safe</Text>
{value ?

          <View style={styles.view2}>
                    <Text style={styles.text2}>Sorry, another passenger got this driver. Please, re select another driver</Text>
      {/* <View style={{flexDirection:'row',alignItems:'center'}}>
      <Text style={{color:'#000000',fontSize:17,fontFamily:fontConstant.semibold}}>Random</Text>
      <CheckBox
      value={check}
      onValueChange={()=>setCheck(!check)}
      />
      </View> */}
          </View>
          : null}
          <FlatList
          data={data}
          renderItem={renderDetails}
          />
          
            </View>
            <CustomButton
          buttonName={'CONFIRM'}
          backgroundColor={status ? '#14E429' : '#848484'}
          position={'absolute'}
          bottom={10}
          borderRadius={10}
          OnButtonPress={()=>props.navigation.navigate('DriverApproval')}
          />
          
        </View>
    )
}

export default RequestCancel
const styles = StyleSheet.create({
    main: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    mapStyle: {
        flex: 2,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: '48%',
    },
    view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Width
    },
    view1: {
        backgroundColor: '#FFFFFF',
        height: '40%',
        position: 'absolute',
        top: '52%',
        width: Width,
        alignItems: 'center',
       

    },
    text: {
        color:'#000000',
        fontFamily:fontConstant.bold,
        fontSize:17,
        marginTop:10,
    },
    view2:{
        width:'95%',
        backgroundColor:'#FFFFFF',
        height:'20%',
        // borderTopWidth:1,
        marginTop:'2%',
        flexDirection:'row',
        justifyContent:'space-between',
        // marginTop:'5%',
        alignItems:'center'
    },
    text2:{
        color:colorConstant.theme,
        fontFamily:fontConstant.bold,
        fontSize:18,
        width:'100%'
    },
    view3:{
        flexDirection:'row',
        justifyContent:'space-between',
        // alignItems:'center',
        marginTop:'8%'
    },
    cancelText:{
        alignSelf:'center',
        color:colorConstant.theme,
        marginTop:'15%'
    },
    img1:{
        borderWidth: 2, 
        borderColor: 'blue', 
        borderRadius: 100, 
        right: 5
    },
    img2:{
        alignSelf: 'flex-end', 
        right: 5,
         top: 15
    },
    img3:{
        marginTop: '5%',
         width: 77,
          height: 6
    },
    touch:{
        flexDirection:'row',
        height:80,
        width:Width,
        justifyContent:'space-around',
        alignItems:'center'
    }
})