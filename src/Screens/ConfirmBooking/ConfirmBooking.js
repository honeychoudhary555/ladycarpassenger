import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity, Pressable } from 'react-native'
import React from 'react'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant';
import { Width } from '../../dimensions/dimension';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CheckBox from '@react-native-community/checkbox';
import { useState } from 'react';
import CustomButton from '../../custom/CustomButton';


const ConfirmBooking = (props) => {
const [check, setCheck]=useState(true)
const [SelectedId, setSelectedId]=useState(null)
const [status,setStatus]=useState(false)
const [value,setValue]=useState(true)
    const mapStyle = [
        { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
        {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{ color: '#263c3f' }],
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#6b9a76' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{ color: '#38414e' }],
        },
        {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#212a37' }],
        },
        {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#9ca5b3' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{ color: '#746855' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#1f2835' }],
        },
        {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#f3d19c' }],
        },
        {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{ color: '#2f3948' }],
        },
        {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#d59563' }],
        },
        {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{ color: '#17263c' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#515c6d' }],
        },
        {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{ color: '#17263c' }],
        },
    ];




    return (
        <View style={styles.main}>
            <MapView
                zoomEnabled={true}
                zoomTapEnabled={true}
                style={styles.mapStyle}
                initialRegion={{
                    latitude: 28.5355,
                    longitude: 77.3910,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
                customMapStyle={mapStyle}>
                <Marker
                    icon={imageConstant.location}
                    draggable
                    coordinate={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                    }}
                    onDragEnd={
                        (e) => alert(JSON.stringify(e.nativeEvent.coordinate))
                    }
                    title={'Test Marker'}
                    description={'This is a description of the marker'}
                />
            </MapView>
         
            <View style={styles.view}>
                <Pressable onPress={()=>{props.navigation.toggleDrawer()}}>
                <Image
                resizeMode='contain'
                style={{height:42,width:42}}
                    source={imageConstant.menuIcon}
                />
                </Pressable>
               <Text style={styles.text}>Your driver
is here!</Text><Pressable onPress={()=>props.navigation.navigate("Profile")}>
                    <Image
                    resizeMode='contain'
                        style={styles.img1}
                        source={imageConstant.profile}
                    />
                </Pressable>
            </View>

            <Image
                style={styles.img2}
                source={imageConstant.Zooms}
            />
              
              <View style={styles.view1}>
            <TouchableOpacity 
            
            style={styles.touch}>
                <Image
                resizeMode='contain'
                style={styles.img4}
                source={imageConstant.profile2}/>
                <View>
                </View>
                <View style={{right:'60%'}}>
                <Text style={styles.text2}>Sonia</Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                <Image
                    source={imageConstant.star}/>
                <Text style={{fontSize:15,color:'#000000',left:5}}>4.3</Text>
                </View>
                </View>

                <View style={{flexDirection:'row',alignItems:'center',width:'38%',justifyContent:'space-between'}}>
                    <Image
                      resizeMode='contain'
                      style={{height:28,width:28,top:2,right:10}}
                    source={imageConstant.Phone}
                    />
                    <Pressable onPress={()=>props.navigation.navigate("Chat")}>
                    <Image
                    resizeMode='contain'
                    style={{height:30,width:30}}
                    source={imageConstant.Speech}
                    />
                    </Pressable>
                       <Image
                    resizeMode='contain'
                    style={{height:55,width:55}}
                    source={imageConstant.droite}
                    />
                </View>
            </TouchableOpacity>
                    <Text style={styles.text3}>Confirm your identity before getting into the car </Text>
                    <View style={styles.view4}>
                    <CustomButton
                    buttonName={'FACE ID'}
                    marginTop={'25%'}
                    width={284}
                    height={29}
                    borderRadius={10}
                    OnButtonPress={()=>props.navigation.navigate('IdVerification')}
                    />
                    <Image
                   resizeMode='contain'
                    style={{marginTop:'25%',left:10,height:24,width:24}}
                    source={imageConstant.info}/>
                    </View>
                    <Pressable 
                    style={{flexDirection:'row',alignItems:'center',marginTop:'5%',marginBottom:'5%'}}
                    onPress={()=>props.navigation.navigate("CancelReason")}>
            <Text style={styles.cancelText}>Cancel?   </Text>
            <Image resizeMode='contain' style={{height:24,width:24}} source={imageConstant.info}/> 
            </Pressable>
            </View>
                    
          
        </View>
    )
}

export default ConfirmBooking
const styles = StyleSheet.create({
    main: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    mapStyle: {
        flex: 2,
        position: 'absolute',
        top: '16%',
        left: 0,
        right: 0,
        bottom: '40%',
    },
    text:{
        fontSize:26,
        color:colorConstant.theme,
        width:150,
        top:30,
        fontFamily:fontConstant.semibold,
        height:90,
        textAlign:'center'
    },
    view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Width,
        marginTop:'2%'
    },
    view1: {
        backgroundColor: '#FFFFFF',
        height: '40%',
        position: 'absolute',
        top: '60%',
        width: Width,
        alignItems: 'center',
       

    },
  
    view2:{
        width:'95%',
        backgroundColor:'#FFFFFF',
        height:'20%',
        // borderTopWidth:1,
        marginTop:'3%',
        flexDirection:'row',
        justifyContent:'space-between',
        // marginTop:'5%',
        alignItems:'center'
    },
 
    view3:{
        flexDirection:'row',
        justifyContent:'space-between',
        // alignItems:'center',
        marginTop:'8%'
    },
    cancelText:{
        alignSelf:'center',
        color:colorConstant.theme,
        // marginTop:'3%',
        // paddingBottom:20
    },
    img1:{
        borderWidth: 2, 
        borderColor: '#FFFFFF', 
        borderRadius: 100, 
        right: 5,
        height:33,
        width:33
    },
    img2:{
        alignSelf: 'flex-end', 
        right: 5,
         top: '8%'
    },
    img3:{
        marginTop: '5%',
         width: 77,
          height: 6
    },
    touch:{
        flexDirection:'row',
        height:60,
        backgroundColor:'#FFFFFF',
        width:Width,
        borderRadius:14,
        justifyContent:'space-around',
        alignItems:'center',
        top:'5%'
    },
    text3:{
        color:colorConstant.theme,
        fontSize:19,
        top:'20%',
        width:'80%',
        alignSelf:'center',
        fontFamily:fontConstant.bold,
        textAlign:'center'
    },
    img4:{
        borderWidth:2,
        height:66,
        width:66,
        borderColor:'#FFFFFF',
        borderRadius:50
    },
    text2:{
        fontSize:20,
        color:'#000000',
        // right:40
    },
    view4:{
        flexDirection:'row',
        alignItems:'center'
    }
})